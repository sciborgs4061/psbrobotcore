#include "HAL/HAL.hpp"


uint32_t HALReport(uint8_t resource, 
                   uint8_t instanceNumber, 
                   uint8_t context, 
                   const char *feature)
{
   return 1; // assume 1 is OK.
}

int HALInitialize(int mode)
{
   return 1; // assume 1 is OK.
}

void HALNetworkCommunicationObserveUserProgramStarting()
{
}

void HALNetworkCommunicationObserveUserProgramDisabled()
{
}

void HALNetworkCommunicationObserveUserProgramAutonomous()
{
}

void HALNetworkCommunicationObserveUserProgramTeleop()
{
}

void HALNetworkCommunicationObserveUserProgramTest()
{
}

