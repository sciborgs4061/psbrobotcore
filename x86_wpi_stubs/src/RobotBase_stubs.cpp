#include "RobotBase.h"

RobotBase * RobotBase::m_instance = NULL;

void RobotBase::setInstance(RobotBase *robot)
{
   m_instance = robot;
}

void RobotBase::robotSetup(RobotBase *robot)
{
   // OK...now I know...this is the main loop of the whole robot.
   // This is the last thing the Main calls so I need to kick things
   // off from here.

   robot->StartCompetition();
  
}

RobotBase &RobotBase::getInstance()
{
   return *m_instance;
}

RobotBase::RobotBase()
   : m_task(NULL)
   , m_ds(NULL)
{
}

RobotBase::~RobotBase()
{
}

// this controlls the transitions between the states
// that the robot takes
static int state_counter = 0;
enum {
   IS_DISABLED_COUNTS = 100,
   IS_AUTONOMOUS_COUNTS = 1000
};

bool RobotBase::IsEnabled()
{
   return (IS_DISABLED_COUNTS <= state_counter);
   return true;
}

bool RobotBase::IsDisabled()
{
   // just jack up the number here...this is always called
   state_counter++;
   return (IS_DISABLED_COUNTS > state_counter);
}

bool RobotBase::IsAutonomous()
{
   return (IS_AUTONOMOUS_COUNTS > state_counter);
}

bool RobotBase::IsOperatorControl()
{
   return !IsAutonomous();
}

bool RobotBase::IsTest()
{
   return false;
}

bool RobotBase::IsNewDataAvailable()
{
   return true;
}

void RobotBase::Prestart()
{
}
