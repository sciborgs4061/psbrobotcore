#include "LiveWindow/LiveWindow.h"

LiveWindow::LiveWindow()
{
   m_enabled = false;
   m_firstTime = true;
}

LiveWindow::~LiveWindow()
{
}

LiveWindow * LiveWindow::GetInstance()
{
   static LiveWindow *lw = NULL;
   if (lw == NULL) 
   {   
      lw = new LiveWindow;
   }
   return lw;
}

void LiveWindow::SetEnabled(bool enabled) { m_enabled = enabled; }
