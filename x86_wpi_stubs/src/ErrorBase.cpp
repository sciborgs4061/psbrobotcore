#include "ErrorBase.h"


////////////////////////////////////////////////////////////////////////////////
MUTEX_ID ErrorBase::_globalErrorMutex;


////////////////////////////////////////////////////////////////////////////////
Error ErrorBase::_globalError;


////////////////////////////////////////////////////////////////////////////////
ErrorBase::ErrorBase()
   : m_error()
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
ErrorBase::~ErrorBase()
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
Error& ErrorBase::GetError()
{
   return _globalError;
}


////////////////////////////////////////////////////////////////////////////////
const Error& ErrorBase::GetError() const
{
   return _globalError;
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::SetErrnoError(const char * /*contextMessage*/, 
                              const char * /*filename*/,
                              const char * /*function*/,
                              uint32_t     /*lineNumber*/) const
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::SetImaqError(int          /*success*/, 
                             const char * /*contextMessage*/, 
                             const char * /*filename*/,
                             const char * /*function*/,
                             uint32_t     /*lineNumber*/) const
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::SetError(Error::Code   /*code*/, 
                         const char  * /*contextMessage*/, 
                         const char  * /*filename*/,
                         const char  * /*function*/, 
                         uint32_t      /*lineNumber*/) const
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::SetWPIError(const char  * /*errorMessage*/,
                            Error::Code   /*code*/,
                            const char  * /*contextMessage*/,
                            const char  * /*filename*/,
                            const char  * /*function*/,
                            uint32_t      /*lineNumber*/) const
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::CloneError(ErrorBase * /*rhs*/) const
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::ClearError() const
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
bool ErrorBase::StatusIsFatal() const
{
   return true;
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::SetGlobalError(Error::Code   /*code*/, 
                               const char  * /*contextMessage*/, 
                               const char  * /*filename*/,
                               const char  * /*function*/,
                               uint32_t      /*lineNumber*/)
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
void ErrorBase::SetGlobalWPIError(const char * /*errorMessage*/,
                                  const char * /*contextMessage*/,
                                  const char * /*filename*/, 
                                  const char * /*function*/, 
                                  uint32_t     /*lineNumber*/)
{
   // Intentionally empty
}


////////////////////////////////////////////////////////////////////////////////
Error& ErrorBase::GetGlobalError()
{
   return _globalError;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

