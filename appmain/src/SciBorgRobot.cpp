// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <PsbLogDataUdpSender.h>
#include <GlobalOiWpiStore.h>
#include <GlobalSiWpiStore.h>
#include <GlobalAiWpiStore.h>
#include "SciBorgRobot.h"

// NEW_SS_INFO
// .... #include <NEW_SUBSYSTEMInterfaceContainer.h>
#include <SSTest1SubsystemContainer.h>


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM 0x5C1B0465


// /////////////////////////////////////////////////////////////////////////////
// Static Data Init
// /////////////////////////////////////////////////////////////////////////////

/// @todo
/// Make the CTOR take no arguments, and then have it internally load an xml
/// config file that tells it where to send the log data
#ifndef X86_PLATFORM
Psb::LogDataUdpSender sv_udpSender(0x0a283de0, 17654);  // 10.40.61.224:17654
#else // X86_PLATFORM
// on the x86 throw the logs to the localhost
Psb::LogDataUdpSender sv_udpSender(0x7f000001, 17654);  // 127.0.0.1:17654
#endif // X86_PLATFORM


// /////////////////////////////////////////////////////////////////////////////
// Namespace
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {

////////////////////////////////////////////////////////////////////////////////
SciBorgRobot::SciBorgRobot()
   : Psb::IterativeRobot()
   , mp_autonomousController(NULL)
   , mp_dashstreamController(NULL)
{
   // Create all singletons
   Logger::setDataSender(&sv_udpSender);
   Logger::instance();
   PSB_LOG_DEBUG3(0);

   // Create all WPI Handle Storage Objects
   Subsystem::GlobalOiWpiStore::instance();
   Subsystem::GlobalSiWpiStore::instance();
   Subsystem::GlobalAiWpiStore::instance();

   mp_dashstreamController = new Subsystem::Dashstream::Controller();
   mp_autonomousController = new Subsystem::Autonomous::Controller();

   // Put the robot subsystem in the dashstream so we can watch what
   // it is doing
   mp_dashstreamController->addSubsystem(getDashstreamInterface());
   // and put the autonomous in there also so we can watch it
   mp_dashstreamController->addSubsystem(mp_autonomousController->getDashstreamInterface());

   // NEW_SS_INFO
   // Now create the Subsystem container for each subsystem and keep track of it.
   m_subsystems.push_back(Psb::Subsystem::SSTest1::SubsystemContainer::instance());

   // now that they are created...init them all...
   // and add them to the dashstream controller
   for (auto p_action : m_subsystems)
   {
      p_action->init();
      mp_dashstreamController->addSubsystem(p_action->getDashstreamInterface());
   }

}


////////////////////////////////////////////////////////////////////////////////
SciBorgRobot::~SciBorgRobot()
{
   // Delete each of the controller objects
   delete mp_dashstreamController;
   mp_dashstreamController = NULL;

   delete mp_autonomousController;
   mp_autonomousController = NULL;

   // clean up all the subsystems
   for (auto p_action : m_subsystems)
   {
      delete p_action;
   }

   delete Subsystem::GlobalAiWpiStore::instance();
   delete Subsystem::GlobalSiWpiStore::instance();
   delete Subsystem::GlobalOiWpiStore::instance();
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::RobotInit()
{
   PSB_LOG_DEBUG3(0);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::DisabledInit()
{
   mp_autonomousController->transitionToState(Psb::Subsystem::SUBSYSTEM_DISABLED);
   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_DISABLED);

   // //////////////////////////////////////////////////////////////////////////
   // Ensure that all subystems are disabled
   // This will be overridden in Teleop mode when we actually read the real
   // values from the Button Box.  When disabled, a subsystem is expected to
   // reset its internal values to a known state.
   // //////////////////////////////////////////////////////////////////////////

   // go through and disable all the controllers
   for (auto p_action : m_subsystems)
   {
      p_action->getControllerInterface()->setEnabledState(Psb::Subsystem::DISABLED);
      p_action->getControllerInterface()->transitionToState(Psb::Subsystem::SUBSYSTEM_DISABLED);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::DisabledPeriodic()
{
   PSB_LOG_DEBUG3(0);

   // I'm not really sure about the ordering here.  In the disabled
   // state how much of this really matters.  All we want to do is look
   // for the operator interfance to update the auto program and lock it
   // in...we don't have to really even update the sensors except for
   // looking at them on the dashboard.  My feeling is that we need to
   // update the operator interfaces...then update the auto controller
   // so it can do its thing...and then we push the sensor interfaces
   // back out...in the end it doesn't really matter because doing it
   // out of order just means one extra cycle is wasted getting the job
   // done.
   Subsystem::GlobalOiWpiStore::instance()->refresh();
   PSB_LOG_DEBUG3(0);
   mp_autonomousController->update();

   // refresh all the sensors so we can see them on the dashstream
   // and in the logger
   PSB_LOG_DEBUG3(0);
   Subsystem::GlobalSiWpiStore::instance()->refresh();

   // Update the dashstream so the drivers can see the autonomous program selection
   PSB_LOG_DEBUG3(0);
   mp_dashstreamController->update();

   PSB_LOG_DEBUG3(0);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::AutonomousInit()
{
   PSB_LOG_DEBUG3(0);

   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_AUTONOMOUS);
   mp_autonomousController->transitionToState(Psb::Subsystem::SUBSYSTEM_AUTOMATIC);

   // go through and change state on all the controllers
   for (auto p_action : m_subsystems)
   {
      p_action->getControllerInterface()->transitionToState(Psb::Subsystem::SUBSYSTEM_AUTOMATIC);
   }
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::AutonomousPeriodic()
{

   /// of course...we do need to call the update function on autonomous
   mp_autonomousController->update();

   // DON'T refresh the OI's...because that is the job of the autonomous "update"

   // grab all the sensor inputs.
   Subsystem::GlobalSiWpiStore::instance()->refresh();

   // walk through the subsystems and do the work that needs done
   for (auto p_action : m_subsystems)
   {
      p_action->getControllerInterface()->update();
   }

   // now push the outputs to the robot
   Subsystem::GlobalAiWpiStore::instance()->flush();

   // Finally, send dashstream data to the dashboard laptop
   mp_dashstreamController->update();

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TeleopInit()
{
   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_TELEOPERATED);
   mp_autonomousController->transitionToState(Psb::Subsystem::SUBSYSTEM_MANUAL);

   // go through and change state on all the controllers
   for (auto p_action : m_subsystems)
   {
      p_action->getControllerInterface()->transitionToState(Psb::Subsystem::SUBSYSTEM_MANUAL);
   }
   return;
}

////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TeleopPeriodic()
{
   PSB_LOG_DEBUG3(0);

   /// if everything is in the global OI/AI/SI stores then we
   /// we can really just update all of those...do the updates
   /// on each controller....and then flush the AI globally

   Subsystem::GlobalOiWpiStore::instance()->refresh();
   Subsystem::GlobalSiWpiStore::instance()->refresh();

   // walk through the subsystems and do the work that needs done
   for (auto p_action : m_subsystems)
   {
      p_action->getControllerInterface()->update();
   }

   // now push the outputs to the robot
   Subsystem::GlobalAiWpiStore::instance()->flush();

   // dump the data so we can see it
   mp_dashstreamController->update();

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TestInit()
{
   // NOTE: This used to pass in TELEOPERATED...but maybe that was a bug
   mp_autonomousController->setFrcMatchState(Psb::Subsystem::MATCH_STATE_TEST);
   PSB_LOG_DEBUG3(0);
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
SciBorgRobot::TestPeriodic()
{
   PSB_LOG_DEBUG3(0);
   return;
}

} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

