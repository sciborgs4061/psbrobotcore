// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// This is a good place to actually build the dashstream interface
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "PsbRobotDashstreamInterface.h"

// don't want to build it again if someone happens to
// include things again
#undef BUILD_IT

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <PsbTimer.h>
#include "PsbIterativeRobot.h"
#include "DriverStation.h"
#include "HAL/HAL.hpp"
#include "SmartDashboard/SmartDashboard.h"
#include "LiveWindow/LiveWindow.h"
#include "networktables/NetworkTable.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM 0x17E40B07

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
IterativeRobot::IterativeRobot()
   : m_disabledInitialized (false)
   , m_autonomousInitialized (false)
   , m_teleopInitialized (false)
   , m_testInitialized (false)
{
   mp_dashstreamIntf = new Psb::Subsystem::PSBRobot::DashstreamInterface;
   m_data = (Psb::Subsystem::PSBRobot::DashstreamInterface::DashstreamPOD_t *)mp_dashstreamIntf->getData();
   memset(m_data,0,sizeof(Psb::Subsystem::PSBRobot::DashstreamInterface::DashstreamPOD_t));
}


////////////////////////////////////////////////////////////////////////////////
IterativeRobot::~IterativeRobot()
{
   delete mp_dashstreamIntf;
   mp_dashstreamIntf = NULL;
}

////////////////////////////////////////////////////////////////////////////////
Psb::Subsystem::Dashstream::BaseInterface_I *
IterativeRobot::getDashstreamInterface(void) 
{
   return mp_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void IterativeRobot::Prestart()
{
   // Don't immediately say that the robot's ready to be enabled.
   // See below.
}


/**
 * Provide an alternate "main loop" via StartCompetition().
 * 
 * This specific StartCompetition() implements "main loop" behaviour synced with the DS packets
 */
void IterativeRobot::StartCompetition()
{
   Psb::Timer loop_timer;
   loop_timer.start();

   HALReport(HALUsageReporting::kResourceType_Framework, HALUsageReporting::kFramework_Iterative);

   LiveWindow *lw = LiveWindow::GetInstance();

// only needed if we are on the actual robot...at least for now
// I just don't want to fake all this stuff out if I don't have to...
#ifndef X86_PLATFORM
   // first and one-time initialization
   SmartDashboard::init();
   NetworkTable::GetTable("LiveWindow")->GetSubTable("~STATUS~")->PutBoolean("LW Enabled", false);
#endif // X86_PLATFORM
   RobotInit();

   // We call this now (not in Prestart like default) so that the robot
   // won't enable until the initialization has finished. This is useful
   // because otherwise it's sometimes possible to enable the robot
   // before the code is ready.
   HALNetworkCommunicationObserveUserProgramStarting();

   // loop forever, calling the appropriate mode-dependent function
   lw->SetEnabled(false);
   while (true)
   {
      loop_timer.reset();

      // Call the appropriate function depending upon the current robot mode
      if (IsDisabled())
      {
         strcpy(m_data->m_robot_state,"Disabled");
         // call DisabledInit() if we are now just entering disabled mode from
         // either a different mode or from power-on
         if(!m_disabledInitialized)
         {
            lw->SetEnabled(false);
            DisabledInit();
            m_disabledInitialized = true;
            // reset the initialization flags for the other modes
            m_autonomousInitialized = false;
            m_teleopInitialized = false;
            m_testInitialized = false;
         }
         HALNetworkCommunicationObserveUserProgramDisabled();
         DisabledPeriodic();
      }
      else if (IsAutonomous())
      {
         strcpy(m_data->m_robot_state,"Autonomous");
         // call AutonomousInit() if we are now just entering autonomous mode from
         // either a different mode or from power-on
         if(!m_autonomousInitialized)
         {
            lw->SetEnabled(false);
            AutonomousInit();
            m_autonomousInitialized = true;
            // reset the initialization flags for the other modes
            m_disabledInitialized = false;
            m_teleopInitialized = false;
            m_testInitialized = false;
         }
         HALNetworkCommunicationObserveUserProgramAutonomous();
         AutonomousPeriodic();
      }
      else if (IsTest())
      {
         strcpy(m_data->m_robot_state,"Test");
         // call TestInit() if we are now just entering test mode from
         // either a different mode or from power-on
         if(!m_testInitialized)
         {
            lw->SetEnabled(true);
            TestInit();
            m_testInitialized = true;
            // reset the initialization flags for the other modes
            m_disabledInitialized = false;
            m_autonomousInitialized = false;
            m_teleopInitialized = false;
         }
         HALNetworkCommunicationObserveUserProgramTest();
         TestPeriodic();
      }
      else
      {
         strcpy(m_data->m_robot_state,"Teleop");
         // call TeleopInit() if we are now just entering teleop mode from
         // either a different mode or from power-on
         if(!m_teleopInitialized)
         {
            lw->SetEnabled(false);
            TeleopInit();
            m_teleopInitialized = true;
            // reset the initialization flags for the other modes
            m_disabledInitialized = false;
            m_autonomousInitialized = false;
            m_testInitialized = false;

// I just don't want to fake all this stuff out if I don't have to...
#ifndef X86_PLATFORM
            Scheduler::GetInstance()->SetEnabled(true);
#endif // X86_PLATFORM
         }
         HALNetworkCommunicationObserveUserProgramTeleop();
         TeleopPeriodic();
      }

      // Start next loop in 10 msec.
      double loop_duration = loop_timer.getElapsedTime();
      double delay         = 0.0095 - loop_duration;
      m_data->m_robot_cycle_time = loop_duration;
      m_data->m_robot_sleep_time = delay;
      if (delay > 0.0)
      {
         loop_timer.waitForTime(delay);
      }
      else
      {
         PSB_LOG_ERROR(2, Psb::LOG_TYPE_FLOAT64, loop_duration,
                          Psb::LOG_TYPE_FLOAT64, delay);
      }
   } // END while(true)

   return;
}

/**
 * Robot-wide initialization code should go here.
 * 
 * Users should override this method for default Robot-wide initialization which will
 * be called when the robot is first powered on.  It will be called exactly 1 time.
 */
void IterativeRobot::RobotInit()
{
   printf("Default %s() method... Overload me!\n", __FUNCTION__);
}

/**
 * Initialization code for disabled mode should go here.
 * 
 * Users should override this method for initialization code which will be called each time
 * the robot enters disabled mode.
 */
void IterativeRobot::DisabledInit()
{
   printf("Default %s() method... Overload me!\n", __FUNCTION__);
}

/**
 * Initialization code for autonomous mode should go here.
 * 
 * Users should override this method for initialization code which will be called each time
 * the robot enters autonomous mode.
 */
void IterativeRobot::AutonomousInit()
{
   printf("Default %s() method... Overload me!\n", __FUNCTION__);
}

/**
 * Initialization code for teleop mode should go here.
 * 
 * Users should override this method for initialization code which will be called each time
 * the robot enters teleop mode.
 */
void IterativeRobot::TeleopInit()
{
    printf("Default %s() method... Overload me!\n", __FUNCTION__);
}

/**
 * Initialization code for test mode should go here.
 * 
 * Users should override this method for initialization code which will be called each time
 * the robot enters test mode.
 */
void IterativeRobot::TestInit()
{
    printf("Default %s() method... Overload me!\n", __FUNCTION__);
}

/**
 * Periodic code for disabled mode should go here.
 * 
 * Users should override this method for code which will be called periodically at a regular
 * rate while the robot is in disabled mode.
 */
void IterativeRobot::DisabledPeriodic()
{
   static bool firstRun = true;
   if (firstRun)
   {
      printf("Default %s() method... Overload me!\n", __FUNCTION__);
      firstRun = false;
   }
   delayTicks(1);
}

/**
 * Periodic code for autonomous mode should go here.
 *
 * Users should override this method for code which will be called periodically at a regular
 * rate while the robot is in autonomous mode.
 */
void IterativeRobot::AutonomousPeriodic()
{
   static bool firstRun = true;
   if (firstRun)
   {
      printf("Default %s() method... Overload me!\n", __FUNCTION__);
      firstRun = false;
   }
   delayTicks(1);
}

/**
 * Periodic code for teleop mode should go here.
 *
 * Users should override this method for code which will be called periodically at a regular
 * rate while the robot is in teleop mode.
 */
void IterativeRobot::TeleopPeriodic()
{
   static bool firstRun = true;
   if (firstRun)
   {
      printf("Default %s() method... Overload me!\n", __FUNCTION__);
      firstRun = false;
   }
   delayTicks(1);
}

/**
 * Periodic code for test mode should go here.
 *
 * Users should override this method for code which will be called periodically at a regular
 * rate while the robot is in test mode.
 */
void IterativeRobot::TestPeriodic()
{
    static bool firstRun = true;
    if (firstRun)
    {
        printf("Default %s() method... Overload me!\n", __FUNCTION__);
        firstRun = false;
    }
    delayTicks(1);
}

} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

