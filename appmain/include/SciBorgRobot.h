// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbIterativeRobot.h>
#include <PsbMacros.h>
#include <PsbSubsystemContainer.h>
#include <AutonomousController.h>
#include <DashstreamController.h>
#include <list>

// /////////////////////////////////////////////////////////////////////////////
// Namespace
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class is the main entry point for the SciBorg robot application.
////////////////////////////////////////////////////////////////////////////////
class SciBorgRobot : public Psb::IterativeRobot
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Default Constructor
      //////////////////////////////////////////////////////////////////////////
      SciBorgRobot(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~SciBorgRobot(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void RobotInit(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void DisabledInit(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void DisabledPeriodic(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void AutonomousInit(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void AutonomousPeriodic(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void TeleopInit(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void TeleopPeriodic(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void TestInit(void);

      //////////////////////////////////////////////////////////////////////////
      /// @todo Documentation
      //////////////////////////////////////////////////////////////////////////
      virtual void TestPeriodic(void);

   private:
      Psb::Subsystem::Autonomous::Controller* mp_autonomousController;
      Psb::Subsystem::Dashstream::Controller* mp_dashstreamController;

      std::list<Psb::Subsystem::SubsystemContainer *> m_subsystems;

   private:
      DISALLOW_COPY_CTOR(SciBorgRobot);
      DISALLOW_ASSIGNMENT_OPER(SciBorgRobot);

}; // END class SciBorgRobot
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

