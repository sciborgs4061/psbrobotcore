// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

#ifndef __IterativeRobot_h__
#define __IterativeRobot_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "Timer.h"
#include "RobotBase.h"
#include "PsbRobotDashstreamInterface.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {

class IterativeRobot : public RobotBase
{
   public:
      static constexpr double kDefaultPeriod = 0.0;

      virtual void StartCompetition();

      virtual void RobotInit();
      virtual void DisabledInit();
      virtual void AutonomousInit();
      virtual void TeleopInit();
      virtual void TestInit();

      virtual void DisabledPeriodic();
      virtual void AutonomousPeriodic();
      virtual void TeleopPeriodic();
      virtual void TestPeriodic();

      Psb::Subsystem::Dashstream::BaseInterface_I *getDashstreamInterface(void);

   protected:
      virtual void Prestart();

      virtual ~IterativeRobot();
      IterativeRobot();

   private:
      bool m_disabledInitialized;
      bool m_autonomousInitialized;
      bool m_teleopInitialized;
      bool m_testInitialized;

      Psb::Subsystem::PSBRobot::DashstreamInterface *mp_dashstreamIntf;
      Psb::Subsystem::PSBRobot::DashstreamInterface::DashstreamPOD_t *m_data;

}; // END class IterativeRobot

} // END namespace Psb

#endif // __PsbIterativeRobot_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE 
// /////////////////////////////////////////////////////////////////////////////
