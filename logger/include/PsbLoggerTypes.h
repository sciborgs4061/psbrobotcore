// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLoggerTypes_h__
#define __PsbLoggerTypes_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Defines the logging levels used in the system
////////////////////////////////////////////////////////////////////////////////
typedef enum : uint8_t
{
   LOG_LEVEL_ALWAYS = 0,  ///< Logs at this level will always be posted
   LOG_LEVEL_ERROR,       ///< Indicates an error condition - something is wrong
   LOG_LEVEL_WARNING,     ///< Indicates a warning condition - something is fishy
   LOG_LEVEL_NOTICE,      ///< Notice to developers - maybe something is wrong
   LOG_LEVEL_INFO,        ///< Informational - nothing wrong, just interesting data
   LOG_LEVEL_DEBUG1,      ///< Terse debugging info
   LOG_LEVEL_DEBUG2,      ///< Verbose debugging info
   LOG_LEVEL_DEBUG3,      ///< VERY Verbose debugging info
   
   // This is not a log level, this is just a counter
   NUM_LOG_LEVELS
} log_level_t;


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Defines the various types that will be logged.
////////////////////////////////////////////////////////////////////////////////
typedef enum : uint8_t
{
   // Scalar quantities, called with (TYPE, VALUE)
   LOG_TYPE_CHAR = 0,
   LOG_TYPE_BOOL,
   LOG_TYPE_UINT8,
   LOG_TYPE_UINT16,
   LOG_TYPE_UINT32,
   LOG_TYPE_HEX8,
   LOG_TYPE_HEX16,
   LOG_TYPE_HEX32,
   LOG_TYPE_INT8,
   LOG_TYPE_INT16,
   LOG_TYPE_INT32,
   LOG_TYPE_FLOAT32,
   LOG_TYPE_FLOAT64,
   LOG_TYPE_STRING,

   // Vector/Array quantities, called with (TYPE, SIZEOF(ARRAY), ARRAY)
   LOG_TYPE_CHAR_A,
   LOG_TYPE_BOOL_A,
   LOG_TYPE_UINT8_A,
   LOG_TYPE_UINT16_A,
   LOG_TYPE_UINT32_A,
   LOG_TYPE_HEX8_A,
   LOG_TYPE_HEX16_A,
   LOG_TYPE_HEX32_A,
   LOG_TYPE_INT8_A,
   LOG_TYPE_INT16_A,
   LOG_TYPE_INT32_A,
   LOG_TYPE_FLOAT32_A,
   LOG_TYPE_FLOAT64_A,
   /// @todo LOG_TYPE_STRING_A,
   
   // This is not a log data type, this is just a counter
   NUM_LOG_TYPES
} log_type_t;


} // END namespace Psb


#endif // __PsbLoggerTypes_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

