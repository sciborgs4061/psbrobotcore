// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLogDataUdpSender_h__
#define __PsbLogDataUdpSender_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <PsbMacros.h>
#include "PsbLogDataSender_I.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { class LogDataBundle; }
struct sockaddr_in;


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a single point of access for posting debug log data.
////////////////////////////////////////////////////////////////////////////////
class LogDataUdpSender : public LogDataSender_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] dstIpv4Addr   Destination network address - IPv4
      /// @param[in] dstUdpPort    Destination UDP Port Number
      //////////////////////////////////////////////////////////////////////////
      LogDataUdpSender(uint32_t dstIpv4Addr,
                       uint16_t dstUdpPort);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~LogDataUdpSender(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::LogDataSender_I::send()
      ///
      /// @remarks
      /// This implementation of send() will forward the log data over a UDP
      /// network socket to the destination IP:port provided in the CTOR.
      //////////////////////////////////////////////////////////////////////////
      virtual void send(LogDataBundle& r_logData);

   private:
      int                 m_socketDescriptor;
      struct sockaddr_in* mp_dstAddress;

   private:
      DISALLOW_DEFAULT_CTOR(LogDataUdpSender);
      DISALLOW_COPY_CTOR(LogDataUdpSender);
      DISALLOW_ASSIGNMENT_OPER(LogDataUdpSender);
}; // END class LogDataUdpSender


} // END Psb Namespace


#endif // __PsbLogDataUdpSender_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

