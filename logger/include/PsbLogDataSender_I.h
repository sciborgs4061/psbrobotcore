// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLogDataSender_I_h__
#define __PsbLogDataSender_I_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { class LogDataBundle; }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a single point of access for posting debug log data.
////////////////////////////////////////////////////////////////////////////////
class LogDataSender_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~LogDataSender_I(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function takes a reference to a LogDataBundle object and sends
      /// its contents to the proper destination.
      ///
      /// @remarks
      /// Depending on the implementation of the derived Sender class, log data
      /// might be saved in a file on disk, transmitted over a network socket,
      /// etc.
      ///
      /// @param[in] r_logData   Reference to a log data bundle
      //////////////////////////////////////////////////////////////////////////
      virtual void send(LogDataBundle& r_logData) = 0;

   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default constructor - called only by children
      //////////////////////////////////////////////////////////////////////////
      LogDataSender_I(void);

   private:
      DISALLOW_COPY_CTOR(LogDataSender_I);
      DISALLOW_ASSIGNMENT_OPER(LogDataSender_I);
}; // END class LogDataSender_I


} // END Psb Namespace


#endif // __PsbLogDataSender_I_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

