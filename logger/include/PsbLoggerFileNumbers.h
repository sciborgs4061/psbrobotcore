// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLoggerFileNumbers_h__
#define __PsbLoggerFileNumbers_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>


// /////////////////////////////////////////////////////////////////////////////
// File Number Base Defines for Repos (top 16 bits)
// /////////////////////////////////////////////////////////////////////////////
#define AUTONOMOUS       (1u  << 16)
// #define COLLECTOR        (2u  << 16)
#define DASHSTREAM       (3u  << 16)
// #define DRIVEBASE        (4u  << 16)
#define SSTEST1          (5u  << 16) 
#define PSBXML           (6u  << 16)
// #define PUSHER           (7u  << 16)
// #define GRABBER          (8u  << 16)
// #define SMARTLED         (9u  << 16)
#define SUBSYSTEM_COMMON (10u << 16)
#define WPILIB           (11u << 16)


// /////////////////////////////////////////////////////////////////////////////
// Defines for Files (top 16 bits)
// /////////////////////////////////////////////////////////////////////////////
#define ACTUATOR_INTERFACE      (1u)
#define AUX_DATA_INTERFACE      (2u)
#define CONFIGURATION_INTERFACE (3u)
#define CONTROLLER              (4u)
#define OPERATOR_INTERFACE      (5u)
#define SENSOR_INTERFACE        (6u)

// All other files not part of the "common" design pattern number from here
#define EXTRA_FILE              (7u)


#endif // __PsbLoggerFileNumbers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

