// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLogDataBundle_h__
#define __PsbLogDataBundle_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <cstddef>
#include <ctime>
#include <string>
#include "PsbMacros.h"
#include "PsbLoggerTypes.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @todo
/// DOCUMENTATION
////////////////////////////////////////////////////////////////////////////////
struct __attribute__ ((__packed__)) LogDataHeader
{
   uint32_t  ts_whole;       ///< The whole integer seconds of the timestamp
   uint32_t  ts_fractional;  ///< The fractional piece of the timestamp (nsec)
   uint32_t  seq_num;        ///< The sequence number of this bundle - start @ 0
   uint16_t  log_level;      ///< Logging level
   uint32_t  file_num;       ///< Posting file number
   uint32_t  line_num;       ///< Posting line number
   uint16_t  element_count;  ///< Number of data elements in the log
};


////////////////////////////////////////////////////////////////////////////////
/// @todo
/// DOCUMENTATION
////////////////////////////////////////////////////////////////////////////////
struct __attribute__ ((__packed__)) DataElementDescriptor
{
   log_type_t data_type;
   uint16_t   data_size;
   void*      p_data;
};


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a convenient wrapper for log data that will be sent as
/// binary data to some destination.
////////////////////////////////////////////////////////////////////////////////
class LogDataBundle
{
   public:
      static const size_t BUFFER_SIZE  = 1400u;
      static const size_t MAX_ELEMENTS = (BUFFER_SIZE - sizeof(LogDataHeader)) / 4u;

   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] timeStamp       Time that the log was posted
      /// @param[in] sequenceNumber  A sequence number for tracking logs
      /// @param[in] logLevel        The logging level for this entry
      /// @param[in] fileNumber      The file number from which this log was posted
      /// @param[in] lineNumber      The file number at which this log was posted
      //////////////////////////////////////////////////////////////////////////
      LogDataBundle(struct timespec timeStamp,
                    uint32_t        sequenceNumber,
                    log_level_t     logLevel,
                    uint32_t        fileNumber,
                    uint32_t        lineNumber);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor - By Existing Byte Array
      ///
      /// @remarks
      /// This constructor is intended to be used by the receiving end of log
      /// data bundles for reconstruction & display.
      ///
      /// @param[in] pc_data   Pointer to data that is a LogDataBundle
      /// @param[in] length    Size of said data in bytes
      //////////////////////////////////////////////////////////////////////////
      LogDataBundle(uint8_t const* pc_data,
                    size_t         length);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Copy Constructor
      ///
      /// @param[in] rc_rhs   Reference to "that" object to copy from
      //////////////////////////////////////////////////////////////////////////
      LogDataBundle(LogDataBundle const& rc_rhs);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Assignment Operator
      ///
      /// @param[in] rc_rhs   Reference to "that" object to assign from
      //////////////////////////////////////////////////////////////////////////
      LogDataBundle& operator=(LogDataBundle const& rc_rhs);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~LogDataBundle(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns a pointer to the header for this LogDataBundle
      //////////////////////////////////////////////////////////////////////////
      LogDataHeader* getLogDataHeader(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the number of bytes used in the header
      //////////////////////////////////////////////////////////////////////////
      size_t getLogDataHeaderSize(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns a pointer to the binary data in this bundle
      //////////////////////////////////////////////////////////////////////////
      uint8_t* getDataBuffer(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the number of bytes used in the data buffer
      //////////////////////////////////////////////////////////////////////////
      size_t getDataBufferSize(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns access to the requested data element
      ///
      /// @param[in] index   The index of the data element to access (0-based)
      //////////////////////////////////////////////////////////////////////////
      DataElementDescriptor getLogDataElementByIndex(size_t index);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Packs the provided data into the log data buffer (appending).
      ///
      /// @param[in] data_type  The type (logger type) of data provided
      /// @param[in] data_size  Size of the provided data, in bytes
      /// @param[in] pc_data    Pointer to the provided data
      ///
      /// @retval true   Data successfully added to the buffer
      /// @retval false  Buffer unmodified - no room to contain given data
      //////////////////////////////////////////////////////////////////////////
      bool packData(log_type_t      data_type,
                    size_t          data_size,
                    uint8_t const*  pc_data);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Converts the log data into a human-readable, one-line string.
      //////////////////////////////////////////////////////////////////////////
      std::string toString(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This buffer holds all binary data for the log data bundle.
      //////////////////////////////////////////////////////////////////////////
      uint8_t m_logDataBuffer[BUFFER_SIZE];

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This array holds pointers to each data element for fast lookup
      //////////////////////////////////////////////////////////////////////////
      uint8_t* m_dataElementLocations[MAX_ELEMENTS];

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Index into the log data buffer where the next byte will be written.
      //////////////////////////////////////////////////////////////////////////
      uint8_t* mp_writePtr;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Pointer to the LogDataHeader for this bundle
      //////////////////////////////////////////////////////////////////////////
      LogDataHeader* mp_logHeader;

      /// @todo Documentation
      struct timespec m_timestamp;
      uint32_t        m_sequenceNum;
      log_level_t     m_logLevel;
      uint32_t        m_fileNum;
      uint32_t        m_lineNum;
      uint8_t         m_elementCount;


   private:
      DISALLOW_DEFAULT_CTOR(LogDataBundle);
}; // END class LogDataBundle


} // END Psb Namespace


#endif // __PsbLogDataBundle_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

