// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLogger_h__
#define __PsbLogger_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <PsbMacros.h>
#include "PsbLoggerTypes.h"
#include "PsbLoggerFileNumbers.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { class LogDataSender_I; }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a single point of access for posting debug log data.
////////////////////////////////////////////////////////////////////////////////
class Logger
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~Logger(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function returns the current LogDataSender_I object in use.
      //////////////////////////////////////////////////////////////////////////
      static LogDataSender_I* getDataSender(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function sets the current LogDataSender_I object to use when
      /// sending debug logs.
      //////////////////////////////////////////////////////////////////////////
      static void setDataSender(LogDataSender_I* p_sender);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Singleton access function
      //////////////////////////////////////////////////////////////////////////
      static Logger* instance(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function tells the caller if logs from the given FILE_NUM and 
      /// logging level should be posted.
      ///
      /// @param[in] c_logLevel     Logging level (ERROR, DEBUG1, etc.)
      /// @param[in] c_fileNumber   File number from the calling file
      //////////////////////////////////////////////////////////////////////////
      bool isLoggingEnabled(const log_level_t c_logLevel,
                            const uint32_t    c_fileNumber);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function posts debug log data provided by the caller via a
      /// variadic argument list.
      ///
      /// @param[in] c_logLevel     Logging level (ERROR, DEBUG1, etc.)
      /// @param[in] c_fileNumber   File number from the calling file
      /// @param[in] c_lineNumber   Line number where the log was posted from
      /// @param[in] c_numEntries   Number of data elements in the event
      //////////////////////////////////////////////////////////////////////////
      void postEvent(const log_level_t c_logLevel,
                     const uint32_t    c_fileNumber,
                     const uint32_t    c_lineNumber,
                     const uint32_t    c_numEntries,
                     ...);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default constructor - hidden from view
      //////////////////////////////////////////////////////////////////////////
      Logger(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Reference to the LogDataSender_I object that is implementing "send".
      //////////////////////////////////////////////////////////////////////////
      static LogDataSender_I* msp_sender;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Single instance of the Logger class.
      //////////////////////////////////////////////////////////////////////////
      static Logger* msp_instance;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This tracks the number of logs posted, and will be used as a sequence
      /// number in the LogDataBundle objects that get created while logging.
      //////////////////////////////////////////////////////////////////////////
      uint32_t m_numLogsPosted;

   private:
      DISALLOW_COPY_CTOR(Logger);
      DISALLOW_ASSIGNMENT_OPER(Logger);
}; // END class Logger


} // END Psb Namespace


// /////////////////////////////////////////////////////////////////////////////
// Change these to 1's to enable, 0's to disable
// /////////////////////////////////////////////////////////////////////////////
#define ERROR_LOGGING_ENABLED   1
#define WARNING_LOGGING_ENABLED 1
#define NOTICE_LOGGING_ENABLED  1
#define INFO_LOGGING_ENABLED    1
#define DEBUG1_LOGGING_ENABLED  1
#define DEBUG2_LOGGING_ENABLED  1
#define DEBUG3_LOGGING_ENABLED  1


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#define PSB_LOG_ALWAYS(numEntries, ...)                                        \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_ALWAYS, FILE_NUM)) \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_ALWAYS,                            \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != ERROR_LOGGING_ENABLED
#define PSB_LOG_ERROR(numEntries, ...)                                         \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_ERROR, FILE_NUM))  \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_ERROR,                             \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)
#else
#define PSB_LOG_ERROR(numEntries, ...) do { } while (0)
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != WARNING_LOGGING_ENABLED
#define PSB_LOG_WARNING(numEntries, ...)                                        \
   do                                                                           \
   {                                                                            \
      Psb::Logger* p_logger = Psb::Logger::instance();                          \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_WARNING, FILE_NUM)) \
      {                                                                         \
         p_logger->postEvent(Psb::LOG_LEVEL_WARNING,                            \
                             FILE_NUM,                                          \
                             __LINE__,                                          \
                             numEntries,                                        \
                             ##__VA_ARGS__);                                    \
      }                                                                         \
   } while(false)
#else
#define PSB_LOG_WARNING(numEntries, ...) do { } while (0)
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != NOTICE_LOGGING_ENABLED
#define PSB_LOG_NOTICE(numEntries, ...)                                        \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_NOTICE, FILE_NUM)) \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_NOTICE,                            \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)
#else
#define PSB_LOG_NOTICE(numEntries, ...) do { } while (0)
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != INFO_LOGGING_ENABLED
#define PSB_LOG_INFO(numEntries, ...)                                          \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_INFO, FILE_NUM))   \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_INFO,                              \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)
#else
#define PSB_LOG_INFO(numEntries, ...) do { } while (0)
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != DEBUG1_LOGGING_ENABLED
#define PSB_LOG_DEBUG1(numEntries, ...)                                        \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_DEBUG1, FILE_NUM)) \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_DEBUG1,                            \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)
#else
#define PSB_LOG_DEBUG1(numEntries, ...) do { } while (0)
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != DEBUG2_LOGGING_ENABLED
#define PSB_LOG_DEBUG2(numEntries, ...)                                        \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_DEBUG2, FILE_NUM)) \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_DEBUG2,                            \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)
#else
#define PSB_LOG_DEBUG2(numEntries, ...) do { } while (0)
#endif


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
#if 0 != DEBUG3_LOGGING_ENABLED
#define PSB_LOG_DEBUG3(numEntries, ...)                                        \
   do                                                                          \
   {                                                                           \
      Psb::Logger* p_logger = Psb::Logger::instance();                         \
      if (true == p_logger->isLoggingEnabled(Psb::LOG_LEVEL_DEBUG3, FILE_NUM)) \
      {                                                                        \
         p_logger->postEvent(Psb::LOG_LEVEL_DEBUG3,                            \
                             FILE_NUM,                                         \
                             __LINE__,                                         \
                             numEntries,                                       \
                             ##__VA_ARGS__);                                   \
      }                                                                        \
   } while(false)
#else
#define PSB_LOG_DEBUG3(numEntries, ...) do { } while (0)
#endif



#endif // __PsbLogger_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

