// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstddef>
#include <cerrno>
#include <cassert>
#include <cstring>
#include <string>
#include <cstdio>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include "PsbLogDataUdpSender.h"
#include "PsbLogDataBundle.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
LogDataUdpSender::LogDataUdpSender(uint32_t dstIpv4Addr,
                                   uint16_t dstUdpPort)
   : LogDataSender_I()
   , m_socketDescriptor(-1)
   , mp_dstAddress(NULL)
{
   // Create a UDP socket for sending
   m_socketDescriptor = ::socket(AF_INET,
                                 SOCK_DGRAM,
                                 IPPROTO_UDP);
   assert(-1 != m_socketDescriptor);
                                 
   // This socket should not be able to receive data
   ::shutdown(m_socketDescriptor, SHUT_RD);
   
   ///x // Allow broadcast sending on this socket if not enabled by default
   ///x char bcast_flag = 1;
   ///x ::setsockopt(m_socketDescriptor,
   ///x              SOL_SOCKET,
   ///x              SO_BROADCAST,
   ///x              &bcast_flag,
   ///x              sizeof(bcast_flag));
                
   // We will be sending data to the unicast address (ms_sendAddress:ms_sendPort)
   mp_dstAddress = new struct sockaddr_in;
   ::memset(mp_dstAddress, 0x00, sizeof(struct sockaddr_in));
   mp_dstAddress->sin_family      = AF_INET;
   mp_dstAddress->sin_port        = htons(dstUdpPort);
   mp_dstAddress->sin_addr.s_addr = htonl(dstIpv4Addr);
}


////////////////////////////////////////////////////////////////////////////////
LogDataUdpSender::~LogDataUdpSender(void)
{
   // Free up networking resources
   ::close(m_socketDescriptor);
   m_socketDescriptor = -1;
   delete mp_dstAddress;
   mp_dstAddress = NULL;
}


////////////////////////////////////////////////////////////////////////////////
void
LogDataUdpSender::send(LogDataBundle& r_logData)
{
   int send_status =
      ::sendto(m_socketDescriptor,
               r_logData.getDataBuffer(),
               r_logData.getDataBufferSize(),
               0,
               (struct sockaddr*) mp_dstAddress,
               sizeof(*mp_dstAddress));
               
   // Check if network send failed
   if (-1 == send_status)
   {
      send_status = send_status;
      // int local_errno = errno;
      // ::printf("SENDTO ERROR... %s (%d)\n",
      //          strerror(local_errno),
      //          local_errno);
   }

   return;
}


} // END Psb namespace


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

