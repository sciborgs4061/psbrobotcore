// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstddef>
#include <cstdio>
#include <cstdarg>
#include <cstring>
#include <ctime>
#include <string>
#include "PsbLogger.h"
#include "PsbLogDataBundle.h"
#include "PsbLogDataNullSender.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


// /////////////////////////////////////////////////////////////////////////////
// Static Data Initialization
// /////////////////////////////////////////////////////////////////////////////
static LogDataNullSender sv_nullSender;
LogDataSender_I*         Logger::msp_sender   = &sv_nullSender;
Logger*                  Logger::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
Logger::Logger(void)
   : m_numLogsPosted(0u)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
Logger::~Logger(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
LogDataSender_I*
Logger::getDataSender(void)
{
   return msp_sender;
}


////////////////////////////////////////////////////////////////////////////////
void
Logger::setDataSender(LogDataSender_I* p_sender)
{
   if (NULL != p_sender)
   {
      msp_sender = p_sender;
   }
   return;
}


////////////////////////////////////////////////////////////////////////////////
Logger*
Logger::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new Logger();
   }
   return msp_instance;
}


////////////////////////////////////////////////////////////////////////////////
bool
Logger::isLoggingEnabled(const log_level_t /* c_logLevel */,
                         const uint32_t    /* c_fileNumber */)
{
   /// @todo
   /// Actually implement this!  For now, just return true.
   return true;
}


////////////////////////////////////////////////////////////////////////////////
void
Logger::postEvent(const log_level_t c_logLevel,
                  const uint32_t    c_fileNumber,
                  const uint32_t    c_lineNumber,
                  const uint32_t    c_numEntries,
                  ...)
{
   ///
   /// @todo
   /// We should check num_entries for some sane limit...
   /// Perhaps MAX_ENTRIES = BUFFER_SIZE / (1 + 2 + 1)
   ///                   data type byte ----^   ^   ^
   ///                   data size word --------|   |
   ///                   1-byte data    ------------/
   ///

   // Grab a timestamp as early as possibly
   struct timespec timestamp = { 0, 0 };
   clock_gettime(CLOCK_MONOTONIC, &timestamp);

   // Stack-local container for binary log data
   LogDataBundle bundle(timestamp,
                        m_numLogsPosted,
                        c_logLevel,
                        c_fileNumber,
                        c_lineNumber);
   m_numLogsPosted += 1;

   // Init the arg list
   va_list arg_list;
   va_start(arg_list, c_numEntries);

   uint32_t entry_idx    = 0;
   bool     is_more_room = true;
   while ((entry_idx  < c_numEntries) &&
          (true      == is_more_room))
   {
      // Grab the logger data type from the arg list
      log_type_t data_type = static_cast<log_type_t>(va_arg(arg_list, int));
      size_t     data_size = SIZE_MAX;
      uint8_t*   p_data    = NULL;

      // Current loop iteration data placeholders, used at the end
      int    gen_scalar_int_data    = 0;
      float  gen_scalar_float_data  = 0.0f;
      double gen_scalar_double_data = 0.0;
      int    array_length           = -1;
      int    array_pointer          = -1;

      // Act according to the current data type
      switch (data_type)
      {
         case LOG_TYPE_CHAR:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(char);
         } break;

         case LOG_TYPE_BOOL:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(uint8_t);
         } break;

         case LOG_TYPE_UINT8:
         case LOG_TYPE_HEX8:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(uint8_t);
         } break;

         case LOG_TYPE_UINT16:
         case LOG_TYPE_HEX16:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(uint16_t);
         } break;

         case LOG_TYPE_UINT32:
         case LOG_TYPE_HEX32:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(uint32_t);
         } break;

         case LOG_TYPE_INT8:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(int8_t);
         } break;

         case LOG_TYPE_INT16:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(int16_t);
         } break;

         case LOG_TYPE_INT32:
         {
            // Dig out the data
            gen_scalar_int_data = va_arg(arg_list, int);
            p_data              = reinterpret_cast<uint8_t*>(&gen_scalar_int_data);
            data_size           = sizeof(int32_t);
         } break;

         case LOG_TYPE_FLOAT32:
         {
            // Dig out the data
            gen_scalar_float_data = static_cast<float>(va_arg(arg_list, double));
            p_data                = reinterpret_cast<uint8_t*>(&gen_scalar_float_data);
            data_size             = sizeof(float);
         } break;

         case LOG_TYPE_FLOAT64:
         {
            // Dig out the data
            gen_scalar_double_data = va_arg(arg_list, double);
            p_data                 = reinterpret_cast<uint8_t*>(&gen_scalar_double_data);
            data_size              = sizeof(double);
         } break;

         case LOG_TYPE_STRING:
         {
            // Dig out the data (string is "pseudo" scalar)
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(array_pointer);
            data_size     = strlen(reinterpret_cast<char const*>(p_data)) + 1; // NULL-term
         } break;

         case LOG_TYPE_CHAR_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(char));
         } break;

         case LOG_TYPE_BOOL_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(uint8_t));
         } break;

         case LOG_TYPE_UINT8_A:
         case LOG_TYPE_HEX8_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(uint8_t));
         } break;

         case LOG_TYPE_UINT16_A:
         case LOG_TYPE_HEX16_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(uint16_t));
         } break;

         case LOG_TYPE_UINT32_A:
         case LOG_TYPE_HEX32_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(uint32_t));
         } break;

         case LOG_TYPE_INT8_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(int8_t));
         } break;

         case LOG_TYPE_INT16_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(int16_t));
         } break;

         case LOG_TYPE_INT32_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(int32_t));
         } break;

         case LOG_TYPE_FLOAT32_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(float));
         } break;

         case LOG_TYPE_FLOAT64_A:
         {
            // Dig out the data
            array_length  = va_arg(arg_list, int);
            array_pointer = va_arg(arg_list, int);
            p_data        = reinterpret_cast<uint8_t*>(&array_pointer);
            data_size     = static_cast<size_t>(array_length * sizeof(double));
         } break;

         default:
         {
            // Unsupported type - we must have lost sync somewhere
            entry_idx = c_numEntries;
         } break;
      } // END switch (data_type)

      // Pack the data buffer, so long as we aren't in error
      if (entry_idx < c_numEntries)
      {
         is_more_room = bundle.packData(data_type, data_size, p_data);
         entry_idx += 1;
      }
   } // END while

   // Close the va-arg list
   va_end(arg_list);

   // Push the data to the sender for routing to the appropriate destination.
   msp_sender->send(bundle);

   return;
} // END function postEvent()


} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

