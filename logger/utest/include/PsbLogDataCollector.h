// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbLogDataCollector_h__
#define __PsbLogDataCollector_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <vector>
#include <PsbMacros.h>
#include "PsbLogDataSender_I.h"
#include "PsbLogDataBundle.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a single point of access for posting debug log data.
////////////////////////////////////////////////////////////////////////////////
class LogDataCollector : public LogDataSender_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default constructor
      //////////////////////////////////////////////////////////////////////////
      LogDataCollector(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~LogDataCollector(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::LogDataSender_I::send()
      ///
      /// @remarks
      /// This implementation of send() will simply return.  The incoming data
      /// is flat-out ignored and no action is taken.
      //////////////////////////////////////////////////////////////////////////
      virtual void send(LogDataBundle& r_logData);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function will query the log collector and expect the indicated
      /// number of log entries to be present, and optionally flushing the log
      /// collector.
      ///
      /// @param[in] numExpectedLogs   Number of expected log entries
      /// @param[in] flushFlag         If true, log collector will be drained
      //////////////////////////////////////////////////////////////////////////
      bool verifyLogCount(int  numExpectedLogs,
                          bool flushFlag);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function will query the log collector and expect the indicated
      /// number of log entries, for the given level, to be present, and
      /// optionally flushing the log collector.
      ///
      /// @param[in] logLevel          Level of logs to look at
      /// @param[in] numExpectedLogs   Number of expected log entries
      /// @param[in] flushFlag         If true, log collector will be drained
      //////////////////////////////////////////////////////////////////////////
      bool verifyLogCountForLevel(log_level_t logLevel,
                                  int         numExpectedLogs,
                                  bool        flushFlag);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function will drop all collected logs, unconditionally.
      //////////////////////////////////////////////////////////////////////////
      void clearAllLogs(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This function will dump all log data content to stdout
      //////////////////////////////////////////////////////////////////////////
      void printAllLogs(void);

   private:
      std::vector<LogDataBundle> m_capturedLogs;

   private:
      DISALLOW_COPY_CTOR(LogDataCollector);
      DISALLOW_ASSIGNMENT_OPER(LogDataCollector);
}; // END class LogDataCollector


} // END Psb Namespace


#endif // __PsbLogDataCollector_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

