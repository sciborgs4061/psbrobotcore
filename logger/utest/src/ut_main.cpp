#include <gtest/gtest.h>
#include "PsbLoggerTestEnvironment.h"

int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);

  // Register global environments
  ::testing::AddGlobalTestEnvironment(new PsbLoggerTestEnvironment());

  return RUN_ALL_TESTS();
}

