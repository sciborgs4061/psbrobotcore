// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <list>
#include <gtest/gtest.h>
#include "PsbLogDataBundle.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM 0x1234ABCD
#define LINE_NUM 200u


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies expected API interaction after basic object construction.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBasicConstruction)
{
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Verify the contents of the header
   EXPECT_NE(nullptr,               bundle.getLogDataHeader());
   EXPECT_EQ(123,                   bundle.getLogDataHeader()->ts_whole);
   EXPECT_EQ(456,                   bundle.getLogDataHeader()->ts_fractional);
   EXPECT_EQ(3,                     bundle.getLogDataHeader()->seq_num);
   EXPECT_EQ(Psb::LOG_LEVEL_ALWAYS, bundle.getLogDataHeader()->log_level);
   EXPECT_EQ(FILE_NUM,              bundle.getLogDataHeader()->file_num);
   EXPECT_EQ(LINE_NUM,              bundle.getLogDataHeader()->line_num);
   EXPECT_EQ(0,                     bundle.getLogDataHeader()->element_count);

   // Now access the entire raw data buffer
   EXPECT_NE(nullptr,                    bundle.getDataBuffer());
   EXPECT_EQ(sizeof(Psb::LogDataHeader), bundle.getDataBufferSize());

   /// @todo check access to data elements

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies expected API interaction after object construction from
/// the raw data bytes of a previously-constructed object.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkRawByteArrayConstruction)
{
   // The original object
   Psb::LogDataBundle bundle_src({123,456},
                                 3,
                                 Psb::LOG_LEVEL_ALWAYS,
                                 FILE_NUM,
                                 LINE_NUM);

   // The secondary object
   Psb::LogDataBundle bundle_dst(bundle_src.getDataBuffer(),
                                 bundle_src.getDataBufferSize());

   // Check that the header was copied correctly
   EXPECT_NE(nullptr, bundle_src.getLogDataHeader());
   EXPECT_NE(nullptr, bundle_dst.getLogDataHeader());
   EXPECT_NE(bundle_src.getLogDataHeader(),
             bundle_dst.getLogDataHeader());
   EXPECT_EQ(bundle_src.getLogDataHeaderSize(),
             bundle_dst.getLogDataHeaderSize());
   EXPECT_EQ(0, memcmp(bundle_src.getLogDataHeader(),
                       bundle_dst.getLogDataHeader(),
                       bundle_src.getLogDataHeaderSize()));

   // Check that the raw data buffer was copied correctly
   EXPECT_NE(nullptr, bundle_src.getDataBuffer());
   EXPECT_NE(nullptr, bundle_dst.getDataBuffer());
   EXPECT_NE(bundle_src.getDataBuffer(),
             bundle_dst.getDataBuffer());
   EXPECT_EQ(bundle_src.getDataBufferSize(),
             bundle_dst.getDataBufferSize());
   EXPECT_EQ(0, memcmp(bundle_src.getDataBuffer(),
                       bundle_dst.getDataBuffer(),
                       bundle_src.getDataBufferSize()));

   /// @todo verify the element access API

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies expected API interaction after object construction from
/// the copy-constructor
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkCopyConstruction)
{
   // The original object
   Psb::LogDataBundle bundle_src({123,456},
                                 3,
                                 Psb::LOG_LEVEL_ALWAYS,
                                 FILE_NUM,
                                 LINE_NUM);

   // The secondary object
   Psb::LogDataBundle bundle_dst(bundle_src);

   // Check that the header was copied correctly
   EXPECT_NE(nullptr, bundle_src.getLogDataHeader());
   EXPECT_NE(nullptr, bundle_dst.getLogDataHeader());
   EXPECT_NE(bundle_src.getLogDataHeader(),
             bundle_dst.getLogDataHeader());
   EXPECT_EQ(bundle_src.getLogDataHeaderSize(),
             bundle_dst.getLogDataHeaderSize());
   EXPECT_EQ(0, memcmp(bundle_src.getLogDataHeader(),
                       bundle_dst.getLogDataHeader(),
                       bundle_src.getLogDataHeaderSize()));

   // Check that the raw data buffer was copied correctly
   EXPECT_NE(nullptr, bundle_src.getDataBuffer());
   EXPECT_NE(nullptr, bundle_dst.getDataBuffer());
   EXPECT_NE(bundle_src.getDataBuffer(),
             bundle_dst.getDataBuffer());
   EXPECT_EQ(bundle_src.getDataBufferSize(),
             bundle_dst.getDataBufferSize());
   EXPECT_EQ(0, memcmp(bundle_src.getDataBuffer(),
                       bundle_dst.getDataBuffer(),
                       bundle_src.getDataBufferSize()));

   /// @todo verify the element access API

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies expected API interaction after assignment from another
/// object.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkAssignmentOperator)
{
   // The original object
   Psb::LogDataBundle bundle_src({123,456},
                                 3,
                                 Psb::LOG_LEVEL_ALWAYS,
                                 FILE_NUM,
                                 LINE_NUM);

   // The secondary object
   Psb::LogDataBundle bundle_dst({234,567},
                                 4,
                                 Psb::LOG_LEVEL_ERROR,
                                 FILE_NUM+1,
                                 LINE_NUM+1);

   // Now do the assignment
   bundle_dst = bundle_src;

   // Check that the header was copied correctly
   EXPECT_NE(nullptr, bundle_src.getLogDataHeader());
   EXPECT_NE(nullptr, bundle_dst.getLogDataHeader());
   EXPECT_NE(bundle_src.getLogDataHeader(),
             bundle_dst.getLogDataHeader());
   EXPECT_EQ(bundle_src.getLogDataHeaderSize(),
             bundle_dst.getLogDataHeaderSize());
   EXPECT_EQ(0, memcmp(bundle_src.getLogDataHeader(),
                       bundle_dst.getLogDataHeader(),
                       bundle_src.getLogDataHeaderSize()));

   // Check that the raw data buffer was copied correctly
   EXPECT_NE(nullptr, bundle_src.getDataBuffer());
   EXPECT_NE(nullptr, bundle_dst.getDataBuffer());
   EXPECT_NE(bundle_src.getDataBuffer(),
             bundle_dst.getDataBuffer());
   EXPECT_EQ(bundle_src.getDataBufferSize(),
             bundle_dst.getDataBufferSize());
   EXPECT_EQ(0, memcmp(bundle_src.getDataBuffer(),
                       bundle_dst.getDataBuffer(),
                       bundle_src.getDataBufferSize()));

   /// @todo verify the element access API

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies the output from toString() when there are no packed data
/// elements.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkToStringNoPackedDataElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Verify the string
   std::string out_str = "";
   out_str += "00000000123.000000456,";  // Time stamp
   out_str += "00000000003,";            // Sequence Number
   out_str += "ALWAYS,";                 // Level as string
   out_str += "0x1234abcd,";             // File Number (hex)
   out_str += "000200,";                 // Line Number (dec)
   out_str += "000";                     // Number of data elements packed (dec)
   EXPECT_EQ(out_str, bundle.toString());

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies the output from toString() when there is one packed data
/// element.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkToStringOnePackedDataElement)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   char data_element_char = 'Z';
   bundle.packData(Psb::LOG_TYPE_CHAR,
                   sizeof(data_element_char),
                   reinterpret_cast<uint8_t const*>(&data_element_char));
                   
   // Verify the string
   std::string out_str = "";
   out_str += "00000000123.000000456,";  // Time stamp
   out_str += "00000000003,";            // Sequence Number
   out_str += "ALWAYS,";                 // Level as string
   out_str += "0x1234abcd,";             // File Number (hex)
   out_str += "000200,";                 // Line Number (dec)
   out_str += "001,";                    // Number of data elements packed (dec)
   out_str += "Z";                       // 1st Data Element (char)
   EXPECT_EQ(out_str, bundle.toString());

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies the output from toString() when there are two packed data
/// elements.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkToStringTwoPackedDataElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   char data_element_char = 'Z';
   bundle.packData(Psb::LOG_TYPE_CHAR,
                   sizeof(data_element_char),
                   reinterpret_cast<uint8_t const*>(&data_element_char));

   uint16_t data_element_uint16 = 65534;
   bundle.packData(Psb::LOG_TYPE_UINT16,
                   sizeof(data_element_uint16),
                   reinterpret_cast<uint8_t const*>(&data_element_uint16));
                   
   // Verify the string
   std::string out_str = "";
   out_str += "00000000123.000000456,";  // Time stamp
   out_str += "00000000003,";            // Sequence Number
   out_str += "ALWAYS,";                 // Level as string
   out_str += "0x1234abcd,";             // File Number (hex)
   out_str += "000200,";                 // Line Number (dec)
   out_str += "002,";                    // Number of data elements packed (dec)
   out_str += "Z,";                      // 1st Data Element (char)
   out_str += "65534";                   // 2nd Data Element (uint16)
   EXPECT_EQ(out_str, bundle.toString());

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataHeader element_count field is updated
/// correctly each time a new data element is successfully packed into the
/// bundle.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkElementCountGrowsOnSuccessfulPack)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Verify the current count
   EXPECT_EQ(0, bundle.getLogDataHeader()->element_count);

   // There should be 0 elements in the bundle
   size_t  num_elements_packed    = 0;
   bool    was_packing_successful = true;
   uint8_t data_element           = UINT8_MAX;

   while (true == was_packing_successful)
   {
      // Pack a simple data element
      was_packing_successful = 
         bundle.packData(Psb::LOG_TYPE_UINT8,
                         sizeof(data_element),
                         reinterpret_cast<uint8_t const*>(&data_element));

      // Set the expectation based on the result of the packing
      num_elements_packed = 
         (true == was_packing_successful)
         ? num_elements_packed + 1
         : num_elements_packed;

      // Verify the count after the data packing attempt
      EXPECT_EQ(num_elements_packed, bundle.getLogDataHeader()->element_count);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that a caller can randomly access all data elements in
/// the LogDataBundle.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkGetDataElementByIndexBasic)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Pack a few data elements
   uint8_t  data_element_u8  = UINT8_MAX;
   uint16_t data_element_u16 = UINT16_MAX;
   uint32_t data_element_u32 = UINT32_MAX;
   bundle.packData(Psb::LOG_TYPE_UINT8,
                   sizeof(data_element_u8),
                   reinterpret_cast<uint8_t const*>(&data_element_u8));
   bundle.packData(Psb::LOG_TYPE_UINT16,
                   sizeof(data_element_u16),
                   reinterpret_cast<uint8_t const*>(&data_element_u16));
   bundle.packData(Psb::LOG_TYPE_UINT32,
                   sizeof(data_element_u32),
                   reinterpret_cast<uint8_t const*>(&data_element_u32));

   // There should be 3 elements in the bundle
   EXPECT_EQ(3, bundle.getLogDataHeader()->element_count);

   // Grab access to the data elements in a "random access" method
   Psb::DataElementDescriptor desc = bundle.getLogDataElementByIndex(1);
   EXPECT_EQ(Psb::LOG_TYPE_UINT16, desc.data_type);
   EXPECT_EQ(2u,                   desc.data_size);
   EXPECT_EQ(UINT16_MAX,           *(reinterpret_cast<uint16_t*>(desc.p_data)));

   desc = bundle.getLogDataElementByIndex(0);
   EXPECT_EQ(Psb::LOG_TYPE_UINT8, desc.data_type);
   EXPECT_EQ(1u,                  desc.data_size);
   EXPECT_EQ(UINT8_MAX,           *(reinterpret_cast<uint8_t*>(desc.p_data)));

   desc = bundle.getLogDataElementByIndex(2);
   EXPECT_EQ(Psb::LOG_TYPE_UINT32, desc.data_type);
   EXPECT_EQ(4u,                   desc.data_size);
   EXPECT_EQ(UINT32_MAX,           *(reinterpret_cast<uint32_t*>(desc.p_data)));

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that an invalid descriptor is returned when a data
/// element is requested by an out-of-range index.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkGetDataElementByIndexInvalid)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // There are no elements, so all descriptors should be invalid
   EXPECT_EQ(0, bundle.getLogDataHeader()->element_count);

   for (size_t index = 0; index < Psb::LogDataBundle::MAX_ELEMENTS; index += 1)
   {
      Psb::DataElementDescriptor desc = bundle.getLogDataElementByIndex(index);
      EXPECT_EQ(Psb::NUM_LOG_TYPES, desc.data_type);
      EXPECT_EQ(0u,                 desc.data_size);
      EXPECT_EQ(nullptr,            desc.p_data);
   }

   // Pack a data element
   uint8_t  data_element_u8  = UINT8_MAX;
   bundle.packData(Psb::LOG_TYPE_UINT8,
                   sizeof(data_element_u8),
                   reinterpret_cast<uint8_t const*>(&data_element_u8));

   // There should be 1 element in the bundle
   EXPECT_EQ(1, bundle.getLogDataHeader()->element_count);

   for (size_t index = 1; index < Psb::LogDataBundle::MAX_ELEMENTS; index += 1)
   {
      Psb::DataElementDescriptor desc = bundle.getLogDataElementByIndex(index);
      EXPECT_EQ(Psb::NUM_LOG_TYPES, desc.data_type);
      EXPECT_EQ(0u,                 desc.data_size);
      EXPECT_EQ(nullptr,            desc.p_data);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as data
/// elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthOneBytePackedScalarElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 1 byte plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_CHAR);
   log_type_list.push_back(Psb::LOG_TYPE_BOOL);
   log_type_list.push_back(Psb::LOG_TYPE_UINT8);
   log_type_list.push_back(Psb::LOG_TYPE_HEX8);
   log_type_list.push_back(Psb::LOG_TYPE_INT8);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 1 bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint8_t data_element_1B = 83;
      bundle.packData(lt,
                      sizeof(data_element_1B),
                      reinterpret_cast<uint8_t const*>(&data_element_1B));

      EXPECT_EQ(data_length + (1+2+1), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as data
/// elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthTwoBytePackedScalarElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 2 byte plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_UINT16);
   log_type_list.push_back(Psb::LOG_TYPE_HEX16);
   log_type_list.push_back(Psb::LOG_TYPE_INT16);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 2 bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint16_t data_element_2B = UINT16_MAX;
      bundle.packData(lt,
                      sizeof(data_element_2B),
                      reinterpret_cast<uint8_t const*>(&data_element_2B));

      EXPECT_EQ(data_length + (1+2+2), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as data
/// elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthFourBytePackedScalarElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 4 byte plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_UINT32);
   log_type_list.push_back(Psb::LOG_TYPE_HEX32);
   log_type_list.push_back(Psb::LOG_TYPE_INT32);
   log_type_list.push_back(Psb::LOG_TYPE_FLOAT32);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 4 bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint32_t data_element_4B = UINT32_MAX;
      bundle.packData(lt,
                      sizeof(data_element_4B),
                      reinterpret_cast<uint8_t const*>(&data_element_4B));

      EXPECT_EQ(data_length + (1+2+4), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as data
/// elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthEightBytePackedScalarElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 8 byte plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_FLOAT64);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 8 bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint64_t data_element_8B = UINT64_MAX;
      bundle.packData(lt,
                      sizeof(data_element_8B),
                      reinterpret_cast<uint8_t const*>(&data_element_8B));

      EXPECT_EQ(data_length + (1+2+8), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as data
/// elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthPackedStringElement)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Pack the string data
   std::string input_str = "This is a test string";
   bundle.packData(Psb::LOG_TYPE_STRING,
                   input_str.size(),
                   reinterpret_cast<uint8_t const*>(input_str.c_str()));

   EXPECT_EQ(data_length + (1+2+input_str.size()), bundle.getDataBufferSize());

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as array
/// data elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthOneBytePackedArrayElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 1 byte plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_CHAR_A);
   log_type_list.push_back(Psb::LOG_TYPE_BOOL_A);
   log_type_list.push_back(Psb::LOG_TYPE_UINT8_A);
   log_type_list.push_back(Psb::LOG_TYPE_HEX8_A);
   log_type_list.push_back(Psb::LOG_TYPE_INT8_A);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 1*N bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint8_t data_array_element_1B[3] =
         { UINT8_MAX, UINT8_MAX, UINT8_MAX };
      bundle.packData(lt,
                      sizeof(data_array_element_1B),
                      reinterpret_cast<uint8_t const*>(&data_array_element_1B));

      EXPECT_EQ(data_length + (1+2+3), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as array
/// data elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthTwoBytePackedArrayElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 2 bytes plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_UINT16_A);
   log_type_list.push_back(Psb::LOG_TYPE_HEX16_A);
   log_type_list.push_back(Psb::LOG_TYPE_INT16_A);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 2*N bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint16_t data_array_element_2B[3] =
         { UINT16_MAX, UINT16_MAX, UINT16_MAX };
      bundle.packData(lt,
                      sizeof(data_array_element_2B),
                      reinterpret_cast<uint8_t const*>(&data_array_element_2B));

      EXPECT_EQ(data_length + (1+2+6), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as array
/// data elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthFourBytePackedArrayElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 4 bytes plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_UINT32_A);
   log_type_list.push_back(Psb::LOG_TYPE_HEX32_A);
   log_type_list.push_back(Psb::LOG_TYPE_INT32_A);
   log_type_list.push_back(Psb::LOG_TYPE_FLOAT32_A);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 4*N bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint32_t data_array_element_4B[3] =
         { UINT32_MAX, UINT32_MAX, UINT32_MAX };
      bundle.packData(lt,
                      sizeof(data_array_element_4B),
                      reinterpret_cast<uint8_t const*>(&data_array_element_4B));

      EXPECT_EQ(data_length + (1+2+12), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the LogDataBundle size grows as expected as array
/// data elements are packed into it.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLogDataBundleUT, checkBundleGrowthEightBytePackedArrayElements)
{
   // The log bundle object
   Psb::LogDataBundle bundle({123,456},
                             3,
                             Psb::LOG_LEVEL_ALWAYS,
                             FILE_NUM,
                             LINE_NUM);

   // Each of these log types will allocate 8 bytes plus overhead in the bundle
   std::list<Psb::log_type_t> log_type_list;
   log_type_list.push_back(Psb::LOG_TYPE_FLOAT64_A);

   // Note the number of bytes used before packing any data
   size_t data_length = bundle.getDataBufferSize();

   // Verify each element increases size used by 1 + 2 + 8*N bytes
   for (auto lt : log_type_list)
   {
      // Actually bundle the data
      uint64_t data_array_element_8B[3] =
         { UINT64_MAX, UINT64_MAX, UINT64_MAX };
      bundle.packData(lt,
                      sizeof(data_array_element_8B),
                      reinterpret_cast<uint8_t const*>(&data_array_element_8B));

      EXPECT_EQ(data_length + (1+2+24), bundle.getDataBufferSize());

      // Note the current data use
      data_length = bundle.getDataBufferSize();
   }

   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

