// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <algorithm>
#include <functional>
#include <gtest/gtest.h>
#include "PsbLogDataCollector.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
LogDataCollector::LogDataCollector(void)
   : LogDataSender_I()
   , m_capturedLogs()
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
LogDataCollector::~LogDataCollector(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
LogDataCollector::send(LogDataBundle& r_logData)
{
   m_capturedLogs.push_back(r_logData);
   return;
}


////////////////////////////////////////////////////////////////////////////////
bool
LogDataCollector::verifyLogCount(int  numExpectedLogs,
                                 bool flushFlag)
{
   bool status = true;
   const int exp_count = numExpectedLogs;
   const int act_count = m_capturedLogs.size();

   SCOPED_TRACE(exp_count);
   SCOPED_TRACE(act_count);

   status = status && (exp_count == act_count);
   EXPECT_TRUE(status);

   // Dump logs if there was a problem
   if (false == status)
   {
      for (auto bundle : m_capturedLogs)
      {
         printf("\n%s", bundle.toString().c_str());
      }
   }

   if (true == flushFlag)
   {
      m_capturedLogs.clear();
   }

   return status;
}


////////////////////////////////////////////////////////////////////////////////
bool
LogDataCollector::verifyLogCountForLevel(log_level_t logLevel,
                                         int         numExpectedLogs,
                                         bool        flushFlag)
{
   // Delcare a function object for selecting logs of a certain level
   std::function<int(LogDataBundle&)> selector_func = 
      [logLevel] (LogDataBundle& r_bundle)
      {
         return (r_bundle.getLogDataHeader()->log_level == logLevel);
      }; 

   bool status = true;
   const int exp_count = numExpectedLogs;
   const int act_count = std::count_if(m_capturedLogs.begin(),
                                       m_capturedLogs.end(),
                                       selector_func);
   status = status && (exp_count == act_count);
   EXPECT_TRUE(status);

   // Dump logs if there was a problem
   if (false == status)
   {
      for (auto bundle : m_capturedLogs)
      {
         printf("\n%s", bundle.toString().c_str());
      }
   }

   // And clear them if we were asked to
   if (true == flushFlag)
   {
      m_capturedLogs.clear();
   }

   return status;
}


////////////////////////////////////////////////////////////////////////////////
void
LogDataCollector::clearAllLogs(void)
{
   m_capturedLogs.clear();
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
LogDataCollector::printAllLogs(void)
{
   for (auto bundle : m_capturedLogs)
   {
      printf("\n%s", bundle.toString().c_str());
   }
   return;
}



} // END Psb namespace


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

