// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include "PsbLogger.h"
#include "PsbLogDataCollector.h"

#ifdef FILE_NUM
   #undef FILE_NUM
#endif
#define FILE_NUM 0x1234ABCD


/// @todo Make a test case class with shared data...
/// See here:  code.google.com/p/googletest/wiki/AdvancedGuide#Sharing_Resources_Between_Tests_in_the_Same_Test_Case


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that we can create and destroy a logger singleton.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLoggerUT, checkBasicConstruction)
{
   // This is covered in the global setup environment
   Psb::LogDataCollector* p_collector = 
      reinterpret_cast<Psb::LogDataCollector*>(Psb::Logger::instance()->getDataSender());
   EXPECT_TRUE(p_collector->verifyLogCount(0, true));
   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that we can post basic data using the logging macros.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLoggerUT, checkBasicMacroUse)
{
   PSB_LOG_ALWAYS(1,  Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_ERROR(1,   Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_WARNING(1, Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_NOTICE(1,  Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_INFO(1,    Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_DEBUG1(1,  Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_DEBUG2(1,  Psb::LOG_TYPE_UINT8, 127);
   PSB_LOG_DEBUG3(1,  Psb::LOG_TYPE_UINT8, 127);
   Psb::LogDataCollector* p_collector = 
      reinterpret_cast<Psb::LogDataCollector*>(Psb::Logger::instance()->getDataSender());
   EXPECT_TRUE(p_collector->verifyLogCount(8, true));
   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLoggerUT, checkSingleStringLogging)
{
   // Always
   PSB_LOG_DEBUG3(1,
                  Psb::LOG_TYPE_STRING, "This Is Just A Test");
   Psb::LogDataCollector* p_collector = 
      reinterpret_cast<Psb::LogDataCollector*>(Psb::Logger::instance()->getDataSender());
   EXPECT_TRUE(p_collector->verifyLogCount(1, true));
   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// DOCUMENT ME!
////////////////////////////////////////////////////////////////////////////////
TEST(PsbLoggerUT, checkCharArrayLogging)
{
   Psb::LogDataCollector* p_collector = 
      reinterpret_cast<Psb::LogDataCollector*>(Psb::Logger::instance()->getDataSender());
   EXPECT_TRUE(p_collector->verifyLogCount(0, true));

   // Always
   PSB_LOG_DEBUG3(5,
                  Psb::LOG_TYPE_CHAR_A,  4, "This",
                  Psb::LOG_TYPE_CHAR_A,  2, "Is",
                  Psb::LOG_TYPE_CHAR_A,  4, "Just",
                  Psb::LOG_TYPE_CHAR_A,  1, "A",
                  Psb::LOG_TYPE_CHAR_A,  4, "Test");
   EXPECT_TRUE(p_collector->verifyLogCount(1, true));

   PSB_LOG_DEBUG3(1,
                  Psb::LOG_TYPE_CHAR_A,  19, "This Is Just A Test");
   EXPECT_TRUE(p_collector->verifyLogCount(1, true));

   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

