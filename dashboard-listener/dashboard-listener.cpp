#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <json/json.h>

#include <string>
#include <iostream>
using namespace std;

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

#define PORTNUM 5555
// #define HOSTNAME "localhost"

int main(int argc, char *argv[])
{
   if (argc != 2)
   {
      printf("Usage: %s ipaddr\n",argv[0]);
      return 1;
   }
    int sockfd, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
       error("ERROR opening socket");
    }
    server = gethostbyname(argv[1]);
    if (server == NULL)
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(PORTNUM);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
    {
        error("ERROR connecting");
    }

    //for (int x=0; x<10; x++)
    bool found_delim = false;
    std::string json_desc = "";

    while (!found_delim)
    {
       bzero(buffer,256);
       n = read(sockfd,buffer,255);
       if (n < 0)
       {
          error("ERROR reading from socket");
       }
       // this will exist in there somewhere...even if it
       // was cut in half in the read
       char *location = strstr(buffer,"=====");
       if (location != NULL)
       {
          // found the delimiter
          json_desc += buffer;
          // hack of the end of the string that has the delimiter
          // on it and then it should be valid JSON.
          // Then you can load it into JSONCpp and then start
          // ripping it apart.
          json_desc.erase(json_desc.find("====="));

          // printf("FOUND IT: \n---------------\n%s",json_desc.c_str());
          // printf("\n------------------\n");
          found_delim = true;
       }
       else
       {
          json_desc += buffer;
          // printf("still reading: bytes: %d  Value:%s\n",n,buffer);
       }
    }

    printf("%s",json_desc.c_str());
    // load the json description
    Json::Value root;
    Json::Reader reader;
    if (!reader.parse(json_desc,root))
    {
        error("Error parsing JSON");
    }

    // You can then find each of the data types in each
    // of the sections (in order) and rip the data
    // out of the stream and save it off.

    // I think the best thing to do is to know how big all
    // the data structure is (the whole blob of memory...and
    // how many pieces are in it.  Then you "overlay" memory maps into
    // a region of memory that is right-sized for this purpose...then
    // you simply take what was read in and write over that memory
    // where your overlay reads from.
    // I could make a class for each "type" that comes across that
    // will deal with the offsetting correctly (or at least help).
    // I can then walk the JSON structure and for each item I can
    // make the appropriate type object and give it the next chunk
    // of memory.  It will then tell me how far it will offset
    // so that can be used in the next instance of the class.
    // Then I will have a instance of a class (a data getter) that
    // has a pointer to some memory where the value is...and all
    // of the attributes from the JSON file.  Now we can take those
    // classes and render them in the GUI at some point.
    while (true)
    {
       bzero(buffer,256);
       n = read(sockfd,buffer,255);
       if (n < 0)
       {
          error("ERROR reading from socket");
       }
       printf("\nbytes: %d\n",n);
       for (int x = 0; x < n; x++)
       {
          printf("%x",buffer[x]);
       }

       const Json::Value ss = root["subsystems"];
       int offset = 0;
       for ( int ss_idx = 0; ss_idx < ss.size(); ++ss_idx )
       {
          cout << "\nss_name:" << ss[ss_idx]["ss_name"].asString() << endl;

          const Json::Value data = ss[ss_idx]["ss_data"];
          for ( int data_idx = 0; data_idx < data.size(); ++data_idx )
          {
             cout << "var_name:" << data[data_idx]["var_name"].asString() << endl;
             std::string type = data[data_idx]["type"].asString();
             cout << "  type:" << type << endl;
             cout << "  offset:" << offset << endl;
             if (type == "bool")
             {
                cout << "  a Bool: " << *(bool *)(buffer + offset) << endl;
                offset += 1;
             }
             if (type == "double")
             {
                cout << "  a double: " << *(double*)(buffer + offset) << endl;
                offset += 8;
             }
             if (type == "int32_t")
             {
                cout << "  an int32_t: " << *(int32_t*)(buffer + offset) << endl;
                offset += 4;
             }
             if (type == "char[32]")
             {
                cout << "  a char[32]: " << (char*)(buffer + offset) << endl;
                offset += 32;
             }
          }
       }


    }

    close(sockfd);

    return 0;
}
