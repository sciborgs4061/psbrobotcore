// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __SSTest1SubsystemContainer_h__
#define __SSTest1SubsystemContainer_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "PsbSubsystemContainer.h"
#include "SSTest1Controller.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace SSTest1 {

class DashstreamInterface;

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// A singleton point of access for all Subsystem objects in the SSTest1
/// subsystem.
////////////////////////////////////////////////////////////////////////////////
class SubsystemContainer : public Psb::Subsystem::SubsystemContainer
{
   public:
      ~SubsystemContainer(void);

      static SubsystemContainer* instance(void);

      Dashstream::BaseInterface_I  *getDashstreamInterface(void);
      Controller_I                 *getControllerInterface(void);

      virtual void init(void);

   private:
      static SubsystemContainer* msp_instance;

      DashstreamInterface    *mp_dashstreamIntf;
      Controller             *mp_controllerIntf;

   private:
      SubsystemContainer(void);

   private:
      DISALLOW_COPY_CTOR(SubsystemContainer);
      DISALLOW_ASSIGNMENT_OPER(SubsystemContainer);

}; // END class SubsystemContainer

} // END namespace SSTest1
} // END namespace Subsytem
} // END namespace Psb


#endif // __SSTest1SubsystemContainer_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

