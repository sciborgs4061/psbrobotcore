// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

START_NEW_SS(SSTest1)
   SS_POD_DATA(bool,
               sstest1_enabled,
               "SS",
               "SSTest1 Enabled",
               "Enabled state of SSTest Subsystem")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(double,
               sstest1_motor_1_speed,
               "AI",
               "Motor 1 Speed",
               "This is the speed of Motor 1")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(bool,
               sstest1_limit_switch,
               "SI",
               "Limit Switch",
               "Some limit switch")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(double,
               sstest1_throttle,
               "OI",
               "Throttle",
               "Joystick 1, RawAxis 1 : Throttle")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(int32_t,
               sstest1_left_encoder_ticks,
               "SI",
               "Left Encoder Ticks",
               "The number of Encoder ticks for the Left side")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(bool,
               sstest1_turbo_button,
               "OI",
               "Turbo",
               "Joystick 1, Raw Button 1 : Turbo")
END_SS



