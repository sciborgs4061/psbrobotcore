// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __SSTest1Controller_h__
#define __SSTest1Controller_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <PsbSubsystemTypes.h>
#include "Controller_I.h"
#include "SSTest1DashstreamInterface.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace SSTest1 {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class is the main controller for the SSTest1 
////////////////////////////////////////////////////////////////////////////////
class Controller : public Psb::Subsystem::Controller_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      //////////////////////////////////////////////////////////////////////////
      Controller(DashstreamInterface::DashstreamPOD_t *data);
     
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~Controller(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// does the init of the class
      //////////////////////////////////////////////////////////////////////////
      virtual void init(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Must set the m_enabled variable appropriately
      //////////////////////////////////////////////////////////////////////////
      virtual void setEnabledState(enabled_state_t new_state);

   private:   
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// reset data to known good state
      //////////////////////////////////////////////////////////////////////////
      void resetData(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb:Subsystem::SSTest1::Controller_I::performManualUpdate();
      //////////////////////////////////////////////////////////////////////////
      virtual void performManualUpdate(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb:Subsystem::SSTest1::Controller_I::performAutoUpdate();
      //////////////////////////////////////////////////////////////////////////
      virtual void performAutoUpdate(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::SSTest1::Controller_I::performDisabledUpdate();
      //////////////////////////////////////////////////////////////////////////
      virtual void performDisabledUpdate(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Computes the motor outputs from operator inputs and applies them to
      /// the motors.
      //////////////////////////////////////////////////////////////////////////
      void computeAndApplyMotorOutputs(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Switch subsystem state.
      //////////////////////////////////////////////////////////////////////////
      void transitionToState(subsystem_state_t new_state);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Determines the easy program to run based on the button input
      //////////////////////////////////////////////////////////////////////////
      void selectEasyProgram(void);

   private:

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// BumpController object used to control the left collector arm in
      /// manual control mode.
      //////////////////////////////////////////////////////////////////////////
      // Psb::BumpController* mp_bumpController;

      // The target tick-height of the elevator (NOT POSITION)
      int m_requestedHeightInTicks;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The storage for all the data
      //////////////////////////////////////////////////////////////////////////
      DashstreamInterface::DashstreamPOD_t *m_data;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The currently active easy program
      //////////////////////////////////////////////////////////////////////////
      // Autonomous::XmlProgram * mp_activeProgram;
      // Autonomous::XmlProgram * mp_easyProgramIndex;
      // Autonomous::XmlProgram * mp_easyProgramGoto;
      // Autonomous::XmlProgram * mp_easyProgramNest;

   private:
      DISALLOW_COPY_CTOR(Controller);
      DISALLOW_ASSIGNMENT_OPER(Controller);
}; // END class Controller


} // END namespace SSTest1
} // END namespace Subsystem
} // END namespace Psb


#endif // __SSTest1Controller_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

