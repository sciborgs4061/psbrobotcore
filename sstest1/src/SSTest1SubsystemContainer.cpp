// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// This is a good place to actually build the dashstream interface
// since this is the container that will hand it out if someone wants it
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "SSTest1DashstreamInterface.h"

// don't want to build it again if someone happens to
// include things again
#undef BUILD_IT

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstddef>
#include "SSTest1SubsystemContainer.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (SSTEST1 + 100u)

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace SSTest1 {

// /////////////////////////////////////////////////////////////////////////////
// Static Data
// /////////////////////////////////////////////////////////////////////////////
SubsystemContainer* SubsystemContainer::msp_instance = NULL;

////////////////////////////////////////////////////////////////////////////////
SubsystemContainer::SubsystemContainer(void)
{
   mp_dashstreamIntf = new DashstreamInterface;
   mp_controllerIntf = new Controller(
      ((DashstreamInterface::DashstreamPOD_t *)mp_dashstreamIntf->getData()));

   // TODO: put in a configuration interface to load settings from disk
}


////////////////////////////////////////////////////////////////////////////////
SubsystemContainer::~SubsystemContainer(void)
{
   delete mp_dashstreamIntf;
   mp_dashstreamIntf = NULL;
   delete mp_controllerIntf;
   mp_controllerIntf = NULL;
}

////////////////////////////////////////////////////////////////////////////////
void SubsystemContainer::init(void)
{
   mp_controllerIntf->init();
}

////////////////////////////////////////////////////////////////////////////////
SubsystemContainer*
SubsystemContainer::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new SubsystemContainer();
   }

   return msp_instance;
}

////////////////////////////////////////////////////////////////////////////////
Controller_I *
SubsystemContainer::getControllerInterface(void) 
{
   return mp_controllerIntf;
}

////////////////////////////////////////////////////////////////////////////////
Dashstream::BaseInterface_I *
SubsystemContainer::getDashstreamInterface(void) 
{
   return mp_dashstreamIntf;
}

} // END namespace SSTest1
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

