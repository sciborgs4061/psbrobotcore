// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "SSTest1Controller.h"
#include "SSTest1SubsystemContainer.h"
#include "SSTest1DashstreamInterface.h"
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (SSTEST1 + CONTROLLER)

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace SSTest1 {

////////////////////////////////////////////////////////////////////////////////
Controller::Controller(DashstreamInterface::DashstreamPOD_t *data)
   : Controller_I()
   , m_requestedHeightInTicks(0)
   , m_data(data)
{
   // clear out the data we are in control of
   resetData();
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t new_state)
{
   m_data->sstest1_enabled = (new_state == ENABLED);

   if (!m_data->sstest1_enabled)
   {
      // if we are not enabled...reset the data
      resetData();
   }
}

//////////////////////////////////////////////////////////////////////////
void Controller::resetData(void)
{
   // For now just forcing everything to 0 is good enough.
   // There could be cases where the reset state is not just 0
   // but we will take that on when it happens
   memset(m_data,0,sizeof(DashstreamInterface::DashstreamPOD_t));
}

////////////////////////////////////////////////////////////////////////////////
void Controller::init(void)
{
   // Here is where we would apply all the settings from the
   // configuration file...once we have that in here...if we
   // even do.

   // hook into the Operator Interfaces as needed
   GlobalOiWpiStore *oi = GlobalOiWpiStore::instance();
   oi->attach(new JoystickDataGrabber("sstest1_throttle",
                                      &(m_data->sstest1_throttle),
                                      JoystickDataGrabber::RAW_AXIS_1),
              GlobalOiWpiStore::JOYSTICK_ONE);
   oi->attach(new JoystickDataGrabber("sstest1_turbo_button",
                                      &(m_data->sstest1_turbo_button),
                                      JoystickDataGrabber::RAW_BUTTON_1),
              GlobalOiWpiStore::JOYSTICK_ONE);

   // hook into the Actuator Interfaces as needed
   GlobalAiWpiStore *ai = GlobalAiWpiStore::instance();
   ai->attach(new SpeedControllerDataPusher(&(m_data->sstest1_motor_1_speed)),
              GlobalAiWpiStore::MOTOR_ONE);

   // hook into the Sensor Interfaces as needed
   GlobalSiWpiStore *si = GlobalSiWpiStore::instance();
   si->attach(new DigitalInputDataGrabber(&(m_data->sstest1_limit_switch)),
              GlobalSiWpiStore::LIMITSWITCH_ONE);
   si->attach(new EncoderDataGrabber(&(m_data->sstest1_left_encoder_ticks)),
              GlobalSiWpiStore::ENCODER_ONE);
}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::performManualUpdate(void)
{
   // update the motors
   computeAndApplyMotorOutputs();
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performAutoUpdate(void)
{
  computeAndApplyMotorOutputs();
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   // keep track of the old state just in case
   subsystem_state_t prev_state = m_state;

   // update the base class member so the update() function calls
   // the right thing
   m_state = new_state;

   /// now do whatever else it is you need to do during a transiton
   switch(m_state)
   {
      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;
      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;
      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               // Unknown state!  This is a coding defect!
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;

      default:
      {
         // Unknown state!  This is a coding defect!
         PSB_LOG_ERROR(1,
                       Psb::LOG_TYPE_INT32, m_state,
                       Psb::LOG_TYPE_INT32, new_state);
         m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
      }
      break;
   }
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::computeAndApplyMotorOutputs(void)
{
   // go half speed unless the turbo is enabled
   double mult = 0.5;
   if (m_data->sstest1_turbo_button == 1)
   {
      mult = 1.0;
   }

   // if the limit switch is zero don't let it move
   if (m_data->sstest1_limit_switch == 0)
   {
      m_data->sstest1_motor_1_speed = 0;
   } else {
      m_data->sstest1_motor_1_speed = m_data->sstest1_throttle * mult;
   }
}


////////////////////////////////////////////////////////////////////////////////
void
Controller::selectEasyProgram(void)
{
}


} // END namespace SSTest1
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

