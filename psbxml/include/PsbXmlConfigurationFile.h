// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbXmlConfigurationFile_h__
#define __PsbXmlConfigurationFile_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <map>
#include "PsbMacros.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @todo
/// DOCUMENT ME!!
////////////////////////////////////////////////////////////////////////////////
class XmlConfigurationFile
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Overloaded Constructor
      ///
      /// @param[in] rc_filename   Name of th XML file to open and read
      //////////////////////////////////////////////////////////////////////////
      XmlConfigurationFile(std::string const& rc_filename);

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~XmlConfigurationFile(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Get requested setting as an Integer
      ///
      /// @remarks
      /// When the requested setting cannot be found, the defaultValue is
      /// returned.
      ///
      /// @param[in] rc_settingName  Setting name
      /// @param[in] defaultValue    Default setting value
      ///
      /// @return Setting value as integer
      //////////////////////////////////////////////////////////////////////////
      int getValueAsInt(std::string const& rc_settingName,
                        int                defaultValue) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Get requested setting as a Boolean
      ///
      /// @remarks
      /// When the requested setting cannot be found, the defaultValue is
      /// returned.
      ///
      /// @param[in] rc_settingName  Setting name
      /// @param[in] defaultValue    Default setting value
      ///
      /// @return Setting value as boolean
      //////////////////////////////////////////////////////////////////////////
      bool getValueAsBoolean(std::string const& rc_settingName,
                             bool               defaultValue) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Get requested setting as a Float
      ///
      /// @remarks
      /// When the requested setting cannot be found, the defaultValue is
      /// returned.
      ///
      /// @param[in] rc_settingName  Setting name
      /// @param[in] defaultValue    Default setting value
      ///
      /// @return Setting value as float
      //////////////////////////////////////////////////////////////////////////
      float getValueAsFloat(std::string const& rc_settingName,
                            float              defaultValue) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Get requested setting as a string
      ///
      /// @remarks
      /// When the requested setting cannot be found, the defaultValue is
      /// returned.
      ///
      /// @param[in] rc_settingName  Setting name
      /// @param[in] defaultValue    Default setting value
      ///
      /// @return Setting value as float
      //////////////////////////////////////////////////////////////////////////
      std::string getValueAsString(std::string const& rc_settingName,
                                   std::string        defaultValue) const;

   private:
      DISALLOW_DEFAULT_CTOR(XmlConfigurationFile);
      DISALLOW_COPY_CTOR(XmlConfigurationFile);
      DISALLOW_ASSIGNMENT_OPER(XmlConfigurationFile);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Map containing key-value pairs of settings, stored in strings.
      //////////////////////////////////////////////////////////////////////////
      std::map<std::string,std::string> m_configItemsTable;

}; // END class XmlConfigurationFile


} // END Psb Namespace


#endif // __PsbXmlConfigurationFile_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

