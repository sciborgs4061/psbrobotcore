// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <string>
#include <map>
#include "PsbXmlConfigurationFile.h"

#define RAPIDXML_NO_EXCEPTIONS
#include "rapidxml/rapidxml.hpp"



////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that XmlConfigurationFile class properly loads data out
/// of the given XML file when created.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbXmlConfigurationFile_UT, basicDataLoad)
{
   // Create a stack-local XmlConfigurationFile object
   Psb::XmlConfigurationFile config_file("../data/BasicSettings.xml");

   // Setting:  name="ConfigItemOne", value="1.0"
   EXPECT_EQ(0,                  config_file.getValueAsInt("ConfigItemOne", 0));
   EXPECT_EQ(1.0f,               config_file.getValueAsFloat("ConfigItemOne", 0.0f));
   EXPECT_EQ(false,              config_file.getValueAsBoolean("ConfigItemOne", false));
   EXPECT_EQ(true,               config_file.getValueAsBoolean("ConfigItemOne", true));
   EXPECT_EQ(std::string("1.0"), config_file.getValueAsString("ConfigItemOne", "2.0"));

   // Setting:  name="ConfigItemTwo", value="2"
   EXPECT_EQ(2,                  config_file.getValueAsInt("ConfigItemTwo", 0));
   EXPECT_EQ(2.0f,               config_file.getValueAsFloat("ConfigItemTwo", 0.0f));
   EXPECT_EQ(false,              config_file.getValueAsBoolean("ConfigItemTwo", false));
   EXPECT_EQ(true,               config_file.getValueAsBoolean("ConfigItemTwo", true));
   EXPECT_EQ(std::string("2"),   config_file.getValueAsString("ConfigItemTwo", "1"));

   // Setting:  name="ConfigItemThree", value="TRUE"
   EXPECT_EQ(0,                   config_file.getValueAsInt("ConfigItemThree", 0));
   EXPECT_EQ(0.0f,                config_file.getValueAsFloat("ConfigItemThree", 0.0f));
   EXPECT_EQ(true,                config_file.getValueAsBoolean("ConfigItemThree", false));
   EXPECT_EQ(true,                config_file.getValueAsBoolean("ConfigItemThree", true));
   EXPECT_EQ(std::string("TRUE"), config_file.getValueAsString("ConfigItemThree", "FALSE"));

   // Setting:  name="ConfigItemFour", value="Four"
   EXPECT_EQ(0,                   config_file.getValueAsInt("ConfigItemFour", 0));
   EXPECT_EQ(0.0f,                config_file.getValueAsFloat("ConfigItemFour", 0.0f));
   EXPECT_EQ(false,               config_file.getValueAsBoolean("ConfigItemFour", false));
   EXPECT_EQ(true,                config_file.getValueAsBoolean("ConfigItemFour", true));
   EXPECT_EQ(std::string("Four"), config_file.getValueAsString("ConfigItemFour", "Five"));

   return;
}


/// @todo
/// Figure out how to test that this program SHOULD die when we attempt to load
/// some invalid XML.



// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

