// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <gtest/gtest.h>
#include "RapidXmlErrorHandler.h"

#define RAPIDXML_NO_EXCEPTIONS
#include "rapidxml/rapidxml.hpp"



////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that when a valid XML file is loaded in the RapidXML
/// parser, we show no error condition.
////////////////////////////////////////////////////////////////////////////////
TEST(RapidXmlErrorHandlerUT, noErrorOnValidXml)
{
   // Load the XML file using RapidXML
   std::ifstream xml_stream("../data/Valid.xml", std::ifstream::binary);
   xml_stream.seekg(0, xml_stream.end);
   int num_bytes = xml_stream.tellg();
   xml_stream.seekg(0, xml_stream.beg);

   // Now read the file contents into memory, and close the file
   char* p_contents = new char[num_bytes+1];  // Don't forget the null
   xml_stream.read(p_contents, num_bytes);
   p_contents[num_bytes] = '\0';
   xml_stream.close();

   // Now, let's parse the xml file
   rapidxml::xml_document<char> xml_doc;
   xml_doc.parse<0>(p_contents);

   // And free up the memory we used
   delete [] p_contents;

   // As long as we get here, no assertions occurred - test passes
   return;
}


/// @todo
/// Figure out how to test that this program SHOULD die when we attempt to load
/// some invalid XML.



// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

