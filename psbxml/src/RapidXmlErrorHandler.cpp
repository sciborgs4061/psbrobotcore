// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <functional>
#include <cstdlib>
#include <execinfo.h>
#include "RapidXmlErrorHandler.h"

#define RAPIDXML_NO_EXCEPTIONS
#include <rapidxml/rapidxml.hpp>


// /////////////////////////////////////////////////////////////////////////////
// RapidXML Error Callback Function
// /////////////////////////////////////////////////////////////////////////////
void
rapidxml::parse_error_handler(const char* pc_what, void* p_where)
{
   // Dump the Rapid-XML error information
   char* p_where_string = reinterpret_cast<char*>(p_where);
   fprintf(stderr,
           "\n\n"
           "RapidXML Parsing Error!\n"
           "What:  %s\n"
           "Where: %s\n"
           "\n\n",
           pc_what,
           p_where_string);

   // Grab any stack frame information we can
   void*  p_array[20];
   size_t num_frames = backtrace(p_array, 20);
   char** pp_strings = backtrace_symbols(p_array, num_frames);

   // And print out information for each frame
   for (size_t idx = 0; idx < num_frames; idx++)
   {
      fprintf(stderr, "%s\n", pp_strings[idx]);
   }

   free(pp_strings);
   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

