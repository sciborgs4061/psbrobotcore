// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <PsbLogger.h>
#define RAPIDXML_NO_EXCEPTIONS
#include "rapidxml/rapidxml.hpp"
#include "PsbXmlConfigurationFile.h"
#include "PsbStringConverter.h"


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM 0xFFEE1110


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
XmlConfigurationFile::XmlConfigurationFile(std::string const& rc_filename)
   : m_configItemsTable()
{
   // Open the stream
   std::ifstream file_stream(rc_filename, std::ifstream::binary);

   // If file exists
   if (true == file_stream.good())
   {
      // Find out how big the file is (in bytes)
      file_stream.seekg(0, file_stream.end);
      int length = file_stream.tellg();
      file_stream.seekg(0, file_stream.beg);

      // ///////////////////////////////////////////////////////////////////////
      // The RapidXML library forces the end user to present XML data in string
      // form in memory.  So we will read the contents of the XML file into a
      // heap-allocated character buffer of sufficient size (+1 for
      // NULL-termination safety).
      // ///////////////////////////////////////////////////////////////////////
      char* p_buffer = new char[length+1];
      file_stream.read(p_buffer, length);
      file_stream.close();
      p_buffer[length] = '\0';

      // ///////////////////////////////////////////////////////////////////////
      // Parse the XML file.
      // This code expects an XML schema of this form:
      // <SubsystemSettings>
      //    <Setting name="SomeName1" value="SomeValue1"/>
      //    <Setting name="SomeName2" value="SomeValue2"/>
      //    ...
      // </SubsystemSettings>
      // ///////////////////////////////////////////////////////////////////////
      rapidxml::xml_document<char> doc;
      doc.parse<0>(p_buffer);

      // Get root node, "SubsystemSettings"
      rapidxml::xml_node<char>* p_root = doc.first_node("SubsystemSettings");

      if (NULL != p_root)
      {
         // Get first Setting node
         rapidxml::xml_node<char>* p_setting = p_root->first_node("Setting");

         while (NULL != p_setting)
         {
            // Extract the name/value pair from the DOM
            rapidxml::xml_attribute<char>* p_key   = p_setting->first_attribute("name");
            rapidxml::xml_attribute<char>* p_value = p_setting->first_attribute("value");

            // Make sure we were able to extract them
            if ( (NULL != p_key) &&
                 (NULL != p_value) )
            {
               // Check to see if this setting is already known
               std::map<std::string, std::string>::iterator iter =
                  m_configItemsTable.find(p_key->value());

               if (m_configItemsTable.end() != iter)
               {
                  // The key-value pair is already known to us - ERROR!
                  // Don't change the setting table though... First-In wins!
                  PSB_LOG_ERROR(4, Psb::LOG_TYPE_STRING, iter->first.c_str(),
                                   Psb::LOG_TYPE_STRING, iter->second.c_str(),
                                   Psb::LOG_TYPE_STRING, p_key->value(),
                                   Psb::LOG_TYPE_STRING, p_value->value());
               }
               else
               {
                  // We don't know about this setting yet, so add it
                  m_configItemsTable[p_key->value()] = p_value->value();
                  PSB_LOG_DEBUG1(2, Psb::LOG_TYPE_STRING, p_key->value(),
                                    Psb::LOG_TYPE_STRING, p_value->value());
               }
            }
            else
            {
               PSB_LOG_DEBUG1(2, Psb::LOG_TYPE_HEX32, p_key,
                                 Psb::LOG_TYPE_HEX32, p_value);
            }

            // Move onto the next setting
            p_setting = p_setting->next_sibling();
         }
      }
      else
      {
         // Could not find the root!
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_STRING, rc_filename.c_str());
      }

      // Free the memory for the XML file
      delete [] p_buffer;
   }
   else
   {
      // Failed to open the XML file for reading
      PSB_LOG_ERROR(1, Psb::LOG_TYPE_STRING, rc_filename.c_str());
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
XmlConfigurationFile::~XmlConfigurationFile(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
int
XmlConfigurationFile::getValueAsInt(std::string const& rc_settingName,
                                    int                defaultValue) const
{
   int ret_val = defaultValue;

   // Find the setting key in the map
   std::map<std::string, std::string>::const_iterator iter =
      m_configItemsTable.find(rc_settingName);

   if (m_configItemsTable.end() != iter)
   {
      // We found it, convert it to an integer and set return value
      ret_val = StringConverter::toInt(iter->second, defaultValue);
   }
   else
   {
      // Key not found in our table
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, rc_settingName.c_str(),
                    Psb::LOG_TYPE_INT32,  defaultValue);
      ret_val = defaultValue;
   }

   return ret_val;
}


////////////////////////////////////////////////////////////////////////////////
bool
XmlConfigurationFile::getValueAsBoolean(std::string const& rc_settingName,
                                        bool defaultValue) const
{
   bool ret_val = defaultValue;

   // Find the setting key in the map
   std::map<std::string, std::string>::const_iterator iter =
      m_configItemsTable.find(rc_settingName);

   if (m_configItemsTable.end() != iter)
   {
      // We found it, now examine it for a proper Boolean value.
      std::string setting_value = iter->second;

      ret_val = StringConverter::toBoolean(setting_value, defaultValue);
   }
   else
   {
      // Key not found in our table
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, rc_settingName.c_str(),
                    Psb::LOG_TYPE_BOOL,   defaultValue);
      ret_val = defaultValue;
   }

   return ret_val;
}


////////////////////////////////////////////////////////////////////////////////
float
XmlConfigurationFile::getValueAsFloat(std::string const& rc_settingName,
                                      float              defaultValue) const
{
   float ret_val = defaultValue;

   // Find the setting key in the map
   std::map<std::string, std::string>::const_iterator iter =
      m_configItemsTable.find(rc_settingName);

   if (m_configItemsTable.end() != iter)
   {
      // We found it, now convert the string value to a float
      std::string setting_value = iter->second;

      ret_val = StringConverter::toFloat(setting_value, defaultValue);
   }
   else
   {
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING,  rc_settingName.c_str(),
                    Psb::LOG_TYPE_FLOAT32, defaultValue);
      ret_val = defaultValue;
   }

   return ret_val;
}


////////////////////////////////////////////////////////////////////////////////
std::string
XmlConfigurationFile::getValueAsString(std::string const& rc_settingName,
                                       std::string        defaultValue) const
{
   std::string ret_val = defaultValue;

   // Find the setting key in the map
   std::map<std::string, std::string>::const_iterator iter =
      m_configItemsTable.find(rc_settingName);

   if (m_configItemsTable.end() != iter)
   {
      // We found it, now convert the string value to a float
      ret_val = iter->second;
   }
   else
   {
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, rc_settingName.c_str(),
                    Psb::LOG_TYPE_STRING, defaultValue.c_str());
      ret_val = defaultValue;
   }

   return ret_val;
}


} // END Psb Namespace


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

