////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousXmlDescriptions_h__
#define __AutonomousXmlDescriptions_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <map>
#include <string>


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { namespace Subsystem { namespace Autonomous { class Step; } } }
namespace Psb { namespace Subsystem { namespace Autonomous { class Action; } } }
namespace Psb { namespace Subsystem { namespace Autonomous { class Condition; } } }



// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This structure contains all information presented by an XML "Step" tag in
/// the PSB AutonomousXml schema.
////////////////////////////////////////////////////////////////////////////////
struct step_description_t
{
   std::string id;        ///< A unique Step identifier
   bool        is_entry;  ///< Is this the program entry point?
   bool        is_exit;   ///< Is this a "leaf" step?
   Step*       p_step;    ///< The Step object
};


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This structure contains all information presented by an XML "Action" tag in
/// the PSB AutonomousXml schema.
////////////////////////////////////////////////////////////////////////////////
struct action_description_t
{
   std::string                       id;        ///< A unique Action identifier
   std::string                       parent_id; ///< The ID of the parent Step
   std::string                       classname; ///< The C++ class name of this Action
   std::map<std::string,std::string> params;    ///< List of Action parameters (might be empty)
   Action*                           p_action;  ///< The Action object
};


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This structure contains all information presented by an XML "Condition" tag
/// in the PSB AutonomousXml schema.
////////////////////////////////////////////////////////////////////////////////
struct condition_description_t
{
   std::string                       id;          ///< A unique Condition identifier
   std::string                       parent_id;   ///< The ID of the Parent/Composite condition
   std::string                       type;        ///< Type of condition, composite or unary
   std::string                       operation;   ///< If composite, the logical operation for it
   std::string                       classname;   ///< The C++ class name of this Condition
   std::map<std::string,std::string> params;      ///< List of Condition parameters (might be empty)
   Condition*                        p_condition; ///< The Condition object
};

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This structure contains all information presented by an XML "Transition" tag
/// in the PSB AutonomousXml schema.
////////////////////////////////////////////////////////////////////////////////
struct transition_description_t
{
   std::string  id;            ///< A unique Transition identifier
   std::string  fromstep_id;   ///< The ID of the "from" Step for this Transition
   std::string  tostep_id;     ///< The ID of the "to" Step for this Transition
   std::string  condition_id;  ///< The ID of the Condition that drives this Transition
};


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousXmlDescriptions_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

