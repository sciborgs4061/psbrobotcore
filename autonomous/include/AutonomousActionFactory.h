////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousActionFactory_h__
#define __AutonomousActionFactory_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include "AutonomousXmlDescriptions.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { namespace Subsystem { namespace Autonomous { class Action; } } }
namespace Psb { namespace Subsystem { namespace Autonomous { class Controller; } } }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a static function for building Actions based on XML
/// description information.
////////////////////////////////////////////////////////////////////////////////
class ActionFactory
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Builds and returns an Action object based on the XML description given.
      ///
      /// @remarks
      /// If a suitable Action object cannot be determined from the description
      /// information, then a "ContinueAction" object will be created.  This may
      /// not be what the XML program description is asking for, but it should
      /// at least be safe in most cases.
      ///
      /// @param[in] rc_desc       The action description information
      //////////////////////////////////////////////////////////////////////////
      static Action* createAction(action_description_t const& rc_desc);

   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(ActionFactory);
      DISALLOW_COPY_CTOR(ActionFactory);
      DISALLOW_ASSIGNMENT_OPER(ActionFactory);

      // Neat trick to prevent inheritance
      virtual void disallowInheritance(void) = 0;
}; // END class ActionFactory


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousActionFactory_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

