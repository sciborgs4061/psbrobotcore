////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousAction_h__
#define __AutonomousAction_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// An Action object encapsulates the task of emulating user-input during the
/// autonomous portion of an FRC match.
///
/// @remarks
/// For example, one such action may be "drive forward at 50% power".  This
/// action would emulate end-user input by manipulating the joystick controls
/// such that left/right deflection was 0, and forward/backward power was +50%.
////////////////////////////////////////////////////////////////////////////////
class Action
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~Action(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the action name to the caller
      //////////////////////////////////////////////////////////////////////////
      std::string getName(void) const;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Carries out any required initialization for a given action.
      ///
      /// @remarks
      /// The initialize() method will be called once for each action associated
      /// with a given step, and then not again for as long as that step is
      /// active.
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void) = 0;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method actually implements the action for the associated step.
      //////////////////////////////////////////////////////////////////////////
      virtual void perform(void) = 0;
      
   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overloaded Constructor
      ///
      /// @param[in] rc_name   Name of the action
      //////////////////////////////////////////////////////////////////////////
      Action(std::string const& rc_name);
      
   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The name of the action.
      //////////////////////////////////////////////////////////////////////////
      std::string m_actionName;
      
   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(Action);
      DISALLOW_COPY_CTOR(Action);
      DISALLOW_ASSIGNMENT_OPER(Action);
}; // END class Action


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousAction_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

