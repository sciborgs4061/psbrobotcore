////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousConstCondition_h__
#define __AutonomousConstCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include "AutonomousCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class allows for the creation of "always-true" or "always-false"
/// conditions.
///
/// @remarks
/// The ConstCondition object will return a fixed value just after
/// being created - either true or false.  No external input sources are
/// examined in order to determine the condition status.  This object can be
/// used to either immediately cause a step to end, or to cause a step to run
/// indefinitely.
///
/// @see Step
////////////////////////////////////////////////////////////////////////////////
class ConstCondition : public Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] condition   The value that will be returned by the
      ///                        isConditionMet() method when the associated
      ///                        step is executed.
      //////////////////////////////////////////////////////////////////////////
      ConstCondition(bool condition);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~ConstCondition(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Condition::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Condition::isConditionMet()
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);
      
   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This is set at construction time by the caller.  This value is simply
      /// returned in the isConditionMet() method.
      //////////////////////////////////////////////////////////////////////////
      bool m_condition;
      
   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(ConstCondition);
      DISALLOW_COPY_CTOR(ConstCondition);
      DISALLOW_ASSIGNMENT_OPER(ConstCondition);
}; // END class ConstCondition


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousConstCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

