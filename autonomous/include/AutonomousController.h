// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousController_h__
#define __AutonomousController_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <vector>
#include <string>
#include <PsbSubsystemTypes.h>
#include "Controller_I.h"
#include "AutonomousConfigurationInterface.h"
#include "AutonomousDashstreamInterface.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { namespace Subsystem { namespace Autonomous { class Program; } } }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class is responsible for reading operator and sensor inputs associated
/// with autonomous program selection functionality and making control decisions
/// to implement that functionality.
////////////////////////////////////////////////////////////////////////////////
class Controller : public Psb::Subsystem::Controller_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      //////////////////////////////////////////////////////////////////////////
      Controller(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~Controller(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Controller_I::update()
      //////////////////////////////////////////////////////////////////////////
      // virtual void update(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Controller_I::init()
      //////////////////////////////////////////////////////////////////////////
      virtual void init(void);

      void performManualUpdate(void);
      void performAutoUpdate(void);
      void performDisabledUpdate(void);
      void transitionToState(subsystem_state_t new_state);
      void setEnabledState(enabled_state_t new_state);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Autonomous::Controller_I::getSelectedProgramName()
      //////////////////////////////////////////////////////////////////////////
      virtual std::string getSelectedProgramName(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Autonomous::Controller_I::getLockedProgramName()
      //////////////////////////////////////////////////////////////////////////
      virtual std::string getLockedProgramName(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// sets the match state so the controller can do the right thing
      //////////////////////////////////////////////////////////////////////////
      void setFrcMatchState(frc_match_state_t new_state);

      Dashstream::BaseInterface_I *getDashstreamInterface(void);

   private:
      // void performUpdateDisabled(void);

   private:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      /// Stores Configuration Settings
      ////////////////////////////////////////////////////////////////////////// 
      ConfigurationInterface m_configurationIntf;

      // TODO a bunch of these should be folded into the POD
      // data structure...but we have to build the Edge-triggered thing
      // first...for now these do nothing
      button_state_t    m_nextProgramBtnState;  /// Edge-triggered
      button_state_t    m_prevProgramBtnState;  /// Edge-triggered
      switch_state_t    m_lockInSwitchState;    /// Edge-triggered
      frc_match_state_t m_frcMatchState;        /// Edge-triggered
      frc_match_state_t m_lastFrcMatchState;

      std::vector<Program*> m_programTable;     /// Contains all Programs that could be run
      size_t m_selectedProgramIndex;            /// Identifies the "soft" selection
      size_t m_lockedInProgramIndex;            /// Identifies the "hard" selection
      std::string m_selectedProgramName;        /// Name of the currently selected program
      std::string m_lockedInProgramName;        /// Name of the program that will run if Autonomous mode is engaged

      DashstreamInterface *mp_dashstreamIntf;
      DashstreamInterface::DashstreamPOD_t *m_data;

   private:
      DISALLOW_COPY_CTOR(Controller);
      DISALLOW_ASSIGNMENT_OPER(Controller);
}; // END class Controller

} // END namespace Autonomous
} // END namespace Subsytem
} // END namespace Psb


#endif // __AutonomousController_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

