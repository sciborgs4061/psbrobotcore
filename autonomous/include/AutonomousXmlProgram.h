////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousXmlProgram_h__
#define __AutonomousXmlProgram_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <map>
#include <PsbMacros.h>
#include "AutonomousProgram.h"
#include "AutonomousXmlDescriptions.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace rapidxml { template<class T> class xml_document; }
namespace rapidxml { template<class T> class xml_node; }
namespace Psb { namespace Subsystem { namespace Autonomous { class Step; } } }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The XmlProgram class extends the Program class by reading an XML file and
/// building steps/actions/conditions/transitions according to the definition
/// found therein.
////////////////////////////////////////////////////////////////////////////////
class XmlProgram : public Program
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @param[in] rc_xmlFile  The name of XML file that defines this program
      //////////////////////////////////////////////////////////////////////////
      XmlProgram(std::string const& rc_xmlFile);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~XmlProgram(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Autonomous::initializeImpl()
      //////////////////////////////////////////////////////////////////////////
      virtual void initializeImpl(void);

   private:
      void parseXmlFile(rapidxml::xml_document<char>& r_doc);
      void parseXmlStepNode(rapidxml::xml_node<char>* p_node);
      void parseXmlActionNode(rapidxml::xml_node<char>* p_node);
      void parseXmlConditionNode(rapidxml::xml_node<char>* p_node);
      void parseXmlTransitionNode(rapidxml::xml_node<char>* p_node);

   private:
      /// @todo Documentation
      std::string m_xmlFile;
      bool m_wasParseErrorFound;

      // Convenience types
      typedef std::map<std::string,step_description_t> step_table_t;
      typedef std::map<std::string,action_description_t> action_table_t;
      typedef std::map<std::string,condition_description_t> condition_table_t;
      typedef std::map<std::string,transition_description_t> transition_table_t;

      // Container for program element descriptions
      step_table_t       m_programStepsTable;
      action_table_t     m_programActionsTable;
      condition_table_t  m_programConditionsTable;
      transition_table_t m_programTransitionsTable;

   private:
      // Hidden from public view.
      DISALLOW_DEFAULT_CTOR(XmlProgram);
      DISALLOW_COPY_CTOR(XmlProgram);
      DISALLOW_ASSIGNMENT_OPER(XmlProgram);
}; // END class XmlProgram


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousXmlProgram_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

