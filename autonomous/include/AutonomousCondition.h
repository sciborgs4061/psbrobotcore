////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousCondition_h__
#define __AutonomousCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// A Condition object encapsulates what could be complex logic that results in
/// a Boolean T/F outcome.
///
/// @remarks
/// The AutoProgramStep object builds a collection of these condition objects
/// and uses that collection as the determining factor when moving from one step
/// to the next.
///
/// @see AutoProgramStep
////////////////////////////////////////////////////////////////////////////////
class Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~Condition(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the condition name to the caller
      //////////////////////////////////////////////////////////////////////////
      std::string getName(void) const;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Carries out any initialization required for the condition to evaluate
      /// properly.
      ///
      /// @remarks
      /// The initialize() method will be called once upon entering a step and
      /// not again while that step is active.  This is a good place to reset
      /// or re-calibrate sensors (like encoders or gyros).
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void) = 0;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Implements the logic for determining if this condition has been met.
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void) = 0;
      
   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overloaded Constructor
      ///
      /// @param[in] rc_name   Name of this condition
      //////////////////////////////////////////////////////////////////////////
      Condition(const std::string& rc_name);
      
   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The name of the condition object - used in debugging
      //////////////////////////////////////////////////////////////////////////
      std::string  m_conditionName;
      
   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(Condition);
      DISALLOW_COPY_CTOR(Condition);
      DISALLOW_ASSIGNMENT_OPER(Condition);
}; // END class Condition


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

