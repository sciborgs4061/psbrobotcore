////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

START_NEW_SS(Autonomous)
   SS_POD_DATA(bool,
               prev_program_button,
               "OI",
               "Previous Program",
               "Controls moving to the previous Auto Program")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(bool,
               next_program_button,
               "OI",
               "Next Program",
               "Controls moving to the next Auto Program")
   SS_POD_DATA_DELIMITER
   SS_POD_DATA(bool,
               lock_program,
               "OI",
               "Lock Auto Program",
               "Switch to lock in selected Auto Program")
   SS_POD_DATA_DELIMITER
   SS_CHAR32_POD_DATA(
               selectedProgram,
               "SS",
               "Selected Auto Program",
               "Currently selected Auto Program")
   SS_POD_DATA_DELIMITER
   SS_CHAR32_POD_DATA(
               lockedProgram,
               "SS",
               "Locked Auto Program",
               "Currently locked in Auto Program")
END_SS



