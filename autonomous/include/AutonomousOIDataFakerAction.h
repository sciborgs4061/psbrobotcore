////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousOIDataFakerAction_h__
#define __AutonomousOIDataFakerAction_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include "AutonomousAction.h"
#include "AutonomousXmlDescriptions.h"
#include "OiDataGrabbers.h"
#include <list>

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
///
/// @remarks
////////////////////////////////////////////////////////////////////////////////
class OIDataFakerAction : public Action
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      //////////////////////////////////////////////////////////////////////////
      OIDataFakerAction(action_description_t const& rc_desc);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~OIDataFakerAction(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Action::initialize()
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Action::perform()
      //////////////////////////////////////////////////////////////////////////
      virtual void perform(void);

   private:
      // Hidden from public view
      DISALLOW_COPY_CTOR(OIDataFakerAction);
      DISALLOW_ASSIGNMENT_OPER(OIDataFakerAction);

      // parameters to the fake OI action
      std::map<std::string,std::string> m_params;

      // list of the fakers
      std::list<JoystickDataFaker *> m_fakers;

}; // END class OIDataFakerAction


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousOIDataFakerAction_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

