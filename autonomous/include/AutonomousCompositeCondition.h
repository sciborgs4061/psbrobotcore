////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousCompositeCondition_h__
#define __AutonomousCompositeCondition_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <list>
#include <PsbMacros.h>
#include "AutonomousCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// An CompositeCondition object encapsulates what could be complex logic that
/// results in a Boolean T/F outcome.
///
/// @remarks
/// The CompositeStep object builds a collection of these condition objects
/// and uses that collection as the determining factor when moving from one step
/// to the next.
///
/// @see CompositeStep
////////////////////////////////////////////////////////////////////////////////
class CompositeCondition : public Condition
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This enumeration is used to specify which type of logical operation
      /// will be performed on all conditions that compose the composition.
      //////////////////////////////////////////////////////////////////////////
      typedef enum
      {
         OPERATOR_OR,    ///< True  --> if any of the conditions is true
         OPERATOR_NOR,   ///< False --> if any of the conditions is true
         OPERATOR_XOR,   ///< True  --> if exactly one of the conditions is true
         OPERATOR_XNOR,  ///< False --> if exactly one of the conditions is true
         OPERATOR_AND,   ///< True  --> if all conditions are true
         OPERATOR_NAND,  ///< False --> if all conditions are true

         // Add new logical operators above this line
         NUM_OF_OPERATORS
      } operator_t;


   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Overloaded Constructor
      ///
      /// @param[in] rc_name             Name of this condition
      /// @param[in] logical_operation   The operation to perform when evaluating
      ///                                the overall status of this condition.
      //////////////////////////////////////////////////////////////////////////
      CompositeCondition(const std::string& rc_name,
                         operator_t logical_operation);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~CompositeCondition(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Adds a condition to the composition.
      ///
      /// @param[in] p_condition   Pointer to the condition to add
      //////////////////////////////////////////////////////////////////////////
      void addCondition(Condition* p_condition);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Carries out any initialization required for the condition to evaluate
      /// properly.
      ///
      /// @remarks
      /// The initialize() method will be called once upon entering a step and
      /// not again while that step is active.  This is a good place to reset
      /// or re-calibrate sensors (like encoders or gyros).
      //////////////////////////////////////////////////////////////////////////
      virtual void initialize(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Implements the logic for determining if this condition has been met.
      //////////////////////////////////////////////////////////////////////////
      virtual bool isConditionMet(void);
      
   private:
      // Helpers to implement various logical operations on our conditions
      bool isConditionMet__OR(void);
      bool isConditionMet__NOR(void);
      bool isConditionMet__XOR(void);
      bool isConditionMet__XNOR(void);
      bool isConditionMet__AND(void);
      bool isConditionMet__NAND(void);

   private:
      typedef std::list<Condition*> condition_list_t;

      operator_t       m_operator;
      condition_list_t m_conditionList;

   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(CompositeCondition);
      DISALLOW_COPY_CTOR(CompositeCondition);
      DISALLOW_ASSIGNMENT_OPER(CompositeCondition);
}; // END class CompositeCondition


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousCompositeCondition_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

