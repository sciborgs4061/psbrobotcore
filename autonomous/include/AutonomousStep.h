////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousStep_h__
#define __AutonomousStep_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <list>
#include <map>
#include <string>
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaratio(s)
// /////////////////////////////////////////////////////////////////////////////
class Action;
class Condition;


////////////////////////////////////////////////////////////////////////////////
/// @brief
///  The Step represents a single step in a complete Program.
///  *Each* action in the step will be performed until *any* condition is met.
///
/// @see Program
/// @see Action
/// @see Condition
////////////////////////////////////////////////////////////////////////////////
class Step
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor, builds an Step with empty action and condition
      /// lists.
      ///
      /// @param[in] rc_name   The name of this Step
      //////////////////////////////////////////////////////////////////////////
      Step(const std::string& rc_name);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor, frees memory for all action and condition objects, clears
      /// lists, and step name.
      //////////////////////////////////////////////////////////////////////////
      virtual ~Step(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Adds an action object to this step.
      ///
      /// @param[in] p_action   Pointer to a heap-allocated action.
      //////////////////////////////////////////////////////////////////////////
      void addAction(Action* p_action);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Adds a "transition" out of this step.  When a given condition becomes
      /// met, then the associated next step will become the active step.
      ///
      /// @param[in] p_condition   The condition that causes p_step to be activated
      /// @param[in] p_step        The "next" step if p_condition becomes met
      //////////////////////////////////////////////////////////////////////////
      void addTransition(Condition* p_condition,
                         Step*      p_step);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Performs initialization actions for this Step.
      ///
      /// @remarks
      /// The Program class ensures that when a new step is started, this
      /// initialize method is called.  There is no real work to do here except
      /// to pass the initialization onto the action and condition objects that
      /// have been added to this step.
      //////////////////////////////////////////////////////////////////////////
      void initialize(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Iterates over all stored actions, and calls perform() on each of them
      /// in the order in which the actions were added to the step.
      //////////////////////////////////////////////////////////////////////////
      void performActions(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the auto program step name to the caller.
      //////////////////////////////////////////////////////////////////////////
      std::string getName(void) const;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method returns the next step per the transition table and which
      /// condition triggered the transition.
      //////////////////////////////////////////////////////////////////////////
      Step* getNextStep(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Indicates if this program step is complete.
      ///
      /// @remarks
      /// The program step is complete when its associated condition has been
      /// met.  If the associated condition is a CompositeCondition, then many
      /// different logical operators can be used to get complex conditions.
      ///
      /// @retval false   Step is still in progress
      /// @retval true    Step is complete
      //////////////////////////////////////////////////////////////////////////
      bool isComplete(void);
      
   private:
      typedef std::list<Action*>          action_list_t;
      typedef std::list<Condition*>       condition_list_t;
      typedef std::map<Condition*, Step*> transition_table_t;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The name of this step - used in debugging.
      //////////////////////////////////////////////////////////////////////////
      std::string m_stepName;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The list of actions associated with this step.
      ///
      /// @remarks
      /// Actions are performed in the order that they were added to the step.
      //////////////////////////////////////////////////////////////////////////
      action_list_t m_actionList;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The list of conditions associated with this step.
      ///
      /// @remarks
      /// Conditions are queried for their "is met" status in the order in which
      /// they were added to the step.  The associated transition leads us to
      /// the next step.  So, if 2 Conditions both become met at the same point
      /// in time, the transition associated with the first Condition will be
      /// taken.
      //////////////////////////////////////////////////////////////////////////
      condition_list_t m_conditionList;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// A mapping of Condition objects to Step objects.
      ///
      /// @remarks
      /// When a condition is deemed "met" we will set the next step to the step
      /// indicated in the transition table - which might be NULL.
      //////////////////////////////////////////////////////////////////////////
      transition_table_t m_transitionTable;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The that will be activated next - might be NULL.
      ///
      /// @see m_transitionTable
      //////////////////////////////////////////////////////////////////////////
      Step* mp_nextStep;
      
   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Iterates over the list of this step's actions and initializes them.
      //////////////////////////////////////////////////////////////////////////
      void initializeActions(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Iterates over the list of this step's conditions and initializes them.
      //////////////////////////////////////////////////////////////////////////
      void initializeConditions(void);
      
   private:
      // Hidden from public view
      DISALLOW_DEFAULT_CTOR(Step);
      DISALLOW_COPY_CTOR(Step);
      DISALLOW_ASSIGNMENT_OPER(Step);
}; // END class Step


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousStep_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

