// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousConfigurationInterface_h__
#define __AutonomousConfigurationInterface_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "AutonomousConfiguration_I.h"


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { class XmlConfigurationFile; }


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides an application layer interface to configuration items
/// (settings) for the Autonomous subsystem.
////////////////////////////////////////////////////////////////////////////////
class ConfigurationInterface : public Psb::Subsystem::Autonomous::Configuration_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default Constructor
      //////////////////////////////////////////////////////////////////////////
      ConfigurationInterface(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~ConfigurationInterface(void);

      //////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Configuration_I::load() function
      //////////////////////////////////////////////////////////////////////////
      virtual void load(void);


   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Member variable containing an XML file that will be parsed and have
      /// settings extracted from it
      //////////////////////////////////////////////////////////////////////////
      Psb::XmlConfigurationFile* mp_configFile;


   private:
      DISALLOW_COPY_CTOR(ConfigurationInterface);
      DISALLOW_ASSIGNMENT_OPER(ConfigurationInterface);
      
}; // END class ConfigurationInterface

} // END namespace Autonomous
} // END namespace Subsytem
} // END namespace Psb


#endif // __AutonomousConfigurationInterface_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

