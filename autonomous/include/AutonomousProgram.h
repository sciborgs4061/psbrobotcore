////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AutonomousProgram_h__
#define __AutonomousProgram_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <list>
#include <string>
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
class Step;
class Action;
class Condition;


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The Program class encapsulates a single program segment that can be
/// executed during the autonomous portion of the FRC match.
////////////////////////////////////////////////////////////////////////////////
class Program
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @enum
      /// This enumeration defines all of the FSM states needed to execute an
      /// autonomous program from start to finish.
      //////////////////////////////////////////////////////////////////////////
      enum fsm_state_t
      {
         NO_INIT_STATE,   ///< Waiting for initialize() to be called, busy-state
         READY_STATE,     ///< On first run() will go to running
         RUNNING_STATE,   ///< Actively processing steps
         COMPLETE_STATE,  ///< Program end-point - could be reinitialized

         // Add autonomous program states above this line
         NUM_PROGRAM_STATES
      };
      
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~Program(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the name of the program to the caller.
      //////////////////////////////////////////////////////////////////////////
      std::string getName(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Performs all actions on all steps, sequentially, until all steps are
      /// complete.
      /// 
      /// @remarks
      /// This method method is called repeatedly from the main robot control
      /// loop.
      ///
      /// @see
      /// SciBorgRobot::AutonomousPeriodic()
      //////////////////////////////////////////////////////////////////////////
      void run(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Returns the running status of the program to the caller.
      ///
      /// @retval false   The autonomous program is still running.
      /// @retval true    The autonomous program is complete.
      //////////////////////////////////////////////////////////////////////////
      bool isProgramCompleted(void) const;
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method is used to initialize an Autonomous Program.
      /// 
      /// @remarks
      /// It is expected that after this method is called, all program steps, 
      /// actions, and conditions have been added to the program, with the
      /// desired transitions.  This includes the entry step and the current
      /// step.
      /// 
      /// @remarks
      /// The point of this method is to call the initializeImpl() method which
      /// is implemented in derived classes of AutonomousProgram, and then to
      /// initialize class level data such as the FSM state.
      ///
      /// @see
      /// initializeImpl()
      //////////////////////////////////////////////////////////////////////////
      void initialize(void);
      
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method is used to reinitialize an Autonomous Program to its
      /// just-initialized state.
      /// 
      /// @remarks
      /// The current step will be reset to the entry Step.  All existing Step
      /// objects will be left intact, but they will be reinitialized.
      //////////////////////////////////////////////////////////////////////////
      void reinitialize(void);

   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method is implemented in all concrete autonomous program classes
      /// and is responsible for bringing that program to a usable/runnable
      /// state.
      /// 
      /// @remarks
      /// It is expected that after this method is called, all program steps, 
      /// actions, and conditions have been added to the program, in the desired
      /// order.
      //////////////////////////////////////////////////////////////////////////
      virtual void initializeImpl(void) = 0;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Adds a program step to the end of the current list of steps.
      /// 
      /// @remarks
      /// The current step is executed until the conditions attached to that
      /// step have been met.  While on a given step, all associated actions are
      /// continuously performed.
      //////////////////////////////////////////////////////////////////////////
      void addStep(Step* p_step);

      // These functions allow a single tracking point for memory cleanup
      void catalogueAction(Action* p_action);
      void catalogueCondition(Condition* p_condition);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Selects starting (entry) step as the passed in step.
      ///
      /// @remarks
      /// Autonomous Programs are basically just a directed graph of Steps.  The
      /// starting step is not readily known and must be explicitly identified.
      //////////////////////////////////////////////////////////////////////////
      void setEntryStep(Step* p_step);


   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Creates a Program object with an empty step list.
      ///
      /// @param[in] rc_name   The name of the program.
      //////////////////////////////////////////////////////////////////////////
      Program(std::string const& rc_name);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Changes the program name after creation - only used by derived classes.
      ///
      /// @param[in] rc_name   The new name of the program.
      //////////////////////////////////////////////////////////////////////////
      void setName(std::string const& rc_name);
      
   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The table of step objects that make up the auto program.
      //////////////////////////////////////////////////////////////////////////
      typedef std::list<Step*> step_list_t;
      step_list_t m_stepList;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// A list of all actions in any/all steps
      //////////////////////////////////////////////////////////////////////////
      typedef std::list<Action*> action_list_t;
      action_list_t m_actionList;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// A list of all conditions to/from any/all steps
      //////////////////////////////////////////////////////////////////////////
      typedef std::list<Condition*> condition_list_t;
      condition_list_t m_conditionList;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The current Autonomous program execution FSM state.
      //////////////////////////////////////////////////////////////////////////
      fsm_state_t m_currentProgramState;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The entry step for the program - might be the current step.
      ///
      /// @remarks
      /// If the entry step is set to NULL, then it is assumed that the program
      /// has not yet been initialized.
      //////////////////////////////////////////////////////////////////////////
      Step* mp_entryStep;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The currently-executing step of the program.
      ///
      /// @remarks
      /// If the current step is set to NULL, then it is assumed that the
      /// program has run to completion.
      //////////////////////////////////////////////////////////////////////////
      Step* mp_currentStep;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// The name of the program.
      //////////////////////////////////////////////////////////////////////////
      std::string m_programName;
      

   private:
      // Hidden from public view.
      DISALLOW_DEFAULT_CTOR(Program);
      DISALLOW_COPY_CTOR(Program);
      DISALLOW_ASSIGNMENT_OPER(Program);
}; // END class Program


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

#endif  // __AutonomousProgram_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

