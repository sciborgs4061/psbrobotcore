////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <algorithm>
#include <PsbLogger.h>
#include "AutonomousStep.h"
#include "AutonomousAction.h"
#include "AutonomousCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 10u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
Step::Step(const std::string& rc_name)
   : m_stepName(rc_name)
   , m_actionList()
   , m_conditionList()
   , m_transitionTable()
   , mp_nextStep(NULL)
{
   m_actionList.clear();
   m_conditionList.clear();
   m_transitionTable.clear();
}


////////////////////////////////////////////////////////////////////////////////
Step::~Step(void)
{
   // Just clear the lists, the step doesn't own the memory for actions or conditions
   m_actionList.clear();
   m_conditionList.clear();

   // Do NOT free memory in this table, it would be a double-delete!
   m_transitionTable.clear();
}


////////////////////////////////////////////////////////////////////////////////
void
Step::addAction(Action* p_action)
{
   // Sanity Check the Input
   if (NULL != p_action)
   {
      // Add the action...
      m_actionList.push_back(p_action);

      PSB_LOG_INFO(3,
                   Psb::LOG_TYPE_STRING, m_stepName.c_str(),
                   Psb::LOG_TYPE_STRING, p_action->getName().c_str(),
                   Psb::LOG_TYPE_INT32,  m_actionList.size());
   }
   else
   {
      // Invalid attempt to add NULL action to this step
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, m_stepName.c_str(),
                    Psb::LOG_TYPE_INT32,  m_actionList.size());
   }
   
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Step::addTransition(Condition* p_condition,
                    Step*      p_step)
{
   // Sanity Check the Inputs
   if ( (NULL != p_condition) &&
        (NULL != p_step) )
   {
      // Make sure we don't yet know about the Condition
      if (m_conditionList.end() == std::find(m_conditionList.begin(),
                                             m_conditionList.end(),
                                             p_condition))
      {
         // Adding transition from this step to another
         PSB_LOG_INFO(3,
                      Psb::LOG_TYPE_STRING, m_stepName.c_str(),
                      Psb::LOG_TYPE_STRING, p_step->getName().c_str(),
                      Psb::LOG_TYPE_STRING, p_condition->getName().c_str());

         // First, add the condition to our condition list
         m_conditionList.push_back(p_condition);

         // Secondly, add the transition association
         typedef std::pair<Condition*, Step*> transition_t;
         m_transitionTable.insert(transition_t(p_condition, p_step));
      }
      else
      {
         // Duplicate Transition!
         PSB_LOG_ERROR(3,
                       Psb::LOG_TYPE_STRING, m_stepName.size(),
                       Psb::LOG_TYPE_STRING, p_condition->getName().c_str(),
                       Psb::LOG_TYPE_STRING, p_step->getName().c_str());
      }

      /// @todo
      /// Allow or disallow transitions to self?
   }
   else
   {
      // Attempting to add a Transition with either a NULL Condition or Step
      PSB_LOG_ERROR(3,
                    Psb::LOG_TYPE_STRING, m_stepName.c_str(),
                    Psb::LOG_TYPE_HEX32,  p_condition,
                    Psb::LOG_TYPE_HEX32,  p_step);
   }
   
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Step::initialize(void)
{
   this->initializeActions();
   this->initializeConditions();
   mp_nextStep = NULL;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Step::performActions(void)
{
   // //////////////////////////////////////////////////////////////////////////
   // Here, we iterate over all registered actions and perform each one.  The
   //  actions will be performed in the order they are registered.  However,
   //  all actions are merely a set of pre-determined inputs.  So, to the outside
   //  observer, all actions appear to take affect simultaneously.
   // //////////////////////////////////////////////////////////////////////////
   
   if (false == m_actionList.empty())
   {
      // We have actions to perform, so perform them one after the other
      for (auto p_action : m_actionList)
      {
         p_action->perform();
      }
   }
   else
   {
      // Our action list is empty!
      // This isn't necessarily an error, but is not expected
      PSB_LOG_NOTICE(1,
                     Psb::LOG_TYPE_STRING, m_stepName.c_str());
   }
   
   return;
}


////////////////////////////////////////////////////////////////////////////////
std::string
Step::getName(void) const
{
   return m_stepName;
}


////////////////////////////////////////////////////////////////////////////////
Step*
Step::getNextStep(void) const
{
   return mp_nextStep;
}


////////////////////////////////////////////////////////////////////////////////
bool
Step::isComplete(void)
{
   bool is_complete = false;
   
   if (false == m_conditionList.empty())
   {
      // We have conditions to evaluate.
      // This step is complete when any one of its exit-conditions is met
      for (auto p_condition : m_conditionList)
      {
         if (true == p_condition->isConditionMet())
         {
            // /////////////////////////////////////////////////////////////////
            // We've found a condition that has been met!  And we have to find
            // out where we are going next, so look up the next step in the
            // transition table, via the condition that is met.
            // /////////////////////////////////////////////////////////////////
            is_complete = true;

            transition_table_t::iterator iter =
               m_transitionTable.find(p_condition);

            if (m_transitionTable.end() != iter)
            {
               // Found it!  We have a transition
               mp_nextStep = iter->second;
               PSB_LOG_INFO(3,
                            Psb::LOG_TYPE_STRING, m_stepName.c_str(),
                            Psb::LOG_TYPE_STRING, p_condition->getName().c_str(),
                            Psb::LOG_TYPE_STRING, mp_nextStep->getName().c_str());
            }
            else
            {
               // This is not an error condition... but it is a little strange
               // We would expect all conditions to have transitions to other
               // steps... or steps that simply have conditions that will never
               // be met (e.g. ConstCondition)
               mp_nextStep = NULL;
               PSB_LOG_INFO(2,
                            Psb::LOG_TYPE_STRING, m_stepName.c_str(),
                            Psb::LOG_TYPE_STRING, p_condition->getName().c_str());
            }

            break;
         }
      }
   }
   else
   {
      // ///////////////////////////////////////////////////////////////////////
      // An empty condition list basically means there are no transitions for
      // this step to another... this is an end-step.  This is not an error in 
      // the program, but this is an interesting scenario.  Since this is an
      // "end" step, we declare the step complete.
      // ///////////////////////////////////////////////////////////////////////
      PSB_LOG_NOTICE(1,
                     Psb::LOG_TYPE_STRING, m_stepName.c_str());
      is_complete = true;
   }
   
   return is_complete;
}


// /////////////////////////////////////////////////////////////////////////////
// .............................................................................
// .......######................................................................
// .......#.....#....#####.....#....#....#......##......#####....######.........
// .......#.....#....#....#....#....#....#.....#..#.......#......#..............
// .......######.....#....#....#....#....#....#....#......#......#####..........
// .......#..........#####.....#....#....#....######......#......#..............
// .......#..........#...#.....#.....#..#.....#....#......#......#..............
// .......#..........#....#....#......##......#....#......#......######.........
// .............................................................................
// /////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
void
Step::initializeActions(void)
{
   for (auto p_action : m_actionList)
   {
      p_action->initialize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Step::initializeConditions(void)
{
   for (auto p_condition : m_conditionList)
   {
      p_condition->initialize();
   }
   
   return;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

