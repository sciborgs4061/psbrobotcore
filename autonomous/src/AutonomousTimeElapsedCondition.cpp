////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <PsbTimer.h>
#include "AutonomousTimeElapsedCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 11u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
TimeElapsedCondition::
TimeElapsedCondition(double waitTime)
   : Condition(__FUNCTION__)
   , m_waitTimeInSeconds(waitTime)
   , mp_timer(NULL)
{
   mp_timer = new Psb::Timer();
}


////////////////////////////////////////////////////////////////////////////////
TimeElapsedCondition::
~TimeElapsedCondition(void)
{
   delete mp_timer;
   mp_timer = NULL;
}


////////////////////////////////////////////////////////////////////////////////
void
TimeElapsedCondition::initialize(void)
{
   // Clear out and start the timer
   mp_timer->setExpiration(m_waitTimeInSeconds);
   mp_timer->reset();
   mp_timer->start();
   return;
}


////////////////////////////////////////////////////////////////////////////////
bool
TimeElapsedCondition::isConditionMet(void)
{
   // //////////////////////////////////////////////////////////////////////////
   // The WaitForTime condition is met once the provided wait time has elapsed.
   //  We can check this condition by starting a stop-watch timer in the
   //  initialize() method and seeing whether or not the HasPeriodPassed()
   //  method returns true for the given wait time.
   // //////////////////////////////////////////////////////////////////////////
   
   // Read the limit switch state from the InputFacade
   bool is_condition_met = (mp_timer->isExpired());
   
   // Post an entry to the debug log...
   PSB_LOG_DEBUG1(3,
                  Psb::LOG_TYPE_FLOAT64, mp_timer->getElapsedTime(),
                  Psb::LOG_TYPE_FLOAT64, m_waitTimeInSeconds,
                  Psb::LOG_TYPE_BOOL,    is_condition_met);
                
   // And we're done... return the indication
   return is_condition_met;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

