////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include "GlobalOiWpiStore.h"
#include "AutonomousOIDataFakerAction.h"
#include <assert.h>


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 8u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
OIDataFakerAction::OIDataFakerAction(action_description_t const& rc_desc)
   : Action(__FUNCTION__)
   , m_params(rc_desc.params)
{
}


////////////////////////////////////////////////////////////////////////////////
OIDataFakerAction::~OIDataFakerAction(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
OIDataFakerAction::initialize(void)
{
   GlobalOiWpiStore *oi = GlobalOiWpiStore::instance();

   for (auto& param_pair : m_params)
   {
      // WARNING: We are ASSUMING all OI Data Grabbers are Joystick
      //          DataGrabbers.  Probably valid now...but you never know.
      JoystickDataGrabber *grabber = 
         static_cast<JoystickDataGrabber *>(oi->getGrabberByName(param_pair.first));

      if (grabber != NULL)
      {
         m_fakers.push_back(new JoystickDataFaker(grabber,param_pair.second));
      }
      else
      {
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_STRING, param_pair.first.c_str());
      }
   }
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
OIDataFakerAction::perform(void)
{
   for (auto faker : m_fakers)
   {
      faker->fake_it();
   }
   return;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

