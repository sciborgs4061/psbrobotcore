// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// This is a good place to actually build the dashstream interface
// /////////////////////////////////////////////////////////////////////////////
#define BUILD_IT
#include "AutonomousDashstreamInterface.h"

// don't want to build it again if someone happens to
// include things again
#undef BUILD_IT

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstddef>
#include <cassert>
#include <list>
#include <PsbLogger.h>
#include "AutonomousController.h"
#include "AutonomousXmlProgram.h"

////////////////////////////////////////////////////////////////////////////////
// Define(s)
////////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + CONTROLLER)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


// /////////////////////////////////////////////////////////////////////////////
// Static Data
// /////////////////////////////////////////////////////////////////////////////
static const std::string s_XmlProgramNamePrefix =
#ifndef PSB_UNIT_TEST
   // Production Code Version!
   "/home/lvuser/psb_settings/";
#else
   // Unit test version!
   "/tmp/";
#endif


////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Psb::Subsystem::Controller_I()
   , m_configurationIntf()
   , m_nextProgramBtnState(BUTTON_RELEASED)
   , m_prevProgramBtnState(BUTTON_RELEASED)
   , m_lockInSwitchState(SWITCH_OPEN)
   , m_frcMatchState(MATCH_STATE_DISABLED)
   , m_programTable()
   , m_selectedProgramIndex(0u)
   , m_lockedInProgramIndex(0u)
   , m_selectedProgramName("")
   , m_lockedInProgramName("")
   , mp_dashstreamIntf(NULL)
   , m_data(NULL)
{

   mp_dashstreamIntf = new DashstreamInterface;
   m_data = (DashstreamInterface::DashstreamPOD_t *)mp_dashstreamIntf->getData();
   memset(m_data,0,sizeof(DashstreamInterface::DashstreamPOD_t));

   // Read subsystem settings from the XML file
   m_configurationIntf.load();

   // Build our program table - "SitStill" MUST be the first program
   // ...it would be awesome to just read from the directory
   //    and make sure you number them like "1 - sitstill.xml"
   //    so they show up in the right order...then you don't
   //    have to modify code to add a new program.
   std::list<std::string> short_prgm_name_list;
   short_prgm_name_list.push_back("SitStill.xml");
   short_prgm_name_list.push_back("MoveABit.xml");

   for (auto name : short_prgm_name_list)
   {
      // Build the proper program file path
      std::string xml_filename = s_XmlProgramNamePrefix + name;

      // Create the program & initialize it - VERY IMPORTANT to do this just once
      XmlProgram* p_program = new XmlProgram(xml_filename);
      p_program->initialize();

      // Add the program to our table
      m_programTable.push_back(p_program);
   }

   // Update the program names to start
   m_selectedProgramName  = "";
   m_selectedProgramName += "0 - ";
   m_selectedProgramName += m_programTable[0]->getName();

   m_lockedInProgramName  = "";
   m_lockedInProgramName += "0 - ";
   m_lockedInProgramName += m_programTable[0]->getName();

   // Cap size if needed
   if (m_selectedProgramName.size() > 31) { m_selectedProgramName.resize(31); }
   if (m_lockedInProgramName.size() > 31) { m_lockedInProgramName.resize(31); }

   // take the local members and shove them into the data values
   // so we can see them on the dashboard
   strncpy(m_data->selectedProgram,m_selectedProgramName.c_str(),31);
   strncpy(m_data->lockedProgram,m_lockedInProgramName.c_str(),31);
   m_data->selectedProgram[31] = '\0';
   m_data->lockedProgram[31] = '\0';
}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
   for (auto p_program : m_programTable)
   {
      delete p_program;
   }
}


////////////////////////////////////////////////////////////////////////////////
Psb::Subsystem::Dashstream::BaseInterface_I *
Controller::getDashstreamInterface(void) 
{
   return mp_dashstreamIntf;
}

////////////////////////////////////////////////////////////////////////////////
void 
Controller::init(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void 
Controller::performManualUpdate(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void 
Controller::performAutoUpdate(void)
{
   switch (m_frcMatchState)
   {
      case MATCH_STATE_DISABLED:
      {
         // There's a lot to do here, rely on the helper
         // this->performUpdateDisabled();
      }
      break;

      case MATCH_STATE_AUTONOMOUS:
      {
         if (MATCH_STATE_DISABLED == m_lastFrcMatchState)
         {
            // We just landed in autonomous, re-init the program!
            m_programTable[m_lockedInProgramIndex]->reinitialize();
         }

         // And run it
         m_programTable[m_lockedInProgramIndex]->run();
      }
      break;

      case MATCH_STATE_TELEOPERATED:
      {
         // This is a coding defect!
         // Don't run the autonomous subsystem in TELEOP!
         PSB_LOG_ERROR(0);
      }
      break;

      case MATCH_STATE_TEST:
      {
         // This is a coding defect!
         // Don't run the autonomous subsystem in TEST!
         PSB_LOG_ERROR(0);
      }
      break;

      default:
      {
         // This is a coding defect!
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, m_frcMatchState);
      }
      break;

   }

   // sync these up
   m_lastFrcMatchState = m_frcMatchState;
}

////////////////////////////////////////////////////////////////////////////////
void
Controller::transitionToState(subsystem_state_t new_state)
{
   // keep track of the old state just in case
   subsystem_state_t prev_state = m_state;

   // update the base class member so the update() function calls
   // the right thing
   m_state = new_state;

   /// now do whatever else it is you need to do during a transiton
   switch(m_state)
   {
      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;
      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;
      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         switch(prev_state)
         {
            case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
            break;

            case Psb::Subsystem::SUBSYSTEM_DISABLED:
            break;

            case Psb::Subsystem::SUBSYSTEM_MANUAL:
            break;

            default:
            {
               // Unknown state!  This is a coding defect!
               PSB_LOG_ERROR(2,
                             Psb::LOG_TYPE_INT32, m_state,
                             Psb::LOG_TYPE_INT32, new_state);
               m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
            } break;
         }
      }
      break;

      default:
      {
         // Unknown state!  This is a coding defect!
         PSB_LOG_ERROR(1,
                       Psb::LOG_TYPE_INT32, m_state,
                       Psb::LOG_TYPE_INT32, new_state);
         m_state = Psb::Subsystem::SUBSYSTEM_DISABLED;
      }
      break;
   }
}

////////////////////////////////////////////////////////////////////////////////
void Controller::setEnabledState(enabled_state_t /*new_state*/)
{
}

//////////////////////////////////////////////////////////////////////////
void Controller::setFrcMatchState(frc_match_state_t new_state)
{
   // keep the old one around for comparison
   m_lastFrcMatchState = m_frcMatchState;
   m_frcMatchState = new_state;
}

////////////////////////////////////////////////////////////////////////////////
   /* TODO ...I don't think we need this...I think the base
    * update function is going to take care of it by calling
    * the functions above here...but need to deal with that...
    * ....BUT...maybe Auto does something special here and
    *     needs to override it...time will tell
void 
Controller::update(void)
{
   Operator_I& r_oi = 
      InterfaceContainer::instance()->getOperatorInterface();

   // Grab the new match state
   frc_match_state_t new_match_state = r_oi.getFrcMatchState();

   switch (new_match_state)
   {
      case MATCH_STATE_DISABLED:
      {
         // There's a lot to do here, rely on the helper
         this->performUpdateDisabled();
      } break;

      case MATCH_STATE_AUTONOMOUS:
      {
         if (MATCH_STATE_DISABLED == m_frcMatchState)
         {
            // We just landed in autonomous, re-init the program!
            m_programTable[m_lockedInProgramIndex]->reinitialize();
         }

         // And run it
         m_programTable[m_lockedInProgramIndex]->run();
      } break;

      case MATCH_STATE_TELEOPERATED:
      {
         // This is a coding defect!
         // Don't run the autonomous subsystem in TELEOP!
         PSB_LOG_ERROR(0);
      } break;

      case MATCH_STATE_TEST:
      {
         // This is a coding defect!
         // Don't run the autonomous subsystem in TEST!
         PSB_LOG_ERROR(0);
      } break;

      default:
      {
         // This is a coding defect!
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, new_match_state);
      } break;
   }

   PSB_LOG_DEBUG1(6,
         Psb::LOG_TYPE_INT32,  m_selectedProgramIndex,
         Psb::LOG_TYPE_STRING, m_selectedProgramName.c_str(),
         Psb::LOG_TYPE_INT32,  m_lockedInProgramIndex,
         Psb::LOG_TYPE_STRING, m_lockedInProgramName.c_str(),
         Psb::LOG_TYPE_INT32,  m_frcMatchState,
         Psb::LOG_TYPE_INT32,  new_match_state);

   // Keep track of the match state for next time
   m_frcMatchState = new_match_state;

   return;
}

   */

////////////////////////////////////////////////////////////////////////////////
void
Controller::performDisabledUpdate(void)
{
   // TODO OK...here we need to deal with the actual buttons
   // and switches to allow the user to pick new programs and lock them
   // in.  We had a discussion about how the new automatic dashboard/oi grabber
   // interface may need the ability to have a "last value" baked into it
   // because we use that a lot to do edge triggered things.
   // This is an example of one of those cases.  We need to see the button
   // go from not pressed to pressed and then make a decision.
   // ...I think there is an easy way to make that happen...perhaps
   // we just do the right kind of grabber...or we build a separate
   // class that we can use if we wan't edge triggered.
   // I could also see one that automatically debounces...cool...stuff TODO!

   // for now...you can't pick a different probram
   // ...and there are a bunch of member variables that should go away
   // and start using the POD structure
   
   /* TODO
   Operator_I& r_oi = 
      InterfaceContainer::instance()->getOperatorInterface();

   // //////////////////////////////////////////////////////////////////////////
   // Handle buttons for the "selected" program
   // //////////////////////////////////////////////////////////////////////////
   button_state_t new_next_prgm_btn_state = r_oi.getNextProgramButtonState();
   button_state_t new_prev_prgm_btn_state = r_oi.getPrevProgramButtonState();

   if ( (BUTTON_RELEASED == m_nextProgramBtnState) &&
        (BUTTON_PRESSED  == new_next_prgm_btn_state) )
   {
      // Increment the program index, and wrap to beginning if needed
      m_selectedProgramIndex =
         (m_selectedProgramIndex < (m_programTable.size() - 1))
         ? m_selectedProgramIndex + 1  // Still room to increment
         : 0;                          // Wrap around (end --> begin)
   }
   else if ( (BUTTON_RELEASED == m_prevProgramBtnState) &&
             (BUTTON_PRESSED  == new_prev_prgm_btn_state) )
   {
      // Decrement the program index, and wrap to beginning if needed
      m_selectedProgramIndex =
         (m_selectedProgramIndex > 0)
         ? m_selectedProgramIndex - 1  // Still room to decrement
         : m_programTable.size() - 1;  // Wrap around (begin --> end)
   }
   else
   {
      // Don't change the program number, no button pushes
   }


   // //////////////////////////////////////////////////////////////////////////
   // Handle the lock-in switch for the "Locked in" program
   // //////////////////////////////////////////////////////////////////////////
   switch_state_t new_lockin_switch_state = r_oi.getLockInSwitchState();
   if ( (SWITCH_CLOSED == new_lockin_switch_state) &&
        (SWITCH_OPEN   == m_lockInSwitchState) )
   {
      // Yep, Operator wants to lock in a program
      m_lockedInProgramIndex = m_selectedProgramIndex;
   }
   else if (SWITCH_OPEN == new_lockin_switch_state)
   {
      // Autonomous lock-in switch is UNLOCKED, make sure we sit still
      m_lockedInProgramIndex = 0;
   }
   else
   {
      // Don't change the locked-in program index
   }


   // //////////////////////////////////////////////////////////////////////////
   // Update our selected/locked program names
   // //////////////////////////////////////////////////////////////////////////
   m_selectedProgramName  = "";
   m_selectedProgramName += std::to_string(m_selectedProgramIndex);
   m_selectedProgramName += " - ";
   m_selectedProgramName += m_programTable[m_selectedProgramIndex]->getName();

   m_lockedInProgramName  = "";
   m_lockedInProgramName += std::to_string(m_lockedInProgramIndex);
   m_lockedInProgramName += " - ";
   m_lockedInProgramName += m_programTable[m_lockedInProgramIndex]->getName();

   // Cap size if needed
   if (m_selectedProgramName.size() > 31) { m_selectedProgramName.resize(31); }
   if (m_lockedInProgramName.size() > 31) { m_lockedInProgramName.resize(31); }


   // Keep track of button/switch states for time time through the loop
   m_nextProgramBtnState = new_next_prgm_btn_state;
   m_prevProgramBtnState = new_prev_prgm_btn_state;
   m_lockInSwitchState   = new_lockin_switch_state;
   */
   return;
}


////////////////////////////////////////////////////////////////////////////////
std::string
Controller::getSelectedProgramName(void) const
{
   return m_selectedProgramName;
}


////////////////////////////////////////////////////////////////////////////////
std::string
Controller::getLockedProgramName(void) const
{
   return m_lockedInProgramName;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

