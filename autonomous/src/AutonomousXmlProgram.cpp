////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cassert>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#define RAPIDXML_NO_EXCEPTIONS
#include <rapidxml/rapidxml.hpp>
#include <PsbStringConverter.h>
#include <PsbLogger.h>

#include "AutonomousXmlProgram.h"
#include "AutonomousStep.h"
#include "AutonomousAction.h"
#include "AutonomousCondition.h"
#include "AutonomousCompositeCondition.h"
#include "AutonomousActionFactory.h"
#include "AutonomousConditionFactory.h"


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 13u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
XmlProgram::XmlProgram(const std::string& rc_xmlFile)
   : Program(__FUNCTION__)
   , m_xmlFile(rc_xmlFile)
   , m_wasParseErrorFound(false)
   , m_programStepsTable()
   , m_programActionsTable()
   , m_programConditionsTable()
   , m_programTransitionsTable()
{
   // Open the stream
   std::ifstream file_stream(rc_xmlFile, std::ifstream::binary);

   // If file exists
   if (true == file_stream.good())
   {
      // Find out how big the file is (in bytes)
      file_stream.seekg(0, file_stream.end);
      int length = file_stream.tellg();
      file_stream.seekg(0, file_stream.beg);

      // ///////////////////////////////////////////////////////////////////////
      // The RapidXML library forces the end user to present XML data in string
      // form in memory.  So we will read the contents of the XML file into a
      // heap-allocated character buffer of sufficient size (+1 for
      // NULL-termination safety).
      // ///////////////////////////////////////////////////////////////////////
      char* p_buffer = new char[length+1];
      file_stream.read(p_buffer, length);
      file_stream.close();
      p_buffer[length] = '\0';

      rapidxml::xml_document<char> doc;
      doc.parse<0>(p_buffer);
      this->parseXmlFile(doc);

      delete [] p_buffer;
   }
   else
   {
      // Could not open the file - we're dead
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_xmlFile.c_str());
   }
}


////////////////////////////////////////////////////////////////////////////////
XmlProgram::~XmlProgram(void)
{
}


////////////////////////////////////////////////////////////////////////////////
void
XmlProgram::initializeImpl(void)
{
   // //////////////////////////////////////////////////////////////////////////
   // XML file parsing as well as Step/Action/Condition object creation occurs
   // in the constructor.  Here, we are simply arranging the autonomous objects
   // in a meaningful way based on the information in the XML file.
   // //////////////////////////////////////////////////////////////////////////

   // Only perform true initialization if a parsing error was not detected
   if (false == m_wasParseErrorFound)
   {
      // ///////////////////////////////////////////////////////////////////////
      // Build all Step Objects
      // ///////////////////////////////////////////////////////////////////////
      for (auto& kv_pair : m_programStepsTable)
      {
         PSB_LOG_DEBUG1(3,
               Psb::LOG_TYPE_STRING, kv_pair.second.id.c_str(),
               Psb::LOG_TYPE_BOOL,   kv_pair.second.is_entry,
               Psb::LOG_TYPE_BOOL,   kv_pair.second.is_exit);

         // Build a step object and add it to the program
         step_description_t desc = kv_pair.second;
         this->addStep(desc.p_step);

         // And mark it as an entry step if needed
         if (true == desc.is_entry)
         {
            this->setEntryStep(desc.p_step);
         }
      }

      // ///////////////////////////////////////////////////////////////////////
      // Build and Associate All Action Objects
      // ///////////////////////////////////////////////////////////////////////
      for (auto& kv_pair : m_programActionsTable)
      {
         std::string param_str = "";
         for (auto& param_pair : kv_pair.second.params)
         {
            param_str += ", <";
            param_str += param_pair.first;
            param_str += "=";
            param_str += param_pair.second;
            param_str += ">";
         }

         PSB_LOG_DEBUG1(4,
               Psb::LOG_TYPE_STRING, kv_pair.second.id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.parent_id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.classname.c_str(),
               Psb::LOG_TYPE_STRING, param_str.c_str());


         // Add the action object to the proper step
         action_description_t desc = kv_pair.second;
         m_programStepsTable[desc.parent_id].p_step->addAction(desc.p_action);
      }

      // ///////////////////////////////////////////////////////////////////////
      // Build and Arrange all Conditions Objects
      // ///////////////////////////////////////////////////////////////////////
      for (auto& kv_pair : m_programConditionsTable)
      {
         std::string param_str = "";
         for (auto& param_pair : kv_pair.second.params)
         {
            param_str += ", <";
            param_str += param_pair.first;
            param_str += "=";
            param_str += param_pair.second;
            param_str += ">";
         }

         PSB_LOG_DEBUG1(6,
               Psb::LOG_TYPE_STRING, kv_pair.second.id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.parent_id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.type.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.operation.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.classname.c_str(),
               Psb::LOG_TYPE_STRING, param_str.c_str());

         // Build the condition hierarchy
         condition_description_t desc = kv_pair.second;
         if (desc.parent_id != "NULL")
         {
            CompositeCondition* p_parent = 
               dynamic_cast<CompositeCondition*>(m_programConditionsTable[desc.parent_id].p_condition);
            p_parent->addCondition(desc.p_condition);
         }
      }

      // ///////////////////////////////////////////////////////////////////////
      // Handle all Transitions
      // ///////////////////////////////////////////////////////////////////////
      for (auto& kv_pair : m_programTransitionsTable)
      {
         PSB_LOG_DEBUG1(4,
               Psb::LOG_TYPE_STRING, kv_pair.second.id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.fromstep_id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.tostep_id.c_str(),
               Psb::LOG_TYPE_STRING, kv_pair.second.condition_id.c_str());

         // Dig out the appropriate information
         transition_description_t desc = kv_pair.second;
         Step*      p_from_step = m_programStepsTable[desc.fromstep_id].p_step;
         Step*      p_to_step   = m_programStepsTable[desc.tostep_id].p_step;
         Condition* p_condition = m_programConditionsTable[desc.condition_id].p_condition;

         // And note the transition
         p_from_step->addTransition(p_condition, p_to_step);
      }
   }
   else
   {
      // XML Parsing Error found - not building program!
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, m_xmlFile.c_str());
   }
   return;
}



////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Parse the Autonomous Program XML definition file.
///
/// @remarks
/// The XML file conforms to this schema:
///
/// @code
///            @todo   @todo   @todo   @todo   @todo 
///            @todo   @todo   @todo   @todo   @todo 
///            @todo   @todo   @todo   @todo   @todo 
/// @endcode
///
////////////////////////////////////////////////////////////////////////////////
void
XmlProgram::parseXmlFile(rapidxml::xml_document<char>& r_doc)
{
   // Get root node, "Program"
   rapidxml::xml_node<char>* p_root_node       = r_doc.first_node("Program");
   rapidxml::xml_node<char>* p_step_node       = NULL;
   rapidxml::xml_node<char>* p_action_node     = NULL;
   rapidxml::xml_node<char>* p_condition_node  = NULL;
   rapidxml::xml_node<char>* p_transition_node = NULL;

   // We know the name of the program now...
   this->setName(p_root_node->first_attribute("name")->value());

   // Parse all step objects (steps contain actions)
   p_step_node = p_root_node->first_node("Step");
   while (NULL != p_step_node)
   {
      // Work through this step...
      this->parseXmlStepNode(p_step_node);

      // Parse all action objects
      p_action_node = p_step_node->first_node("Action");
      while (NULL != p_action_node)
      {
         this->parseXmlActionNode(p_action_node);
         p_action_node = p_action_node->next_sibling("Action");
      }

      p_step_node = p_step_node->next_sibling("Step");
   }

   // Parse all condition objects
   p_condition_node = p_root_node->first_node("Condition");
   this->parseXmlConditionNode(p_condition_node);

   // Parse all transition objects
   p_transition_node = p_root_node->first_node("Transition");
   while (NULL != p_transition_node)
   {
      this->parseXmlTransitionNode(p_transition_node);
      p_transition_node = p_transition_node->next_sibling("Transition");
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
XmlProgram::parseXmlStepNode(rapidxml::xml_node<char>* p_node)
{
   /// @warning
   /// This function assumes XML that conforms to our schema!

   // Fill out a step description instance for this step
   step_description_t desc;
   desc.id       = p_node->first_attribute("id")->value();
   desc.is_entry = Psb::StringConverter::toBoolean(p_node->first_attribute("entry")->value(), false);
   desc.is_exit  = Psb::StringConverter::toBoolean(p_node->first_attribute("exit")->value(), false);
   desc.p_step   = NULL;

   // Check to make sure we don't already have a step by this ID
   step_table_t::iterator iter = m_programStepsTable.find(desc.id);
   if (m_programStepsTable.end() == iter)
   {
      // This is a step we don't know about yet
      desc.p_step = new Step(desc.id);
      m_programStepsTable[desc.id] = desc;
   }
   else
   {
      // Duplicate step ID!
      PSB_LOG_ERROR(4,
                    Psb::LOG_TYPE_STRING, m_xmlFile.c_str(),
                    Psb::LOG_TYPE_STRING, desc.id.c_str(),
                    Psb::LOG_TYPE_BOOL,   desc.is_entry,
                    Psb::LOG_TYPE_BOOL,   desc.is_exit);
      m_wasParseErrorFound = true;
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
XmlProgram::parseXmlActionNode(rapidxml::xml_node<char>* p_node)
{
   /// @warning
   /// This function assumes XML that conforms to our schema!

   // Fill out an action description instance for this node
   action_description_t desc;
   desc.id        = p_node->first_attribute("id")->value();
   desc.parent_id = p_node->first_attribute("parent")->value();
   desc.classname = p_node->first_attribute("classname")->value();
   desc.p_action  = NULL;

   // Look for parameters if any exist
   rapidxml::xml_node<char>* p_param_node = p_node->first_node("param");
   while (NULL != p_param_node)
   {
      std::string param_id    = p_param_node->first_attribute("id")->value();
      std::string param_value = p_param_node->value();
      desc.params[param_id]   = param_value;
      p_param_node = p_param_node->next_sibling("param");
   }

   // Check to make sure we don't already have a action by this ID
   action_table_t::iterator iter = m_programActionsTable.find(desc.id);
   if (m_programActionsTable.end() == iter)
   {
      // This is an action we don't know about yet
      desc.p_action = ActionFactory::createAction(desc);
      m_programActionsTable[desc.id] = desc;

      // Catalog the created action for deletion later
      this->catalogueAction(desc.p_action);
   }
   else
   {
      // Duplicate action ID!
      PSB_LOG_ERROR(4,
                    Psb::LOG_TYPE_STRING, m_xmlFile.c_str(),
                    Psb::LOG_TYPE_STRING, desc.id.c_str(),
                    Psb::LOG_TYPE_STRING, desc.parent_id.c_str(),
                    Psb::LOG_TYPE_STRING, desc.classname.c_str());
      m_wasParseErrorFound = true;
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
XmlProgram::parseXmlConditionNode(rapidxml::xml_node<char>* p_node)
{
   if (NULL == p_node)
   {
      // This is the end of our recursion loop
      return;
   }

   /// @warning
   /// This function assumes XML that conforms to our schema!

   rapidxml::xml_node<char>* p_condition_node = p_node;
   while (NULL != p_condition_node)
   {
      // Recurse to any children conditions first!
      this->parseXmlConditionNode(p_condition_node->first_node("Condition"));

      // Fill out an condition description instance for this node
      condition_description_t desc;
      desc.id          = p_condition_node->first_attribute("id")->value();
      desc.parent_id   = p_condition_node->first_attribute("parent")->value();
      desc.type        = p_condition_node->first_attribute("type")->value();
      desc.operation   = p_condition_node->first_attribute("operation")->value();
      desc.classname   = p_condition_node->first_attribute("classname")->value();
      desc.p_condition = NULL;

      // Look for parameters if any exist
      rapidxml::xml_node<char>* p_param_node = p_condition_node->first_node("param");
      while (NULL != p_param_node)
      {
         std::string param_id    = p_param_node->first_attribute("id")->value();
         std::string param_value = p_param_node->value();
         desc.params[param_id]   = param_value;
         p_param_node = p_param_node->next_sibling("param");
      }

      // Check to make sure we don't already have a condition by this ID
      condition_table_t::iterator iter = m_programConditionsTable.find(desc.id);
      if (m_programConditionsTable.end() == iter)
      {
         // This is an condition we don't know about yet
         desc.p_condition = ConditionFactory::createCondition(desc);
         m_programConditionsTable[desc.id] = desc;

         // Catalog the created condition for deletion later
         this->catalogueCondition(desc.p_condition);
      }
      else
      {
         // Duplicate condition ID!
         PSB_LOG_ERROR(4,
                       Psb::LOG_TYPE_STRING, m_xmlFile.c_str(),
                       Psb::LOG_TYPE_STRING, desc.id.c_str(),
                       Psb::LOG_TYPE_STRING, desc.parent_id.c_str(),
                       Psb::LOG_TYPE_STRING, desc.type.c_str(),
                       Psb::LOG_TYPE_STRING, desc.operation.c_str(),
                       Psb::LOG_TYPE_STRING, desc.classname.c_str());
         m_wasParseErrorFound = true;
      }

      p_condition_node = p_condition_node->next_sibling("Condition");
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
XmlProgram::parseXmlTransitionNode(rapidxml::xml_node<char>* p_node)
{
   /// @warning
   /// This function assumes XML that conforms to our schema!

   // Fill out a transition description instance for this transition
   transition_description_t desc;
   desc.id           = p_node->first_attribute("id")->value();
   desc.fromstep_id  = p_node->first_attribute("fromstep")->value();
   desc.tostep_id    = p_node->first_attribute("tostep")->value();
   desc.condition_id = p_node->first_attribute("condition")->value();

   // Check to make sure we don't already have a transition by this ID
   transition_table_t::iterator iter = m_programTransitionsTable.find(desc.id);
   if (m_programTransitionsTable.end() == iter)
   {
      // This is a transition we don't know about yet
      m_programTransitionsTable[desc.id] = desc;
   }
   else
   {
      // Duplicate Transition ID!
      PSB_LOG_ERROR(5,
                    Psb::LOG_TYPE_STRING, m_xmlFile.c_str(),
                    Psb::LOG_TYPE_STRING, desc.id.c_str(),
                    Psb::LOG_TYPE_STRING, desc.fromstep_id.c_str(),
                    Psb::LOG_TYPE_STRING, desc.tostep_id.c_str(),
                    Psb::LOG_TYPE_STRING, desc.condition_id.c_str());
      m_wasParseErrorFound = true;
   }

   return;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

