////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cassert>
#include <PsbLogger.h>
#include "AutonomousProgram.h"
#include "AutonomousStep.h"
#include "AutonomousAction.h"
#include "AutonomousCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 6u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
Program::Program(const std::string& rc_name)
   : m_stepList()
   , m_currentProgramState(NO_INIT_STATE)
   , mp_entryStep(NULL)
   , mp_currentStep(NULL)
   , m_programName(rc_name)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
Program::~Program(void)
{
   // Free up memory for program objects
   for (auto p_condition : m_conditionList) { delete p_condition; }
   for (auto p_action    : m_actionList)    { delete p_action;    }
   for (auto p_step      : m_stepList)      { delete p_step;      }

   // And don't forget the containers themselves
   m_conditionList.clear();
   m_actionList.clear();
   m_stepList.clear();

   // And since we no longer have any steps to speak of, all known steps are nil
   mp_entryStep   = NULL;
   mp_currentStep = NULL;
}


////////////////////////////////////////////////////////////////////////////////
std::string
Program::getName(void) const
{
   return m_programName;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::setName(std::string const& rc_name)
{
   m_programName = rc_name;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::run(void)
{
   // Default our next state to the current one
   fsm_state_t next_state = m_currentProgramState;

   switch (m_currentProgramState)
   {
      // ///////////////////////////////////////////////////////////////////////
      // In the NO_INIT_STATE, the autonomous program object is created, but not
      // yet populated.  We expect to have the program initialized by the
      // external runner (via the initialize()) method.  Once that happens, we
      // will land in the "READY" state.  Until then, never leave here.
      // ///////////////////////////////////////////////////////////////////////
      case NO_INIT_STATE:
      {
         // If run() is called while we are in this state, it indicated that
         // we forgot to call initialize() before calling run().  This is an
         // error condition.
         PSB_LOG_ERROR(1,
                       Psb::LOG_TYPE_STRING, m_programName.c_str());
      } break;

      // ///////////////////////////////////////////////////////////////////////
      // In the READY_STATE, the autonomous program object is should already be
      // populated.  If run() is called in this state, we initialize the current
      // step, and go to the RUNNING_STATE.
      // ///////////////////////////////////////////////////////////////////////
      case READY_STATE:
      {
         // Sanity check - initialize() should have set the entry state
         if (NULL != mp_currentStep)
         {
            // Initialize the current step, and move to the running state.
            PSB_LOG_INFO(2,
                         Psb::LOG_TYPE_STRING, m_programName.c_str(),
                         Psb::LOG_TYPE_STRING, mp_currentStep->getName().c_str());
            mp_currentStep->initialize();
            next_state = RUNNING_STATE;
         }
         else
         {
            // This program has no entry step!  Check initializeImpl()
            PSB_LOG_ERROR(1,
                          Psb::LOG_TYPE_STRING, m_programName.c_str());
            next_state = COMPLETE_STATE;
         }
      } break;
      
      
      // ///////////////////////////////////////////////////////////////////////
      // In the RUNNING_STATE, we continuously ask the step to perform all of
      // its associated actions via the performActions() method, until the step
      // advancement condition has been met, via the isConditionMet() method.
      // Once the condition is met, we move back to the PREPARE_STEP_STATE and
      // process the next step.
      // ///////////////////////////////////////////////////////////////////////
      case RUNNING_STATE:
      {
         // First, perform all the actions of this state
         mp_currentStep->performActions();

         // And check to see if the step is complete
         if (true == mp_currentStep->isComplete())
         {
            // The current step has completed...
            PSB_LOG_INFO(2,
                         Psb::LOG_TYPE_STRING, m_programName.c_str(),
                         Psb::LOG_TYPE_STRING, mp_currentStep->getName().c_str());
                        
            // Find out what the next step is
            mp_currentStep = mp_currentStep->getNextStep();

            if (NULL != mp_currentStep)
            {
               // We have a new current step, initialize it and keep running
               PSB_LOG_INFO(2,
                            Psb::LOG_TYPE_STRING, m_programName.c_str(),
                            Psb::LOG_TYPE_STRING, mp_currentStep->getName().c_str());
               mp_currentStep->initialize();
               next_state = RUNNING_STATE;
            }
            else
            {
               // The program has completed!
               PSB_LOG_INFO(1,
                            Psb::LOG_TYPE_STRING, m_programName.c_str());
               next_state = COMPLETE_STATE;
            }
         }
         else
         {
            // Current step is still running, come around next time
         }
      } break;
      
      
      // ///////////////////////////////////////////////////////////////////////
      // In the COMPLETE, the autonomous program has finished, and there is
      // nothing more to do.  However, we do want to handle the case where we
      // reach the complete state with a non-null current step.  This indicates
      // an error that should be diagnosed & repaired.
      // ///////////////////////////////////////////////////////////////////////
      case COMPLETE_STATE:
      {
         if (NULL != mp_currentStep)
         {
            // The program ended prematurely - coding ERROR!
            PSB_LOG_ERROR(3,
                          Psb::LOG_TYPE_STRING, m_programName.c_str(),
                          Psb::LOG_TYPE_STRING, mp_currentStep->getName().c_str(),
                          Psb::LOG_TYPE_INT32,  m_stepList.size());
         }
         else
         {
            // This is the expected case, nothing to do here
         }
      } break;
      
      
      // ///////////////////////////////////////////////////////////////////////
      // This default state is handled separately so as to call out errors in
      // processing.  If this happens, the program will automatically skip to
      // the COMPLETE_STATE and will not perform any remaining step actions.
      // ///////////////////////////////////////////////////////////////////////
      default:
      {
         // Something's gone wrong, move to complete
         PSB_LOG_ERROR(2,
                       Psb::LOG_TYPE_STRING, m_programName.c_str(),
                       Psb::LOG_TYPE_INT32,  m_currentProgramState);
         next_state = COMPLETE_STATE;
      } break;
   } // end switch()

   // Log state changes for debugging
   if (next_state != m_currentProgramState)
   {
      PSB_LOG_DEBUG1(3,
                     Psb::LOG_TYPE_STRING, m_programName.c_str(),
                     Psb::LOG_TYPE_INT32,  m_currentProgramState,
                     Psb::LOG_TYPE_INT32,  next_state);
   }

   // And update the current state
   m_currentProgramState = next_state;
   return;
}


////////////////////////////////////////////////////////////////////////////////
bool
Program::isProgramCompleted(void) const
{
   return (COMPLETE_STATE == m_currentProgramState);
}


////////////////////////////////////////////////////////////////////////////////
void
Program::initialize(void)
{
   switch (m_currentProgramState)
   {
      case NO_INIT_STATE:
      case READY_STATE:
      case COMPLETE_STATE:
      {
         // Initialization is allowed from these states
         this->initializeImpl();

         // Dump some debug info
         PSB_LOG_INFO(5,
                      Psb::LOG_TYPE_STRING, m_programName.c_str(),
                      Psb::LOG_TYPE_INT32,  m_stepList.size(),
                      Psb::LOG_TYPE_INT32,  m_currentProgramState,
                      Psb::LOG_TYPE_HEX32,  mp_currentStep,
                      Psb::LOG_TYPE_HEX32,  mp_entryStep);

         // The initialize() function is required to set these up.
         if ( (NULL != mp_currentStep) &&
              (NULL != mp_entryStep) )
         {
            // And note that we've been initialized
            m_currentProgramState = READY_STATE;
         }
         else
         {
            // Initialization has failed.
            PSB_LOG_ERROR(2,
                          Psb::LOG_TYPE_HEX32,  mp_currentStep,
                          Psb::LOG_TYPE_HEX32,  mp_entryStep);
            m_currentProgramState = NO_INIT_STATE;
         }
      } break;

      case RUNNING_STATE:
      {
         // ERROR - We do not reinitialize from the running state
         PSB_LOG_ERROR(2,
                       Psb::LOG_TYPE_STRING, m_programName.c_str(),
                       Psb::LOG_TYPE_INT32,  m_currentProgramState);
      } break;

      default:
      {
         // ERROR - This is an unknown state... coding defect!
         PSB_LOG_ERROR(2,
                       Psb::LOG_TYPE_STRING, m_programName.c_str(),
                       Psb::LOG_TYPE_INT32,  m_currentProgramState);
         assert(false);
      }
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::reinitialize(void)
{
   // Reset everything
   mp_currentStep        = mp_entryStep;
   m_currentProgramState = READY_STATE;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::addStep(Step* p_step)
{
   // Sanity check
   if (NULL != p_step)
   {
      // Post debug information
      PSB_LOG_INFO(3,
                   Psb::LOG_TYPE_STRING, m_programName.c_str(),
                   Psb::LOG_TYPE_STRING, p_step->getName().c_str(),
                   Psb::LOG_TYPE_INT32,  m_stepList.size());
      m_stepList.push_back(p_step);
   }
   else
   {
      // We are attempting to add a NULL Step to the list - ERROR!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, m_programName.c_str(),
                    Psb::LOG_TYPE_INT32,  m_stepList.size());
      assert(false);
   }
   
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::catalogueAction(Action* p_action)
{
   // Sanity check
   if (NULL != p_action)
   {
      // Post debug information
      PSB_LOG_INFO(3,
                   Psb::LOG_TYPE_STRING, m_programName.c_str(),
                   Psb::LOG_TYPE_STRING, p_action->getName().c_str(),
                   Psb::LOG_TYPE_INT32,  m_actionList.size());
      m_actionList.push_back(p_action);
   }
   else
   {
      // We are attempting to add a NULL Action to the list - ERROR!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, m_programName.c_str(),
                    Psb::LOG_TYPE_INT32,  m_actionList.size());
      assert(false);
   }
   
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::catalogueCondition(Condition* p_condition)
{
   // Sanity check
   if (NULL != p_condition)
   {
      // Post debug information
      PSB_LOG_INFO(3,
                   Psb::LOG_TYPE_STRING, m_programName.c_str(),
                   Psb::LOG_TYPE_STRING, p_condition->getName().c_str(),
                   Psb::LOG_TYPE_INT32,  m_conditionList.size());
      m_conditionList.push_back(p_condition);
   }
   else
   {
      // We are attempting to add a NULL Step to the list - ERROR!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, m_programName.c_str(),
                    Psb::LOG_TYPE_INT32,  m_conditionList.size());
      assert(false);
   }
   
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Program::setEntryStep(Step* p_step)
{
   // Sanity check
   if (NULL != p_step)
   {
      // Set the entry step to the provided step
      PSB_LOG_INFO(2,
                   Psb::LOG_TYPE_STRING, m_programName.c_str(),
                   Psb::LOG_TYPE_STRING, p_step->getName().c_str());
      mp_entryStep   = p_step;
      mp_currentStep = p_step;
   }
   else
   {
      // We are attempting to set the Entry Step to NULL - ERROR!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_STRING, m_programName.c_str(),
                    Psb::LOG_TYPE_INT32,  m_stepList.size());
      assert(false);
   }
   
   return;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

