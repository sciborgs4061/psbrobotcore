// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <PsbXmlConfigurationFile.h>
#include "AutonomousConfigurationInterface.h"


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + CONFIGURATION_INTERFACE)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
ConfigurationInterface::ConfigurationInterface(void)
   : Psb::Subsystem::Autonomous::Configuration_I()
   , mp_configFile(NULL)
{
   // Intentionally left empty
   /// @todo
   /// Put this back in when settings for the autonomous subsystem are needed
   /// mp_configFile = new Psb::XmlConfigurationFile("/home/lvuser/psb_settings/autonomous_settings.xml");
}


////////////////////////////////////////////////////////////////////////////////
ConfigurationInterface::~ConfigurationInterface(void)
{
   // Intentionally left empty
   /// @todo
   /// Put this back in when settings for the autonomous subsystem are needed
   /// delete mp_configFile;
   /// mp_configFile = NULL;
}


////////////////////////////////////////////////////////////////////////////////
void 
ConfigurationInterface::load(void)
{
   // Nothing to do just yet
   return;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

