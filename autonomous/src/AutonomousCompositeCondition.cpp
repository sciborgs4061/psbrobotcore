////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <functional>
#include <algorithm>
#include <cassert>
#include <PsbLogger.h>
#include "AutonomousCompositeCondition.h"


// /////////////////////////////////////////////////////////////////////////////
// Defines(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 2u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {


////////////////////////////////////////////////////////////////////////////////
CompositeCondition::CompositeCondition(const std::string& rc_name,
                                       operator_t logical_operation)
   : Condition(rc_name)
   , m_operator(logical_operation)
   , m_conditionList()
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
CompositeCondition::~CompositeCondition(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
CompositeCondition::addCondition(Condition* p_condition)
{
   if (NULL != p_condition)
   {
      m_conditionList.push_back(p_condition);
   }
   else
   {
      // Attempting to add NULL condition to the composite list
      PSB_LOG_ERROR(3,
                    Psb::LOG_TYPE_STRING, this->getName().c_str(),
                    Psb::LOG_TYPE_INT32,  m_operator,
                    Psb::LOG_TYPE_INT32,  m_conditionList.size());
      assert(false);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
CompositeCondition::initialize(void)
{
   // Initialize all conditions we know about
   for (auto p_cond : m_conditionList)
   {
      p_cond->initialize();
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet(void)
{
   bool is_met = false;

   // There is work to do if we have conditions
   if (false == m_conditionList.empty())
   {
      // The condition status is determined by the operation
      switch (m_operator)
      {
         case OPERATOR_OR:   { is_met = this->isConditionMet__OR();   } break;
         case OPERATOR_NOR:  { is_met = this->isConditionMet__NOR();  } break;
         case OPERATOR_XOR:  { is_met = this->isConditionMet__XOR();  } break;
         case OPERATOR_XNOR: { is_met = this->isConditionMet__XNOR(); } break;
         case OPERATOR_AND:  { is_met = this->isConditionMet__AND();  } break;
         case OPERATOR_NAND: { is_met = this->isConditionMet__NAND(); } break;

         default:
         {
            // ERROR: Invalid Operator type!
            PSB_LOG_ERROR(2,
                          Psb::LOG_TYPE_STRING, this->getName().c_str(),
                          Psb::LOG_TYPE_INT32,  m_operator);
            assert(false);
         }
      }
   }
   else
   {
      // We default to moving on if we have no data to operate on.
      is_met = true;
   }

   return is_met;
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet__OR(void)
{
   // Default to "not met"
   bool is_condition_met = false;

   // If any condition is met, we're done!
   for (auto p_cond : m_conditionList)
   {
      if (true == p_cond->isConditionMet())
      {
         // We found one, we're done.
         is_condition_met = true;
         break;
      }
   }

   return is_condition_met;
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet__NOR(void)
{
   return !(this->isConditionMet__OR());
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet__XOR(void)
{
   // Default to "not met"
   bool is_condition_met = false;

   // Create a lambda function to scan the condition list
   std::function<bool(Condition*)> condition_func =
      [/*None*/] (Condition* p_condition)
      {
         return p_condition->isConditionMet();
      };

   // Count up all the "true" conditions we have in our list
   int const c_num_trues = 
      std::count_if(m_conditionList.begin(),
                    m_conditionList.end(),
                    condition_func);

   // XOR logic means "exactly one" true
   is_condition_met = (1 == c_num_trues);
   return is_condition_met;
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet__XNOR(void)
{
   return !(this->isConditionMet__XOR());
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet__AND(void)
{
   // Default to "met"
   bool is_condition_met = true;

   // If any condition is met, we're done!
   for (auto p_cond : m_conditionList)
   {
      if (false == p_cond->isConditionMet())
      {
         // We found an unmet condition, we're done.
         is_condition_met = false;
         break;
      }
   }

   return is_condition_met;
}


////////////////////////////////////////////////////////////////////////////////
bool
CompositeCondition::isConditionMet__NAND(void)
{
   return !(this->isConditionMet__AND());
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

