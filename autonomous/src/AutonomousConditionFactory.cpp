////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbLogger.h>
#include <PsbStringConverter.h>
#include "AutonomousConditionFactory.h"
#include "AutonomousController.h"
#include "AutonomousCompositeCondition.h"
#include "AutonomousConstCondition.h"
#include "AutonomousTimeElapsedCondition.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (AUTONOMOUS + EXTRA_FILE + 14u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Autonomous {

////////////////////////////////////////////////////////////////////////////////
Condition*
ConditionFactory::createCondition(condition_description_t const& rc_desc)
{
   Condition* p_condition = NULL;

   // Create conditions that we support
   if ("CompositeCondition" == rc_desc.classname)
   {
      // We are building a CompositeCondition, and we need the inputs
      CompositeCondition::operator_t operation = CompositeCondition::OPERATOR_OR;
           if ("OR"   == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_OR;   }
      else if ("NOR"  == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_NOR;  }
      else if ("XOR"  == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_XOR;  }
      else if ("XNOR" == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_XNOR; }
      else if ("AND"  == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_AND;  }
      else if ("NAND" == rc_desc.operation)  { operation = CompositeCondition::OPERATOR_NAND; }
      else                                   { operation = CompositeCondition::OPERATOR_OR;   }

      // Build the object
      p_condition =
         new CompositeCondition(rc_desc.id, operation);
   }
   else if ("ConstCondition" == rc_desc.classname)
   {
      bool flag =
         Psb::StringConverter::toBoolean(rc_desc.params.at("flag").c_str(), true);
      p_condition = new ConstCondition(flag);
   }
   else if ("TimeElapsedCondition" == rc_desc.classname)
   {
      float wait_time = 
         Psb::StringConverter::toFloat(rc_desc.params.at("wait_time").c_str(), 1.0f);
      p_condition = new TimeElapsedCondition(wait_time);
   }
   /// @todo MORE SUPPORTED CONDITIONS HERE!
   //    ... change this to walk through some list of registered autonomous "functions"
   else
   {
      // Unknown condition!
      PSB_LOG_ERROR(5, Psb::LOG_TYPE_STRING, rc_desc.id.c_str(),
                       Psb::LOG_TYPE_STRING, rc_desc.parent_id.c_str(),
                       Psb::LOG_TYPE_STRING, rc_desc.type.c_str(),
                       Psb::LOG_TYPE_STRING, rc_desc.operation.c_str(),
                       Psb::LOG_TYPE_STRING, rc_desc.classname.c_str());

      // Build the object
      p_condition =
         new ConstCondition(true);
   }

   return p_condition;
}


} // END namespace Autonomous
} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

