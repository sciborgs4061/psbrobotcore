################################################################################
##
## License:
##   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
## 
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to deal
## in the Software without restriction, including without limitation the rights
## to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
## copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
## 
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
## 
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
## OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
## THE SOFTWARE.
##
################################################################################


################################################################################
# Directories
################################################################################
PROJECT_ROOT = ../..
SRC_DIR = ../src
BIN_DIR = ../bin


################################################################################
# Include Paths
################################################################################
INCLUDES  =
INCLUDES += -I$(PROJECT_ROOT)/autonomous/include
INCLUDES += -I$(PROJECT_ROOT)/subsystems-common/include
INCLUDES += -I$(PROJECT_ROOT)/dashstream/include
INCLUDES += -I$(PROJECT_ROOT)/logger/include
INCLUDES += -I$(PROJECT_ROOT)/psbxml/include
INCLUDES += -I$(PROJECT_ROOT)/wpilib/include


################################################################################
# Defines
################################################################################
DEFINES  =


################################################################################
# Library Tools
################################################################################
ARM_AR = arm-frc-linux-gnueabi-ar
X86_AR = ar


################################################################################
# Compiler & Flags
################################################################################
ARM_CXX = arm-frc-linux-gnueabi-g++
X86_CXX = g++

###############################################################
# These are specific for the X86 to force certain behavior
###############################################################
X86_CXXFLAGS  =
X86_CXXFLAGS += -DX86_PLATFORM

CXXFLAGS  =
CXXFLAGS += -gdwarf-2
CXXFLAGS += -O0
CXXFLAGS += -Wall
CXXFLAGS += -Wextra
# Can't use this because WPILib throws HUGE numbers of errors with this warning
# CXXFLAGS += -Weffc++
CXXFLAGS += -Werror
CXXFLAGS += -fno-exceptions
# What is this one for?
CXXFLAGS += -fmessage-length=0
CXXFLAGS += -std=c++1y
CXXFLAGS += $(INCLUDES)
CXXFLAGS += $(DEFINES)


################################################################################
# Source Code To Compile
################################################################################
SOURCES  =
SOURCES += $(SRC_DIR)/AutonomousAction.cpp
SOURCES += $(SRC_DIR)/AutonomousActionFactory.cpp
SOURCES += $(SRC_DIR)/AutonomousAuxDataInterface.cpp
SOURCES += $(SRC_DIR)/AutonomousCompositeCondition.cpp
SOURCES += $(SRC_DIR)/AutonomousCondition.cpp
SOURCES += $(SRC_DIR)/AutonomousConditionFactory.cpp
SOURCES += $(SRC_DIR)/AutonomousConfigurationInterface.cpp
SOURCES += $(SRC_DIR)/AutonomousConfiguration_I.cpp
SOURCES += $(SRC_DIR)/AutonomousConstCondition.cpp
SOURCES += $(SRC_DIR)/AutonomousContinueAction.cpp
SOURCES += $(SRC_DIR)/AutonomousController.cpp
SOURCES += $(SRC_DIR)/AutonomousDashstreamInterface.cpp
SOURCES += $(SRC_DIR)/AutonomousOIDataFakerAction.cpp
SOURCES += $(SRC_DIR)/AutonomousProgram.cpp
SOURCES += $(SRC_DIR)/AutonomousStep.cpp
SOURCES += $(SRC_DIR)/AutonomousTimeElapsedCondition.cpp
SOURCES += $(SRC_DIR)/AutonomousXmlProgram.cpp

################################################################################
# Object Files to Create
# We build the list of object files by translating the local source files.  This
# is done in 2 steps
# Xyz.cpp        --> ../bin/Xyz.cpp
# ../bin/Xyz.cpp --> ../bin/Xyz.o
################################################################################
ARM_OBJECTS = $(patsubst %.cpp,%.o, $(subst $(SRC_DIR)/,$(BIN_DIR)/arm/, $(SOURCES)))
X86_OBJECTS = $(patsubst %.cpp,%.o, $(subst $(SRC_DIR)/,$(BIN_DIR)/x86/, $(SOURCES)))


################################################################################
# Build Artifacts
################################################################################
ARM_STATIC_LIB = $(BIN_DIR)/arm/libautonomous.a
X86_STATIC_LIB = $(BIN_DIR)/x86/libautonomous.a


################################################################################
# Builds the library
################################################################################
.PHONY: all
all: $(ARM_STATIC_LIB) $(X86_STATIC_LIB)
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| ARM & X86 Libraries Done!"
	@echo "+---------------------------------------------------------------------"


################################################################################
# Builds the ARM library
################################################################################
.PHONY: arm
arm: $(ARM_STATIC_LIB)
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| ARM Library Done!"
	@echo "+---------------------------------------------------------------------"


################################################################################
# Builds the X86 library
################################################################################
.PHONY: x86
x86: $(X86_STATIC_LIB)
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| X86 Library Done!"
	@echo "+---------------------------------------------------------------------"


################################################################################
# Rule to Build the ARM Static Library
################################################################################
$(ARM_STATIC_LIB): $(SOURCES) $(ARM_OBJECTS)
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| Assembling $(ARM_STATIC_LIB)"
	@echo "+---------------------------------------------------------------------"
	$(ARM_AR) rcs $(ARM_STATIC_LIB) $(ARM_OBJECTS)
	@echo ""

################################################################################
# Rule to Build the X86 Static Library
################################################################################
$(X86_STATIC_LIB): $(SOURCES) $(X86_OBJECTS)
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| Assembling $(X86_STATIC_LIB)"
	@echo "+---------------------------------------------------------------------"
	$(X86_AR) rcs $(X86_STATIC_LIB) $(X86_OBJECTS)
	@echo ""


################################################################################
# Rule to Build Local ARM Source Code
################################################################################
$(BIN_DIR)/arm/%.o : $(SRC_DIR)/%.cpp
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| Compiling $< for ARM"
	@echo "+---------------------------------------------------------------------"
	$(ARM_CXX) -c $(CXXFLAGS) $< -o $@
	@echo ""


################################################################################
# Rule to Build Local X86 Source Code
################################################################################
$(BIN_DIR)/x86/%.o : $(SRC_DIR)/%.cpp
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| Compiling $< for X86"
	@echo "+---------------------------------------------------------------------"
	$(X86_CXX) -c $(CXXFLAGS) $(X86_CXXFLAGS) $< -o $@
	@echo ""


################################################################################
# Clean Rule
################################################################################
.PHONY: clean
clean:
	@echo ""
	@echo "+---------------------------------------------------------------------"
	@echo "| Cleaning..."
	@echo "+---------------------------------------------------------------------"
	-rm -f $(ARM_STATIC_LIB)
	-rm -f $(ARM_OBJECTS)
	-rm -f $(X86_STATIC_LIB)
	-rm -f $(X86_OBJECTS)


################################################################################
# END OF FILE
################################################################################

