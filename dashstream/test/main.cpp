#include "GlobalOiWpiStore.h"
#include "GlobalSiWpiStore.h"
#include "GlobalAiWpiStore.h"
#include "AutonomousController.h"
#include "CollectorController.h"
#include "DrivebaseController.h"
#include "ElevatorController.h"
#include "PusherController.h"
#include "GrabberController.h"
#include "AutonomousInterfaceContainer.h"
#include "CollectorInterfaceContainer.h"
#include "DrivebaseInterfaceContainer.h"
#include "ElevatorInterfaceContainer.h"
#include "PusherInterfaceContainer.h"
#include "GrabberInterfaceContainer.h"

#include "DashstreamController.h"
#include <unistd.h>
#include <iostream>

#include "DashstreamAutonomousInterface.h"

#define TICKRATE_MS 250
#define TICKS_PER_SEC 4

using namespace Psb::Subsystem;

// prototypes
void simulateElevatorData(void);
void simulatePusherData(void);
void simulateAutonomousData(Autonomous::Controller &con);
void simulateDrivebaseData(void);
void simulateCollectorData(void);
void simulateGrabberData(void);

int main(void)
{

   Collector::Controller  collector_controller;
   Drivebase::Controller  drivebase_controller;
   Elevator::Controller   elevator_controller;
   Pusher::Controller     pusher_controller;
   Autonomous::Controller autonomous_controller;
   Dashstream::Controller dashstream_controller(drivebase_controller,
                                                collector_controller,
                                                elevator_controller,
                                                pusher_controller,
                                                autonomous_controller);



   while (true)
   {
      dashstream_controller.update();
      usleep(TICKRATE_MS*1000);
      std::cerr << ".";

      simulateCollectorData();
      simulateDrivebaseData();
      simulateElevatorData();
      simulatePusherData();
      simulateGrabberData();
      simulateAutonomousData(autonomous_controller);

   }
   return 0;
}

///////////////////////////////////////////////////////////////////////////////
void simulateDrivebaseData(void)
{
   // DRIVEBASE
   Drivebase::Operator_I &dboi = Drivebase::InterfaceContainer::instance()->getOperatorInterface();
   Drivebase::Actuator_I &dbai = Drivebase::InterfaceContainer::instance()->getActuatorInterface();
   Drivebase::Sensor_I &dbsi = Drivebase::InterfaceContainer::instance()->getSensorInterface();

   static double db_lms = 0.0;
   static double db_rms = 0.0;
   static double db_lms_mult = -1.0;
   static double db_rms_mult = 1.0;
   static int32_t db_encmax = 1000;
   static int32_t db_ret = 0;
   static int32_t db_let = db_encmax;
   static int32_t db_ret_dir = 1;
   static int32_t db_let_dir = -1;

   dboi.setTurboButtonState(dboi.getTurboButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   dboi.setAntiTurboButtonState(dboi.getAntiTurboButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   dboi.setForwardEasyButtonState(dboi.getForwardEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   dboi.setReverseEasyButtonState(dboi.getReverseEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   dboi.setClockwiseSpinEasyButtonState(dboi.getClockwiseSpinEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   dboi.setCounterClockwiseSpinEasyButtonState(dboi.getCounterClockwiseSpinEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   dboi.setCancelButtonState(dboi.getCancelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);

   db_lms += db_lms_mult*0.1;
   db_rms += db_rms_mult*0.1;
   if (db_lms >= 1.0 || db_lms <= -1.0) { db_lms = db_lms_mult; db_lms_mult = db_lms_mult * -1; }
   if (db_rms >= 1.0 || db_rms <= -1.0) { db_rms = db_rms_mult; db_rms_mult = db_rms_mult * -1; }
   dbai.setLeftMotorSpeed(db_lms);
   dbai.setRightMotorSpeed(db_rms);

   db_ret += db_ret_dir;
   db_let += db_ret_dir;
   if (db_ret >= db_encmax || db_ret <= 0) { db_ret_dir = db_ret_dir * -1; }
   if (db_let >= db_encmax || db_let <= 0) { db_let_dir = db_let_dir * -1; }
   dbsi.setRightEncoderTicks(db_ret);
   dbsi.setLeftEncoderTicks(db_let);

   static double db_throttle = 0.8;
   static double db_heading = -0.3;
   static double db_throttle_mult = -1.0;
   static double db_heading_mult = 1.0;

   db_throttle += db_throttle_mult*0.1;
   db_heading += db_heading_mult*0.1;
   if (db_throttle >= 1.0 || db_throttle <= -1.0) { db_throttle = db_throttle_mult; db_throttle_mult = db_throttle_mult * -1; }
   if (db_heading >= 1.0 || db_heading <= -1.0) { db_heading = db_heading_mult; db_heading_mult = db_heading_mult * -1; }
   dboi.setThrottle(db_throttle);
   dboi.setHeading(db_heading);

   static double db_gyro = 1.8;
   static double db_gyro_mult = -1.2;

   db_gyro += db_gyro_mult*2.1;
   if (db_gyro <= 0 || db_gyro >= 360) { db_gyro = db_gyro_mult; db_gyro_mult = db_gyro_mult * -1; }
   dbsi.setHeadingAngleInDegrees(db_gyro);
}

///////////////////////////////////////////////////////////////////////////////
void simulateElevatorData(void)
{
   // Elevator
   Elevator::Operator_I &oi = Elevator::InterfaceContainer::instance()->getOperatorInterface();
   Elevator::Actuator_I &ai = Elevator::InterfaceContainer::instance()->getActuatorInterface();
   Elevator::Sensor_I &si = Elevator::InterfaceContainer::instance()->getSensorInterface();

   static int32_t ele_encmax = 1000;
   static int32_t ele_het = ele_encmax;
   static int32_t ele_het_dir = -1;
   static float ele_ms = 0.0;
   static float ele_ms_mult = 1.0;

   oi.setEnabledState(oi.getEnabledState() == ENABLED?DISABLED:ENABLED);
   oi.setManualOverrideEnabledState(oi.getManualOverrideEnabledState() == ENABLED?DISABLED:ENABLED);
   oi.setUpButtonState(oi.getUpButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setDownButtonState(oi.getDownButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setIndexEasyButtonState(oi.getIndexEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setGotoEasyButtonState(oi.getGotoEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setNestEasyButtonState(oi.getNestEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setCancelButtonState(oi.getCancelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);

   si.setMaxHeightLimitSwitchOneState(si.getMaxHeightLimitSwitchOneState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);
   si.setMaxHeightLimitSwitchTwoState(si.getMaxHeightLimitSwitchTwoState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);
   si.setMinHeightLimitSwitchState(si.getMinHeightLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);

   ele_het += ele_het_dir;
   if (ele_het >= ele_encmax || ele_het <= 0) { ele_het_dir = ele_het_dir * -1; }
   si.setHeightEncoderTicks(ele_het);

   ele_ms += ele_ms_mult*0.1;
   if (ele_ms >= 1.0 || ele_ms <= -1.0) { ele_ms = ele_ms_mult; ele_ms_mult = ele_ms_mult * -1; }
   ai.setMotorSpeed(ele_ms);

   static float ele_psv = 0.0;
   static float ele_psv_mult = 0.2;
   ele_psv += ele_psv_mult*0.1;
   if (ele_psv >= 1.0 || ele_psv <= -1.0) { ele_psv = ele_psv_mult; ele_psv_mult = ele_psv_mult * -1; }
   si.setProxSensorVoltage(ele_psv);

   si.setToteCaptureState(si.getToteCaptureState()==Elevator::TOTE_CAPTURED?
                               Elevator::TOTE_RELEASED:
                               Elevator::TOTE_CAPTURED);

   si.setElevatorIsOnTarget(si.getElevatorIsOnTarget()==true?  false: true);

   static int32_t ele_cp = 0;
   static int32_t ele_cp_movedir = 1;
   static int32_t ele_sp = 7;
   static int32_t ele_sp_movedir = -1;
   ele_cp += ele_cp_movedir;
   ele_sp += ele_sp_movedir;
   if (ele_sp < 1 || ele_sp > 6) { ele_sp_movedir = ele_sp_movedir * -1; }
   if (ele_cp < 1 || ele_cp > 6) { ele_cp_movedir = ele_cp_movedir * -1; }
   oi.setRequestedPositionNumber(ele_sp);
   si.setCurrentElevatorPosition(ele_cp);
}

///////////////////////////////////////////////////////////////////////////////
void simulateCollectorData(void)
{
   // Collector
   Collector::Operator_I &oi = Collector::InterfaceContainer::instance()->getOperatorInterface();
   Collector::Actuator_I &ai = Collector::InterfaceContainer::instance()->getActuatorInterface();
   Collector::Sensor_I &si = Collector::InterfaceContainer::instance()->getSensorInterface();

   // SI Limit switch states
   si.setLeftMaxInPivotLimitSwitchState(si.getLeftMaxInPivotLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);
   si.setRightMaxInPivotLimitSwitchState(si.getRightMaxInPivotLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);
   si.setLeftMaxOutPivotLimitSwitchState(si.getLeftMaxOutPivotLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);
   si.setRightMaxOutPivotLimitSwitchState(si.getRightMaxOutPivotLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);

   // SI arm angles
   static float coll_silaad = 0.0;
   static float coll_siraad = 0.0;
   static float coll_silaad_mult = -1.0;
   static float coll_siraad_mult = 1.0;
   coll_silaad += coll_silaad_mult*15.15;
   coll_siraad += coll_siraad_mult*18.18;
   if (coll_silaad <= 1.0 || coll_silaad >= 180.0) { coll_silaad = coll_silaad_mult; coll_silaad_mult = coll_silaad_mult * -1; }
   if (coll_siraad <= 1.0 || coll_siraad >= 180.0) { coll_siraad = coll_siraad_mult; coll_siraad_mult = coll_siraad_mult * -1; }
   si.setLeftArmPivotPotentiometerVoltage(coll_silaad);
   si.setRightArmPivotPotentiometerVoltage(coll_siraad);

   // OI buttons
   oi.setLeftFeederOutButtonState(oi.getLeftFeederOutButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setRightFeederOutButtonState(oi.getRightFeederOutButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setLeftFeederInButtonState(oi.getLeftFeederInButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setRightFeederInButtonState(oi.getRightFeederInButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setLeftFeederExpelButtonState(oi.getLeftFeederExpelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setRightFeederExpelButtonState(oi.getRightFeederExpelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setLeftFeederImpelButtonState(oi.getLeftFeederImpelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setRightFeederImpelButtonState(oi.getRightFeederImpelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setLeftTriggerButtonState(oi.getLeftTriggerButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setRightTriggerButtonState(oi.getRightTriggerButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setPlowLeftEasyButtonState(oi.getPlowLeftEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setPlowRightEasyButtonState(oi.getPlowRightEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setBumpEasyButtonState(oi.getBumpEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setHomeEasyButtonState(oi.getHomeEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setSafeEasyButtonState(oi.getSafeEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setCancelButtonState(oi.getCancelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);

   // OI arm angles
   static float coll_lappv = 0.0;
   static float coll_rappv = 0.0;
   static float coll_lappv_mult = -1.0;
   static float coll_rappv_mult = 1.0;
   coll_lappv += coll_lappv_mult*5.05;
   coll_rappv += coll_rappv_mult*8.08;
   if (coll_lappv <= 1.0 || coll_lappv >= 180.0) { coll_lappv = coll_lappv_mult; coll_lappv_mult = coll_lappv_mult * -1; }
   if (coll_rappv <= 1.0 || coll_rappv >= 180.0) { coll_rappv = coll_rappv_mult; coll_rappv_mult = coll_rappv_mult * -1; }
   //oi.setLeftArmPivotPotentiometerVoltage(coll_lappv);
   //oi.setRightArmPivotPotentiometerVoltage(coll_rappv);

   // AI Feeder Motor Speed
   static float coll_lfms = 0.0;
   static float coll_rfms = 0.0;
   static float coll_lfms_mult = -1.0;
   static float coll_rfms_mult = 1.0;
   coll_lfms += coll_lfms_mult*0.05;
   coll_rfms += coll_rfms_mult*0.08;
   if (coll_lfms >= 1.0 || coll_lfms <= -1.0) { coll_lfms = coll_lfms_mult; coll_lfms_mult = coll_lfms_mult * -1; }
   if (coll_rfms >= 1.0 || coll_rfms <= -1.0) { coll_rfms = coll_rfms_mult; coll_rfms_mult = coll_rfms_mult * -1; }
   ai.setLeftFeederMotorSpeed(coll_lfms);
   ai.setRightFeederMotorSpeed(coll_rfms);

   // AI Pivot Motor Speed
   static float coll_lpms = 0.0;
   static float coll_rpms = 0.0;
   static float coll_lpms_mult = -1.0;
   static float coll_rpms_mult = 1.0;
   coll_lpms += coll_lpms_mult*0.02;
   coll_rpms += coll_rpms_mult*0.005;
   if (coll_lpms >= 1.0 || coll_lpms <= -1.0) { coll_lpms = coll_lpms_mult; coll_lpms_mult = coll_lpms_mult * -1; }
   if (coll_rpms >= 1.0 || coll_rpms <= -1.0) { coll_rpms = coll_rpms_mult; coll_rpms_mult = coll_rpms_mult * -1; }
   ai.setLeftPivotMotorSpeed(coll_lpms);
   ai.setRightPivotMotorSpeed(coll_rpms);
}

///////////////////////////////////////////////////////////////////////////////
void simulatePusherData(void)
{
   // Pusher
   Pusher::Operator_I &oi = Pusher::InterfaceContainer::instance()->getOperatorInterface();
   Pusher::Actuator_I &ai = Pusher::InterfaceContainer::instance()->getActuatorInterface();
   Pusher::Sensor_I &si = Pusher::InterfaceContainer::instance()->getSensorInterface();

   static double p_ms = 0.0;
   static double p_ms_mult = 1.0;

   oi.setInButtonState(oi.getInButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setOutButtonState(oi.getOutButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setInEasyButtonState(oi.getInEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setOutEasyButtonState(oi.getOutEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setCycleEasyButtonState(oi.getCycleEasyButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setCancelButtonState(oi.getCancelButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);

   si.setOutwardLimitSwitchState(si.getOutwardLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);
   si.setInwardLimitSwitchState(si.getInwardLimitSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);

   p_ms += p_ms_mult*0.1;
   if (p_ms >= 1.0 || p_ms <= -1.0) { p_ms = p_ms_mult; p_ms_mult = p_ms_mult * -1; }
   ai.setMotorSpeed(p_ms);
}

///////////////////////////////////////////////////////////////////////////////
void simulateGrabberData(void)
{
   // Grabber
   Grabber::Operator_I &oi = Grabber::InterfaceContainer::instance()->getOperatorInterface();
   Grabber::Actuator_I &ai = Grabber::InterfaceContainer::instance()->getActuatorInterface();

   static double p_servo = 0.0;
   static double p_servo_mult = 1.0;

   oi.setRetractButtonState(oi.getRetractButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setDeployButtonState(oi.getDeployButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);

   p_servo += p_servo_mult*0.1;
   if (p_servo >= 1.0 || p_servo <= -1.0) { p_servo = p_servo_mult; p_servo_mult = p_servo_mult * -1; }
   ai.setServoValue(p_servo);
}

///////////////////////////////////////////////////////////////////////////////
void simulateAutonomousData(Autonomous::Controller &con)
{
   //Autonomous 
   Autonomous::Operator_I &oi = Autonomous::InterfaceContainer::instance()->getOperatorInterface();

   // ///////////////////////////////////////
   // do this to get the program names to cycle
   // save button states
   button_state_t prevbs = oi.getPrevProgramButtonState();
   button_state_t nextbs = oi.getNextProgramButtonState();
   switch_state_t liss = oi.getLockInSwitchState();
   frc_match_state_t lastms = oi.getFrcMatchState();

      // force auto program name change
      oi.setFrcMatchState(MATCH_STATE_DISABLED);
      oi.setPrevProgramButtonState(BUTTON_PRESSED);
      oi.setNextProgramButtonState(BUTTON_RELEASED);
      con.update();
      oi.setLockInSwitchState(SWITCH_CLOSED);
      oi.setPrevProgramButtonState(BUTTON_RELEASED);
      oi.setNextProgramButtonState(BUTTON_RELEASED);
      con.update();
      // printf("Selected name: %s\n", autonomous_controller.getSelectedProgramName().c_str());
      // printf("Locked name: %s\n", autonomous_controller.getLockedProgramName().c_str());

   // restore button states
   oi.setFrcMatchState(lastms);
   oi.setPrevProgramButtonState(prevbs);
   oi.setNextProgramButtonState(nextbs);
   oi.setLockInSwitchState(liss);
   // done getting program names to cycle
   // ///////////////////////////////////////

   oi.setPrevProgramButtonState(oi.getPrevProgramButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setNextProgramButtonState(oi.getNextProgramButtonState() == BUTTON_PRESSED?BUTTON_RELEASED:BUTTON_PRESSED);
   oi.setLockInSwitchState(oi.getLockInSwitchState() == SWITCH_CLOSED?SWITCH_OPEN:SWITCH_CLOSED);

   static int ms = 0;
   static int state_delays[NUM_MATCH_STATES] = {1*TICKS_PER_SEC, // 1 sec disabled
                                                15*TICKS_PER_SEC, // 15 sec auto
                                                135*TICKS_PER_SEC, // 135 sec tele
                                                2*TICKS_PER_SEC}; // 2 sec END
   static int delay = state_delays[ms];
   oi.setFrcMatchState((frc_match_state_t)ms);
   if (delay <= 0)
   {
      ms++;
      if (ms == NUM_MATCH_STATES)
      {
         ms = 0;
      }
      delay = state_delays[ms];
   }
   // printf("Delay: %d    state: %d\n",delay,ms);
   delay--;
}

