// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __DashstreamBaseInterface_I_h__
#define __DashstreamBaseInterface_I_h__

// /////////////////////////////////////////////////////////////////////////////
// Includes
// /////////////////////////////////////////////////////////////////////////////
#include "PsbMacros.h"
#include <unistd.h>
#include <stdint.h>

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Dashstream {

////////////////////////////////////////////////////////////////////////////////
/// @todo
/// DOCUMENTATION!!
////////////////////////////////////////////////////////////////////////////////
class BaseInterface_I
{
   public:
      virtual char const *getDescription(void) const = 0;
      virtual void *getData(void) = 0;
      virtual size_t dataSize(void) const = 0;
      virtual uint8_t SID(void) const = 0;
      virtual uint8_t DIV(void) const = 0;

   protected:
      BaseInterface_I(void);
      virtual ~BaseInterface_I(void);

   private:
      DISALLOW_COPY_CTOR(BaseInterface_I);
      DISALLOW_ASSIGNMENT_OPER(BaseInterface_I);
};


} // END namespace Dashstream
} // END namespace Subsystem
} // END namespace Psb

#endif // __DashstreamBaseInterface_I_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

