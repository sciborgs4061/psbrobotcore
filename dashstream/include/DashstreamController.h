// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __DashstreamController_h__
#define __DashstreamController_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "Controller_I.h"
#include "DashstreamConfigurationInterface.h"
#include "DashstreamBaseInterface_I.h"
#include <vector>
#include <list>

// /////////////////////////////////////////////////////////////////////////////
// Forward Declaration(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb { namespace Subsystem { namespace Dashstream { class DataProcessor; }}}
// namespace Psb { namespace Subsystem { namespace Autonomous { class Controller; }}}
namespace Psb { class Timer; }

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Dashstream {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The Controller pulls data from all robot subsystems, packages them and sends
/// to dashboard for display
////////////////////////////////////////////////////////////////////////////////
class Controller : public Psb::Subsystem::Controller_I
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      ///
      /// @todo DOCUMENT PARAMS!
      //////////////////////////////////////////////////////////////////////////
      Controller(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~Controller(void);

      /////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Controller_I::update()
      /////////////////////////////////////////////////////////////////////////
      void update(void);

      /////////////////////////////////////////////////////////////////////////
      /// @see
      /// Psb::Subsystem::Controller_I::update()
      /////////////////////////////////////////////////////////////////////////
      void init(void);

      /////////////////////////////////////////////////////////////////////////
      void addSubsystem(BaseInterface_I *dbi);

      /////////////////////////////////////////////////////////////////////////
      void listen_for_new_connection(void);

      typedef std::vector<BaseInterface_I *> dataInterfaceList_t;

   private:
      // these are no-ops in the dashstream...
      virtual void performManualUpdate(void) {}
      virtual void performAutoUpdate(void) {}
      virtual void performDisabledUpdate(void) {}
      virtual void transitionToState(subsystem_state_t /*new_state*/) {}
      virtual void setEnabledState(enabled_state_t /*new_state*/) {}

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Dashstream's Configuration Interface
      //////////////////////////////////////////////////////////////////////////
      ConfigurationInterface m_configurationIntf;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Dashstream's DataProcessor
      //////////////////////////////////////////////////////////////////////////
      DataProcessor* mp_dataProcessor;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Our pacing timer so we can control how often we send updates on the
      /// wire.
      //////////////////////////////////////////////////////////////////////////
      Psb::Timer* mp_pacingTimer;

      dataInterfaceList_t m_dataInterfaces;

      typedef std::list<int> socketList_t;
      socketList_t m_sockets;

   private:
      DISALLOW_COPY_CTOR(Controller);
      DISALLOW_ASSIGNMENT_OPER(Controller);

}; // END class Controller

} // END namespace Dashstream
} // END namespace Subsytem
} // END namespace Psb


#endif // __DashstreamController_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

