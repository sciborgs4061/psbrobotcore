// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stddef.h>
#include <PsbTimer.h>
#include "DashstreamController.h"
#include "DashstreamDataProcessor.h"
#include <pthread.h>
#include <strings.h>
#include <signal.h>

// #include <iostream>
// using namespace std;

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Dashstream {

// some statics
static pthread_mutex_t sg_socket_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t sg_listenthread;

// where to listen for the incomming connection...
#define LISTEN_PORTNUM 5555
#define JSON_DELIMITER "\n\n====================\n\n"

///////////////////////////////////////////////////////////////////////////////
static void *listener_thread(void *controller)
{
   // this call will never return...maybe we should fix that someday
   ((Controller *)controller)->listen_for_new_connection();
   return NULL;
}

///////////////////////////////////////////////////////////////////////////////
static void error(const char *msg)
{
   printf("ERROR: ");
   perror(msg);
   exit(1);
}

////////////////////////////////////////////////////////////////////////////////
Controller::Controller(void)
   : Psb::Subsystem::Controller_I()
   , m_configurationIntf()
   , mp_dataProcessor(NULL)
   , mp_pacingTimer(NULL)
   , m_dataInterfaces()
   , m_sockets()
{
   // Load XML settings from disk
   m_configurationIntf.load();

   // And our timer used to pace data at the configured rate
   mp_pacingTimer = new Psb::Timer();
   float wait_time_in_seconds = 
      static_cast<float>(m_configurationIntf.getUpdatePeriodInMs()) / 1000.0;
   mp_pacingTimer->setExpiration(wait_time_in_seconds);
   mp_pacingTimer->start();

   // ignore SIGPIPE so we don't fail on a socket write
   signal(SIGPIPE,SIG_IGN);

   // start the listener thread to listen for dashboard connections
   int rc;
   rc = pthread_create(&sg_listenthread, NULL, listener_thread, this);
   if (rc){
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      exit(-1);
   }
}


////////////////////////////////////////////////////////////////////////////////
Controller::~Controller(void)
{
   delete mp_dataProcessor;
   mp_dataProcessor = NULL;

   delete mp_pacingTimer;
   mp_pacingTimer = NULL;
}

////////////////////////////////////////////////////////////////////////////////
void 
Controller::init(void)
{
}

////////////////////////////////////////////////////////////////////////////////
void Controller::addSubsystem(BaseInterface_I *dbi)
{
   m_dataInterfaces.push_back(dbi);
}

////////////////////////////////////////////////////////////////////////////////
void 
Controller::update(void)
{
   if (NULL == mp_dataProcessor)
   {
      // Create the workhorse
      mp_dataProcessor = new DataProcessor(m_dataInterfaces);
   }

   // Normal processing, send updates per our configured refresh rate
   if (true == mp_pacingTimer->isExpired())
   {
      // Time to send updates!  Make sure we can next time too...
      mp_pacingTimer->reset();

      // First go collect all the data from all the subsystems
      mp_dataProcessor->collectAllData();

      // Now we package all that data up into a packet to send on the wire
      mp_dataProcessor->packageAllData();

      // Just send already!
      pthread_mutex_lock(&sg_socket_mutex);
      for (auto sock : m_sockets)
      {
         if (!mp_dataProcessor->sendPacket(sock))
         {
            // failed to send...nuke this socket
            close(sock);
            m_sockets.remove(sock);
            // bail for now since we modified the list
            break;
         }
      }
      pthread_mutex_unlock(&sg_socket_mutex);
   }
   else
   {
      // Too soon...do nothing...
   }

}


///////////////////////////////////////////////////////////////////////////////
void Controller::listen_for_new_connection(void)
{
   int sockfd;
   int newsockfd;
   socklen_t clilen;
   struct sockaddr_in serv_addr;
   struct sockaddr_in cli_addr;
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   if (sockfd < 0) 
   {
      error("ERROR opening socket");
   }

   int enable = 1;
   if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
   {
      error("setsockopt(SO_REUSEADDR) failed");
   }

   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(LISTEN_PORTNUM);
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
   {
      error("ERROR on binding");
   }

   while (true)
   {
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
     if (newsockfd < 0) 
     {
        error("ERROR on accept");
     }

     pthread_mutex_lock(&sg_socket_mutex);

     // need to write all the JSON descriptions out to the new connection
     std::string delim = "";
     std::string desc;
     desc += "{\"subsystems\":[";
     for (auto interface: m_dataInterfaces)
     {
        desc += delim;
        desc += interface->getDescription();
        delim = ",";
     }
     desc += "]}";
     desc += JSON_DELIMITER;

     int send_status = write(newsockfd, desc.c_str(), desc.size());
     if (-1 == send_status)
     {
         error("failed to send JSON description:");
     }

     // keep track of this socket so we can send data out of it
     m_sockets.push_back(newsockfd);

     pthread_mutex_unlock(&sg_socket_mutex);
   }

}

} // END namespace Dashstream
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

