// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbXmlConfigurationFile.h>
#include "DashstreamConfigurationInterface.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Dashstream {


// /////////////////////////////////////////////////////////////////////////////
// Static Data
// /////////////////////////////////////////////////////////////////////////////
#ifndef PSB_UNIT_TEST
// Production Code Version!
static const std::string s_XmlFileName =
   "/home/lvuser/psb_settings/dashstream_settings.xml";
#else
// Unit test version!
static const std::string s_XmlFileName =
   "/tmp/ut_dashstream_settings.xml";
#endif


////////////////////////////////////////////////////////////////////////////////
ConfigurationInterface::ConfigurationInterface(void)
   : Psb::Subsystem::Dashstream::Configuration_I()
   , mp_configFile(NULL)
   , m_dstAddressString("10.40.61.224")
   , m_updatePeriodMsec(100)
{
   mp_configFile =
      new Psb::XmlConfigurationFile(s_XmlFileName.c_str());
}


////////////////////////////////////////////////////////////////////////////////
ConfigurationInterface::~ConfigurationInterface(void)
{
   delete mp_configFile;
   mp_configFile = NULL;
}


////////////////////////////////////////////////////////////////////////////////
void 
ConfigurationInterface::load(void)
{
   m_dstAddressString =
      mp_configFile->getValueAsString("DestinationIpAddress", "10.40.61.224");
   m_updatePeriodMsec =
      mp_configFile->getValueAsInt("UpdatePeriodMsec",    100);
   return;
}


////////////////////////////////////////////////////////////////////////////////
std::string
ConfigurationInterface::getDestinationIPAddress(void) const
{
   return m_dstAddressString;
}


////////////////////////////////////////////////////////////////////////////////
int
ConfigurationInterface::getUpdatePeriodInMs(void) const
{
   return m_updatePeriodMsec;
}


} // END namespace Dashstream
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

