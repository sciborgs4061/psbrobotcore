// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstddef>
#include "DashstreamInterfaceContainer.h"


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (DASHSTREAM + 100u)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {
namespace Dashstream {


// /////////////////////////////////////////////////////////////////////////////
// Static Data
// /////////////////////////////////////////////////////////////////////////////
InterfaceContainer* InterfaceContainer::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
InterfaceContainer::InterfaceContainer(void)
   : m_configurationIntf()
{
   // Load the settings from disk
   m_configurationIntf.load();
}


////////////////////////////////////////////////////////////////////////////////
InterfaceContainer::~InterfaceContainer(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
InterfaceContainer*
InterfaceContainer::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new InterfaceContainer();
   }

   return msp_instance;
}


////////////////////////////////////////////////////////////////////////////////
Configuration_I&
InterfaceContainer::getConfigurationInterface(void) 
{
   return m_configurationIntf;
}


} // END namespace Dashstream
} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

