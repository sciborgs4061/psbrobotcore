// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <PsbLogger.h>
#include <PsbLogDataCollector.h>
#include "DashstreamConfigurationInterface.h"


// /////////////////////////////////////////////////////////////////////////////
// Test Fixture
// /////////////////////////////////////////////////////////////////////////////
class DashstreamConfigurationInterfaceTest : public ::testing::Test
{
   protected:
      // Per-test-case set-up.
      // Called before the first test in this test case.
      // Can be omitted if not needed.
      static void SetUpTestCase(void);

      // Per-test-case tear-down.
      // Called after the last test in this test case.
      // Can be omitted if not needed.
      static void TearDownTestCase(void);

      // You can define per-test set-up and tear-down logic as usual.
      virtual void SetUp(void);
      virtual void TearDown(void);

      static Psb::LogDataCollector* mp_collector;
};


// /////////////////////////////////////////////////////////////////////////////
// Static Data
// /////////////////////////////////////////////////////////////////////////////
Psb::LogDataCollector* DashstreamConfigurationInterfaceTest::mp_collector = NULL;


////////////////////////////////////////////////////////////////////////////////
void
DashstreamConfigurationInterfaceTest::SetUpTestCase(void)
{
   mp_collector = 
      reinterpret_cast<Psb::LogDataCollector*>(Psb::Logger::instance()->getDataSender());
}


////////////////////////////////////////////////////////////////////////////////
void
DashstreamConfigurationInterfaceTest::TearDownTestCase(void)
{
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
DashstreamConfigurationInterfaceTest::SetUp(void)
{
   mp_collector->printAllLogs();
   EXPECT_TRUE(mp_collector->verifyLogCountForLevel(Psb::LOG_LEVEL_ERROR, 0, true));
   mp_collector->clearAllLogs();
}


////////////////////////////////////////////////////////////////////////////////
void
DashstreamConfigurationInterfaceTest::TearDown(void)
{
   mp_collector->printAllLogs();
   EXPECT_TRUE(mp_collector->verifyLogCountForLevel(Psb::LOG_LEVEL_ERROR, 0, true));
}



////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test confirms values that the ConfigurationInterface will produce just
/// after creation.
////////////////////////////////////////////////////////////////////////////////
TEST_F(DashstreamConfigurationInterfaceTest, checkBasicConstruction)
{
   // Create the config interface
   Psb::Subsystem::Dashstream::ConfigurationInterface ci;

   // Extract and confirm start-up defaults for simple settings
   EXPECT_EQ(std::string("10.40.61.224"), ci.getDestinationIPAddress());
   EXPECT_EQ(100,                         ci.getUpdatePeriodInMs());
   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test confirms values that the ConfigurationInterface will produce when
/// it loads values from a sane XML file.
////////////////////////////////////////////////////////////////////////////////
TEST_F(DashstreamConfigurationInterfaceTest, checkValuesAfterLoad)
{
   // Copy our canned data to the unit-test XML file
   int rc = system("cp "
                   "../data/checkValuesAfterLoad_settings.xml "
                   "/tmp/ut_dashstream_settings.xml");
   ASSERT_NE(-1, rc);
   
   // Create the config interface
   Psb::Subsystem::Dashstream::ConfigurationInterface ci;
   ci.load();

   // Extract and confirm start-up defaults for simple settings
   EXPECT_EQ(std::string("10.40.61.106"), ci.getDestinationIPAddress());
   EXPECT_EQ(50,                          ci.getUpdatePeriodInMs());
   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

