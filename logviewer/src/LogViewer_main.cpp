// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <cstddef>
#include <cerrno>
#include <cassert>
#include <cstring>
#include <string>
#include <cstdio>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include "PsbLogDataBundle.h"


////////////////////////////////////////////////////////////////////////////////
int
main(int /*argc*/, char** /*argv*/)
{
   // Open a socket listening on the Logger port
   int socket_fd = ::socket(AF_INET,
                            SOCK_DGRAM,
                            IPPROTO_UDP);
   assert(-1 != socket_fd);

   // This socket should not be able to transmit data
   ::shutdown(socket_fd, SHUT_WR);

   // Set up our receive address
   struct sockaddr_in recv_addr;
   recv_addr.sin_family      = AF_INET;
   recv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
   recv_addr.sin_port        = htons(17654);

   // And bind our socket to it
   int result = bind(socket_fd,
                     reinterpret_cast<struct sockaddr*>(&recv_addr),
                     sizeof(recv_addr));
   assert(result >= 0);
   printf("Listening on port %d\n", ntohs(recv_addr.sin_port));

   while (true)
   {
      // Declare a buffer big enough to hold the log data
      uint8_t raw_bytes[Psb::LogDataBundle::BUFFER_SIZE];

      // Do a blocking read on the socket
      socklen_t addr_len = sizeof(recv_addr);
      ssize_t num_rx_bytes
         = recvfrom(socket_fd,
                    raw_bytes,
                    sizeof(raw_bytes),
                    0,
                    reinterpret_cast<struct sockaddr*>(&recv_addr),
                    &addr_len);

      // Create a LogDataBundle out of the received bytes and print its contents
      Psb::LogDataBundle bundle(raw_bytes, num_rx_bytes);
      printf("%s\n", bundle.toString().c_str());
   }

   return 0;
}



// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

