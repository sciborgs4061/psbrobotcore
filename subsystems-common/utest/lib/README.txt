This file is a placeholder so that Mercurial does not delete the required lib/
directory.  The lib/ directory is required to house the temporary libraries we
build for gtest etc.
