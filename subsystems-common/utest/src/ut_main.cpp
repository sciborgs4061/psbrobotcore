#include <gtest/gtest.h>
#include "GlobalWpiStoreTestEnvironment.h"

int main(int argc, char** argv)
{
   ::testing::InitGoogleTest(&argc, argv);

   // Register global environments
   ::testing::AddGlobalTestEnvironment(new GlobalWpiStoreTestEnvironment());

   return RUN_ALL_TESTS();
}

