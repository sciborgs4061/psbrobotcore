// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include <list>
#include "PsbTimer.h"


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that we can create and destroy a Timer
////////////////////////////////////////////////////////////////////////////////
TEST(PsbTimerUT, checkBasicConstruction)
{
   // Create the object
   Psb::Timer tmr;

   // Accessors right after construction
   EXPECT_FALSE(tmr.isRunning());
   EXPECT_FALSE(tmr.isExpired());
   EXPECT_FLOAT_EQ(tmr.getElapsedTime(), 0.0);
   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that when the start() method is called, without first
/// having set a timer expiration value, the API properly updates.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbTimerUT, checkStartWithoutExpiration)
{
   // Create the object
   Psb::Timer tmr;

   // Start it running and verify
   tmr.start();
   EXPECT_TRUE(tmr.isRunning());
   EXPECT_FALSE(tmr.isExpired());  // No expiration set, QED never expired
   EXPECT_GT(tmr.getElapsedTime(), 0.0);
   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the Timer reasonably tracks the passage of time.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbTimerUT, checkBasicTimeTracking)
{
   // Create the object
   Psb::Timer tmr;

   // Start it running
   tmr.start();

   // Grab start/end snapshots
   for (int idx = 0; idx < 10; idx++)
   {
      double before = tmr.getElapsedTime();
      tmr.waitForTime(0.010);
      double after  = tmr.getElapsedTime();

      // Check the actual elapsed time... note that due to the machine on which
      // this unit test is running, we cannot reliably predict the exact amount
      // of time that will pass.  In the ideal world, we would stub out the
      // system calls that the Psb::Timer makes (clock_gettime() and select()).
      // Until that happens, we just ensure that our time tracking gives us at
      // least the value we are looking for.
      EXPECT_GT(after - before, 0.010);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the Timer correctly handles expiration.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbTimerUT, checkBasicExpiration)
{
   // Create the object
   Psb::Timer tmr;

   // Start it running
   tmr.setExpiration(0.050);  // 50 msec
   tmr.start();

   // Wait for 1/2 the time - no expiration
   tmr.waitForTime(0.025);
   EXPECT_FALSE(tmr.isExpired());

   // Wait for the other 1/2 the time - with expiration
   tmr.waitForTime(0.025);
   EXPECT_TRUE(tmr.isExpired());

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the Timer correctly resets after an expiration.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbTimerUT, checkBasicResetAfterExpiration)
{
   // Create the object
   Psb::Timer tmr;

   // Start it running
   tmr.setExpiration(0.050);  // 50 msec
   tmr.start();

   // Let the timer expire, make sure it does
   EXPECT_FALSE(tmr.isExpired());
   tmr.waitForTime(0.050);
   EXPECT_TRUE(tmr.isExpired());

   // Reset the timer, make sure it's not expired
   tmr.reset();
   EXPECT_FALSE(tmr.isExpired());
   tmr.waitForTime(0.025);
   EXPECT_FALSE(tmr.isExpired());
   tmr.waitForTime(0.025);
   EXPECT_TRUE(tmr.isExpired());

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that the Timer correctly resets before an expiration.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbTimerUT, checkBasicResetBeforeExpiration)
{
   // Create the object
   Psb::Timer tmr;

   // Start it running
   tmr.setExpiration(0.050);  // 50 msec
   tmr.start();

   // Let the timer expire, make sure it does
   EXPECT_FALSE(tmr.isExpired());
   tmr.waitForTime(0.025);
   EXPECT_FALSE(tmr.isExpired());

   // Reset the timer, make sure it's not expired
   tmr.reset();
   EXPECT_FALSE(tmr.isExpired());
   tmr.waitForTime(0.025);
   EXPECT_FALSE(tmr.isExpired());
   tmr.waitForTime(0.025);
   EXPECT_TRUE(tmr.isExpired());

   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

