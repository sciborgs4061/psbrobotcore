// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include "GlobalWpiStoreTestEnvironment.h"
#include "GlobalOiWpiStore.h"
#include "GlobalSiWpiStore.h"
#include "GlobalAiWpiStore.h"


////////////////////////////////////////////////////////////////////////////////
GlobalWpiStoreTestEnvironment::GlobalWpiStoreTestEnvironment(void)
   : ::testing::Environment()
{
   // Intentionally left empty - see SetUp()
}


////////////////////////////////////////////////////////////////////////////////
GlobalWpiStoreTestEnvironment::~GlobalWpiStoreTestEnvironment(void)
{
   // Intentionally left empty - see TearDown()
}


////////////////////////////////////////////////////////////////////////////////
void
GlobalWpiStoreTestEnvironment::SetUp(void)
{
   Psb::Subsystem::GlobalOiWpiStore* p_oi_store =
      Psb::Subsystem::GlobalOiWpiStore::instance();
   EXPECT_FALSE(NULL == p_oi_store);

   Psb::Subsystem::GlobalSiWpiStore* p_si_store =
      Psb::Subsystem::GlobalSiWpiStore::instance();
   EXPECT_FALSE(NULL == p_si_store);

   Psb::Subsystem::GlobalAiWpiStore* p_ai_store =
      Psb::Subsystem::GlobalAiWpiStore::instance();
   EXPECT_FALSE(NULL == p_ai_store);

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
GlobalWpiStoreTestEnvironment::TearDown(void)
{
   Psb::Subsystem::GlobalAiWpiStore* p_ai_store =
      Psb::Subsystem::GlobalAiWpiStore::instance();
   EXPECT_FALSE(NULL == p_ai_store);
   delete p_ai_store;

   Psb::Subsystem::GlobalSiWpiStore* p_si_store =
      Psb::Subsystem::GlobalSiWpiStore::instance();
   EXPECT_FALSE(NULL == p_si_store);
   delete p_si_store;

   Psb::Subsystem::GlobalOiWpiStore* p_oi_store =
      Psb::Subsystem::GlobalOiWpiStore::instance();
   EXPECT_FALSE(NULL == p_oi_store);
   delete p_oi_store;

   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

