// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2015 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// Project Home:
//   https://bitbucket.org/sciborgs4061/robot-2015/wiki/Home
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <gtest/gtest.h>
#include "PsbBumpController.h"


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that we can create and destroy a BumpController
////////////////////////////////////////////////////////////////////////////////
TEST(PsbBumpControllerUT, checkBasicConstruction)
{
   Psb::BumpController bumper;
   bumper.setConfiguration(500.0f,           // Setpoint
                           250.0f, 750.0f,   // Min/Max Inputs
                            -0.5f,   0.5f,   // Min/Max Bump Mode Outputs
                            -1.0f,   1.0f,   // Min/Max Fast Mode Outputs
                               1u,     4u,   // On/Off Loop Counts
                            50.0f,  20.0f);  // Epsilon/Tau

   // Accessors right after construction
   EXPECT_FALSE(bumper.isEnabled());
   EXPECT_FALSE(bumper.isOnTarget());

   return;
}


/// @todo Check all valid/invalid configurations
/// --> setpoint + epsilon > max input --> ERROR
/// --> setpoint - epsilon < min input --> ERROR
/// --> setpoint + tau > max input --> ERROR
/// --> setpoint - tau < min input --> ERROR


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that when the process variable is outside of the epsilon
/// region, the output of the controller is always at the "fast" mode level.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbBumpControllerUT, checkBasicFastModeMaxOutput)
{
   Psb::BumpController bumper;
   bumper.setConfiguration(500.0f,           // Setpoint
                           250.0f, 750.0f,   // Min/Max Inputs
                            -0.5f,   0.5f,   // Min/Max Bump Mode Outputs
                            -0.9f,   0.9f,   // Min/Max Fast Mode Outputs
                               1u,     4u,   // On/Off Loop Counts
                            50.0f,  20.0f);  // Epsilon/Tau

   // Enable the controller
   bumper.enable();
   EXPECT_TRUE(bumper.isEnabled());

   // Low-side Epsilon region is setpoint - epsilon --> 450.0-
   float process_var = 450.0f;
   while (process_var > 250.0f)
   {
      float output = bumper.runControlLoop(process_var);
      EXPECT_FLOAT_EQ(0.9f, output);

      process_var -= 1.0f;
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that when the process variable is outside of the epsilon
/// region, the output of the controller is always at the "fast" mode level.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbBumpControllerUT, checkBasicFastModeMinOutput)
{
   Psb::BumpController bumper;
   bumper.setConfiguration(500.0f,           // Setpoint
                           250.0f, 750.0f,   // Min/Max Inputs
                            -0.5f,   0.5f,   // Min/Max Bump Mode Outputs
                            -0.9f,   0.9f,   // Min/Max Fast Mode Outputs
                               1u,     4u,   // On/Off Loop Counts
                            50.0f,  20.0f);  // Epsilon/Tau

   // Enable the controller
   bumper.enable();
   EXPECT_TRUE(bumper.isEnabled());

   // High-side Epsilon region is setpoint + epsilon --> 550.0+
   float process_var = 550.0f;
   while (process_var < 750.0f)
   {
      float output = bumper.runControlLoop(process_var);
      EXPECT_FLOAT_EQ(-0.9f, output);

      process_var += 1.0f;
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that when the process variable is inside of the epsilon
/// region, but still outside of the tau region, then the output of the
/// controller will match the expected "bump" algorithm for low-side process
/// variable values.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbBumpControllerUT, checkBasicBumpModeMaxOutput)
{
   Psb::BumpController bumper;
   bumper.setConfiguration(500.0f,           // Setpoint
                           250.0f, 750.0f,   // Min/Max Inputs
                            -0.5f,   0.5f,   // Min/Max Bump Mode Outputs
                            -0.9f,   0.9f,   // Min/Max Fast Mode Outputs
                               1u,     4u,   // On/Off Loop Counts
                            50.0f,  20.0f);  // Epsilon/Tau

   // Enable the controller
   bumper.enable();
   EXPECT_TRUE(bumper.isEnabled());

   // Low-side Tau region is setpoint - tau --> (450.0, 480.0)
   float process_var = 470.0f;
   float output      = 0.0f;
   for (int idx = 0; idx < 20; idx ++)
   {
      output = bumper.runControlLoop(process_var);  // Bump on
      EXPECT_FLOAT_EQ(0.5f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This test verifies that when the process variable is inside of the epsilon
/// region, but still outside of the tau region, then the output of the
/// controller will match the expected "bump" algorithm, for high-side process
/// variable values.
////////////////////////////////////////////////////////////////////////////////
TEST(PsbBumpControllerUT, checkBasicBumpModeMinOutput)
{
   Psb::BumpController bumper;
   bumper.setConfiguration(500.0f,           // Setpoint
                           250.0f, 750.0f,   // Min/Max Inputs
                            -0.5f,   0.5f,   // Min/Max Bump Mode Outputs
                            -0.9f,   0.9f,   // Min/Max Fast Mode Outputs
                               1u,     4u,   // On/Off Loop Counts
                            50.0f,  20.0f);  // Epsilon/Tau

   // Enable the controller
   bumper.enable();
   EXPECT_TRUE(bumper.isEnabled());

   // Low-side Tau region is setpoint + tau --> (520.0, 550.0)
   float process_var = 530.0f;
   float output      = 0.0f;
   for (int idx = 0; idx < 20; idx ++)
   {
      output = bumper.runControlLoop(process_var);  // Bump on
      EXPECT_FLOAT_EQ(-0.5f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
      output = bumper.runControlLoop(process_var);  // Bump off
      EXPECT_FLOAT_EQ(0.0f, output);
   }

   return;
}


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

