////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "Controller_I.h"
#include "PsbLogger.h"

// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (SUBSYSTEM_COMMON | CONTROLLER)

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
Controller_I::Controller_I(void)
   : m_state(Psb::Subsystem::SUBSYSTEM_MANUAL)
{
   // Intentionally left empty
}

////////////////////////////////////////////////////////////////////////////////
Controller_I::~Controller_I(void)
{
   // Intentionally left empty
}

////////////////////////////////////////////////////////////////////////////////
void
Controller_I::update(void)
{
   // Take action based on current subsystem state
   switch(m_state)
   {
      case Psb::Subsystem::SUBSYSTEM_MANUAL:
      {
         performManualUpdate();
      }
      break;

      case Psb::Subsystem::SUBSYSTEM_AUTOMATIC:
      {
         performAutoUpdate();
      }
      break;

      case Psb::Subsystem::SUBSYSTEM_DISABLED:
      {
         performDisabledUpdate();
      }
      break;

      default:
      {
         // Invalid State!  This is a coding defect!
         PSB_LOG_ERROR(1, Psb::LOG_TYPE_INT32, m_state);
         transitionToState(Psb::Subsystem::SUBSYSTEM_DISABLED);
      }
      break;
   }

   return;
}


} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

