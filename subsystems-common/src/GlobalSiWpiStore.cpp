// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h>  // for NULL
#include "GlobalSiWpiStore.h"
#include "SiDataGrabbers.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalSiWpiStore* GlobalSiWpiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalSiWpiStore::GlobalSiWpiStore(void)
{
   m_encoder[ENCODER_ONE] = new Encoder(20,21,true);
   m_encoder[ENCODER_TWO] = new Encoder(22,23,false);

   m_digitalInput[LIMITSWITCH_ONE] = new DigitalInput(0);
   m_digitalInput[LIMITSWITCH_TWO] = new DigitalInput(1);
}


////////////////////////////////////////////////////////////////////////////////
GlobalSiWpiStore::~GlobalSiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_ENCODERS; x++)
   {
      delete m_encoder[x];
      m_encoder[x] = NULL;
   }

   for (x = 0; x < MAX_DIGITAL_INPUTS; x++)
   {
      delete m_digitalInput[x];
      m_digitalInput[x] = NULL;
   }

   // should walk all the data grabbers and clean then up
}


////////////////////////////////////////////////////////////////////////////////
GlobalSiWpiStore*
GlobalSiWpiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalSiWpiStore();
   }

   return msp_instance;
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(EncoderDataGrabber *edg, si_encoders_t idx)
{
   if (idx < MAX_ENCODERS)
   {
      edg->setEncoder(m_encoder[idx]);
      m_grabbers.push_back(edg);
   }
   else
   {
      // bad index...
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::attach(DigitalInputDataGrabber *didg, si_digitalinputs_t idx)
{
   if (idx < MAX_DIGITAL_INPUTS)
   {
      didg->setDigitalInput(m_digitalInput[idx]);
      m_grabbers.push_back(didg);
   }
   else
   {
      // bad index...
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalSiWpiStore::refresh(void)
{
   for (auto p_grabber : m_grabbers)
   {
      p_grabber->grabData();
   }
}

} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

