////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h> // for NULL
#include "GlobalOiWpiStore.h"
#include "OiDataGrabbers.h"

// ////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// ///////////////////////////////////////////////////////////////////////////// 
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalOiWpiStore* GlobalOiWpiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalOiWpiStore::GlobalOiWpiStore(void)
{
   m_joystick[0] = new Joystick(0);
}


////////////////////////////////////////////////////////////////////////////////
GlobalOiWpiStore::~GlobalOiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_JOYSTICKS; x++)
   {
      delete m_joystick[x];
      m_joystick[x] = NULL;
   }

   // should walk all the data grabbers and clean then up
}


////////////////////////////////////////////////////////////////////////////////
GlobalOiWpiStore*
GlobalOiWpiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalOiWpiStore();
   }

   return msp_instance;
}

////////////////////////////////////////////////////////////////////////////////
void GlobalOiWpiStore::attach(JoystickDataGrabber *jdg, oi_joystick_t idx)
{
   if (idx < MAX_JOYSTICKS)
   {
      jdg->setJoystick(m_joystick[idx]);
      m_grabbers.push_back(jdg);
   }
   else
   {
      // bad index...
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalOiWpiStore::refresh(void)
{
   for (auto p_grabber : m_grabbers)
   {
      p_grabber->grabData();
   }
}

////////////////////////////////////////////////////////////////////////////////
OiDataGrabber *GlobalOiWpiStore::getGrabberByName(std::string const &name)
{
   OiDataGrabber *found_grabber = NULL;
   for (auto p_grabber : m_grabbers)
   {
      if (p_grabber->getName() == name)
      {
         found_grabber = p_grabber;
         break;
      }
   }
   return found_grabber;
}

} // END namespace Subsystem
} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

