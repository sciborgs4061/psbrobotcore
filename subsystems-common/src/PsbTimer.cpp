// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <ctime>
#include <cstdio>
#include <sys/select.h>
#include "PsbTimer.h"


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This function converts the contents of a timespec structure to a double-
/// precision floating point number of seconds.
///
/// @remarks
/// This function is makred for inlining to increase execution speed.
////////////////////////////////////////////////////////////////////////////////
static inline double 
convertTimespecToSeconds(timespec* p_time)
{
   return 
      (static_cast<double>(p_time->tv_sec) +
      (static_cast<double>(p_time->tv_nsec) / static_cast<double>(1e9)));
}

   
////////////////////////////////////////////////////////////////////////////////
Timer::Timer(void)
   : m_isRunning(false)
   , m_startTime(0.0)
   , m_nowTime(0.0)
   , m_expiration(0.0)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
Timer::~Timer(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void 
Timer::start(void)
{
   // Timer has been started, grab the current time.
   timespec currentTime = {0, 0};
   clock_gettime(CLOCK_MONOTONIC, &currentTime);

   m_startTime = convertTimespecToSeconds(&currentTime);
   m_nowTime   = m_startTime;
   m_isRunning = true;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void 
Timer::stop(void)
{
   m_isRunning = false;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void 
Timer::reset(void)
{
   // Timer has been reset, so we need to grab the current time again
   timespec currentTime = {0, 0};
   clock_gettime(CLOCK_MONOTONIC, &currentTime);

   m_startTime = convertTimespecToSeconds(&currentTime);
   m_nowTime   = m_startTime;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
Timer::waitForTime(double delay) const
{
   if (delay > 0.0)
   {
      // Here' we will use select() to get better precision on delays
      timeval timeout;
      timeout.tv_sec  = static_cast<long>(delay);
      timeout.tv_usec = static_cast<long>((delay - static_cast<double>(timeout.tv_sec)) * 1e6);

      // Perform the wait
      int status = select(0, NULL, NULL, NULL, &timeout);
      if (0 != status)
      {
         // oops...
         printf("select() returned an unexpected value\n");
      }
   }
   else
   {
      // Invalid delay time
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void 
Timer::setExpiration(double expiration)
{
   m_expiration = expiration;
   return;
}


////////////////////////////////////////////////////////////////////////////////
bool 
Timer::isRunning(void) const
{
   return m_isRunning;
}


////////////////////////////////////////////////////////////////////////////////
bool 
Timer::isExpired(void) const
{
   bool is_expired = false;

   if (true == m_isRunning)
   {
      if (m_expiration > 0.0)
      {
         // Everything checks out, let's compute the elapsed time and check
         timespec currentTime = {0, 0};
         clock_gettime(CLOCK_MONOTONIC, &currentTime);
         m_nowTime = convertTimespecToSeconds(&currentTime);

         is_expired = 
            ((m_nowTime - m_startTime) > m_expiration)
            ? true     // Timer has expired
            : false;   // Timer has NOT yet expired
      }
      else
      {
         // Expiration value is invalid (or never set), therefore this timer
         // can never be expired.
         is_expired = false;
      }
   }
   else
   {
      // Timer is not running, therefore it cannot be expired
      is_expired = false;
   }

   return is_expired;
}


////////////////////////////////////////////////////////////////////////////////
double 
Timer::getElapsedTime(void) const
{
   double elapsed_time = 0.0;

   if (true == m_isRunning)
   {
      timespec currentTime = {0, 0};
      clock_gettime(CLOCK_MONOTONIC, &currentTime);
      m_nowTime = convertTimespecToSeconds(&currentTime);

      elapsed_time =
         (m_nowTime >= m_startTime)
         ? m_nowTime - m_startTime   // Delta time is positive
         : 0.0;                      // Floor of delta-time is 0.0
   }

   return elapsed_time;
}


} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

