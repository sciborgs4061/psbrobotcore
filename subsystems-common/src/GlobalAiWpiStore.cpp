// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string.h> // needed for NULL
#include "Talon.h"
#include "GlobalAiWpiStore.h"
#include "AiDataPushers.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// ////////////////////////////////////////////////////////////////////////////
// Static Data
// ///////////////////////////////////////////////////////////////////////////// 
GlobalAiWpiStore* GlobalAiWpiStore::msp_instance = NULL;


////////////////////////////////////////////////////////////////////////////////
GlobalAiWpiStore::GlobalAiWpiStore(void)
{
   m_speedController[0] = new Talon(1);
}


////////////////////////////////////////////////////////////////////////////////
GlobalAiWpiStore::~GlobalAiWpiStore(void)
{
   int x;
   for (x = 0; x < MAX_SPEED_CONTROLLER; x++)
   {
      delete m_speedController[x];
      m_speedController[x] = NULL;
   }
}


////////////////////////////////////////////////////////////////////////////////
GlobalAiWpiStore*
GlobalAiWpiStore::instance(void)
{
   if (NULL == msp_instance)
   {
      msp_instance = new GlobalAiWpiStore();
   }

   return msp_instance;
}


////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::attach(SpeedControllerDataPusher *scdp, 
                              ai_speedcontrollers_t idx)
{
   if (idx < MAX_SPEED_CONTROLLER)
   {
      scdp->setSpeedController(m_speedController[idx]);
      m_pushers.push_back(scdp);
   }
   else
   {
      // bad index...
   }
}

////////////////////////////////////////////////////////////////////////////////
void GlobalAiWpiStore::flush(void)
{
   for (auto p_pusher : m_pushers)
   {
      p_pusher->pushData();
   }
}

} // END namespace Subsystem
} // END namespace Psb

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////


