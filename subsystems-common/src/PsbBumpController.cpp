// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cmath>
#include <PsbLogger.h>
#include "PsbBumpController.h"


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define FILE_NUM (SUBSYSTEM_COMMON | 0x0100)


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
BumpController::BumpController()
   : m_setPoint(0)
   , m_minInput(0)
   , m_maxInput(0)
   , m_minBumpOutput(0)
   , m_maxBumpOutput(0)
   , m_minFastOutput(0)
   , m_maxFastOutput(0)
   , m_onLoopMaxCount(0)
   , m_offLoopMaxCount(0)
   , m_epsilon(0)
   , m_tau(0)
   , m_isConfigValid(false)
   , m_isEnabled(false)
   , m_isOnTarget(false)
   , m_processValue(0.0f)
   , m_errorTerm(0.0f)
   , m_outputValue(0.0f)
   , m_onLoopCounter(0)
   , m_offLoopCounter(0)
{
   // It seems that we cannot call class functions from the CTOR.  If we do this
   // we crash on target.  We need to update the CTOR to not take config params.
   // We should simply force the user to call the setConfiguration() method, 
   // which could call these functions as needed.

///x   // Validate the configuration
///x   m_isConfigValid = this->validateConfiguration();
///x
///x   // Log Data for Debugging...
///x   this->dumpStateToLog();
}


////////////////////////////////////////////////////////////////////////////////
BumpController::~BumpController(void)
{
   // Intentionally left empty
}


////////////////////////////////////////////////////////////////////////////////
void
BumpController::enable(void)
{
   // This controller is enabled if and only if it has good config
   m_isEnabled = m_isConfigValid;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
BumpController::disable(void)
{
   m_isEnabled = false;
   return;
}


////////////////////////////////////////////////////////////////////////////////
void
BumpController::setSetPoint(float setPoint)
{
   // Take the new set point value
   m_setPoint = setPoint;

   // Sanity check it & correct as needed
   if (m_setPoint < m_minInput)
   {
      // ERROR: Set point it too small - cap it
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_setPoint,
                    Psb::LOG_TYPE_FLOAT32, m_minInput);
      m_setPoint = m_minInput;
   }
   else if (m_setPoint > m_maxInput)
   {
      // ERROR: Set point it too big - cap it
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_setPoint,
                    Psb::LOG_TYPE_FLOAT32, m_maxInput);
      m_setPoint = m_maxInput;
   }
   else
   {
      // Set point in valid range, no worries here...
   }

   return;
}


////////////////////////////////////////////////////////////////////////////////
void
BumpController::setConfiguration(float    setPoint,
                                 float    minInput,       float    maxInput,
                                 float    minBumpOutput,  float    maxBumpOutput,
                                 float    minFastOutput,  float    maxFastOutput,
                                 uint32_t onLoopMaxCount, uint32_t offLoopMaxCount,
                                 float    epsilon,        float    tau)
{
   // Take the new values
   m_setPoint        = setPoint;
   m_minInput        = minInput;
   m_maxInput        = maxInput;
   m_minBumpOutput   = minBumpOutput;
   m_maxBumpOutput   = maxBumpOutput;
   m_minFastOutput   = minFastOutput;
   m_maxFastOutput   = maxFastOutput;
   m_onLoopMaxCount  = onLoopMaxCount;
   m_offLoopMaxCount = offLoopMaxCount;
   m_epsilon         = epsilon;
   m_tau             = tau;

   // Check the configuration
   m_isConfigValid = this->validateConfiguration();

   // Log Data for Debugging...
   this->dumpStateToLog();
   return;
}


////////////////////////////////////////////////////////////////////////////////
float
BumpController::runControlLoop(float processValue)
{
   // Tracks whether the output should be on or off... direction comes later
   bool should_output_be_on = false;
   bool is_bump_mode_active = false;

   // Default to not on target
   m_isOnTarget = false;
   
   // We have work to do if the Controller is enabled
   if (true == m_isEnabled)
   {
      // Clip the processValue via min and max inputs
      if (processValue < m_minInput)
      {
         // Clipped at min
         m_processValue = m_minInput;
      }
      else if (processValue > m_maxInput)
      {
         // Clipped at max
         m_processValue = m_maxInput;
      }
      else
      {
         // No change
         m_processValue = processValue;
      }

      
      // Calculate the instantaneous error term e(t)
      m_errorTerm = m_setPoint - m_processValue;
      
      // Are we currently on target?
      if (fabs(m_errorTerm) < m_tau)
      {
         // We're on target!
         should_output_be_on = false;
         m_isOnTarget        = true;

         // Reset our "bump" values
         m_onLoopCounter  = 0;
         m_offLoopCounter = 0;
      }
      else if (fabs(m_errorTerm) < m_epsilon)
      {
         // /////////////////////////////////////////////////////////////////
         // We are close to the target, but not there yet...
         // BUMP Mode!
         //    +----+             +----+             +----+
         //    | ON |     OFF     | ON |     OFF     | ON |
         // ---+    +-------------+    +-------------+    +------  ...
         // /////////////////////////////////////////////////////////////////
         is_bump_mode_active = true;

         // Cycle back and forth between on/off via the config counts
         if (m_onLoopCounter < m_onLoopMaxCount)
         {
            // We still need to be bumping "on"
            should_output_be_on  = true;
            m_onLoopCounter     += 1;
         }
         else if (m_offLoopCounter < m_offLoopMaxCount)
         {
            // We still need to be bumping "off"
            should_output_be_on  = false;
            m_offLoopCounter    += 1;
         }
         else
         {
            // Just finished a loop, start again...
            // But remember that this branch will already make 1 loop iteration!
            should_output_be_on = true;
            m_onLoopCounter     = 1;
            m_offLoopCounter    = 0;
         }
      }
      else
      {
         // /////////////////////////////////////////////////////////////////
         // This is fast-approach mode!
         //    +-------------------------------------------------  ...
         //    | ON 
         // ---+
         // /////////////////////////////////////////////////////////////////
         should_output_be_on = true;
         is_bump_mode_active = false;

         // Make sure we start bump over again from the beginning if we need to
         m_onLoopCounter  = 0;
         m_offLoopCounter = 0;
      }
   }
   else
   {
      // The Controller is currently disabled, output should be off!
      should_output_be_on = false;
      is_bump_mode_active = false;
      m_onLoopCounter     = 0;
      m_offLoopCounter    = 0;
   }


   // ///////////////////////////////////////////////////////////////////////
   // If the output needs to be on, handle that here... and adjust the motor
   // output for proper direction.
   //  If the setpoint is above us, make sure we aren't moving the other way.
   // ///////////////////////////////////////////////////////////////////////
   if (true == should_output_be_on)
   {
      if (true == is_bump_mode_active)
      {
         m_outputValue =
            (m_errorTerm >= 0.0f)
            ? m_maxBumpOutput   // setpoint is above us, so move "up" towards it
            : m_minBumpOutput;  // setpoint is below us, adjust similarly
      }
      else
      {
         m_outputValue =
            (m_errorTerm >= 0.0f)
            ? m_maxFastOutput   // setpoint is above us, so move "up" towards it
            : m_minFastOutput;  // setpoint is below us, adjust similarly
      }
   }
   else
   {
      // Make sure the outputs are OFF!
      m_outputValue = 0.0f;
   }

   // Dump to the debug log
   this->dumpStateToLog();
   
   return m_outputValue;
} // END runControlLoop()


////////////////////////////////////////////////////////////////////////////////
bool
BumpController::isEnabled(void) const
{
   return m_isEnabled;
}


////////////////////////////////////////////////////////////////////////////////
bool
BumpController::isOnTarget(void) const
{
   return m_isOnTarget;
}


////////////////////////////////////////////////////////////////////////////////
bool
BumpController::validateConfiguration(void)
{
   // Assume the configuration IS valid, and check for problems
   bool is_valid = true;


   if ( (m_setPoint < m_minInput) ||
        (m_setPoint > m_maxInput) )
   {
      // ERROR:  The set point must be within the min/max input range
      PSB_LOG_ERROR(3,
                    Psb::LOG_TYPE_FLOAT32, m_minInput,
                    Psb::LOG_TYPE_FLOAT32, m_setPoint,
                    Psb::LOG_TYPE_FLOAT32, m_maxInput);
      is_valid = false;
   }
   else if (m_minInput > m_maxInput)
   {
      // ERROR: Min/Max input ranges are inverted!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_minInput,
                    Psb::LOG_TYPE_FLOAT32, m_maxInput);
      is_valid = false;
   }
   else if (m_minBumpOutput > m_maxBumpOutput)
   {
      // ERROR: Min/Max bump output ranges are inverted!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_minBumpOutput,
                    Psb::LOG_TYPE_FLOAT32, m_maxBumpOutput);
      is_valid = false;
   }
   else if (m_minFastOutput > m_maxFastOutput)
   {
      // ERROR: Min/Max fast output ranges are inverted!
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_minFastOutput,
                    Psb::LOG_TYPE_FLOAT32, m_maxFastOutput);
      is_valid = false;
   }
   else if ( (m_epsilon <= 0.0f) ||
             (m_tau     <= 0.0f) )

   {
      // ERROR: Epsilon and Tau must both be greater than 0.0
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_epsilon,
                    Psb::LOG_TYPE_FLOAT32, m_tau);
      is_valid = false;
   }
   else if (fabs(m_tau) > fabs(m_epsilon))
   {
      // ERROR: |Epsilon| must be > |Tau|
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_epsilon,
                    Psb::LOG_TYPE_FLOAT32, m_tau);
      is_valid = false;
   }
   else if ( (0 == m_onLoopMaxCount) ||
             (0 == m_offLoopMaxCount) )
   {
      // ERROR: Loop counts must both be > 0
      PSB_LOG_ERROR(2,
                    Psb::LOG_TYPE_FLOAT32, m_onLoopMaxCount,
                    Psb::LOG_TYPE_FLOAT32, m_offLoopMaxCount);
      is_valid = false;
   }

   return is_valid;
}


////////////////////////////////////////////////////////////////////////////////
void
BumpController::dumpStateToLog(void) const
{
   PSB_LOG_DEBUG1(19,
                  Psb::LOG_TYPE_FLOAT32, m_setPoint,
                  Psb::LOG_TYPE_FLOAT32, m_minInput,
                  Psb::LOG_TYPE_FLOAT32, m_maxInput,
                  Psb::LOG_TYPE_FLOAT32, m_minBumpOutput,
                  Psb::LOG_TYPE_FLOAT32, m_maxBumpOutput,
                  Psb::LOG_TYPE_FLOAT32, m_minFastOutput,
                  Psb::LOG_TYPE_FLOAT32, m_maxFastOutput,
                  Psb::LOG_TYPE_UINT32,  m_onLoopMaxCount,
                  Psb::LOG_TYPE_UINT32,  m_offLoopMaxCount,
                  Psb::LOG_TYPE_FLOAT32, m_epsilon,
                  Psb::LOG_TYPE_FLOAT32, m_tau,
                  Psb::LOG_TYPE_BOOL,    m_isConfigValid,
                  Psb::LOG_TYPE_BOOL,    m_isEnabled,
                  Psb::LOG_TYPE_BOOL,    m_isOnTarget,
                  Psb::LOG_TYPE_FLOAT32, m_processValue,
                  Psb::LOG_TYPE_FLOAT32, m_errorTerm,
                  Psb::LOG_TYPE_FLOAT32, m_outputValue,
                  Psb::LOG_TYPE_UINT32,  m_onLoopCounter,
                  Psb::LOG_TYPE_UINT32,  m_offLoopCounter);
   return;
}


} // END namespace Psb


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

