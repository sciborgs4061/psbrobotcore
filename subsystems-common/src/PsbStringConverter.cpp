// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <string>
#include <climits>
#include <cmath>
#include <PsbLogger.h>
#include "PsbStringConverter.h"

#ifdef FILE_NUM
#undef FILE_NUM
#endif
#define FILE_NUM 0xFFEE1111


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
int
StringConverter::toInt(std::string const& rc_stringValue,
                       int                defaultValue)
{
   int ret_val = defaultValue;

   char const* pc_setting_value = rc_stringValue.c_str();
   char*       p_end            = NULL;
   long        conv_value       = strtol(pc_setting_value, &p_end, 10);

   // Here is the error handling...
   if ( ((ERANGE   == errno) &&
         (LONG_MAX == conv_value)) ||
        (conv_value > INT_MAX) )
   {
      // Overflow condition
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_stringValue.c_str());
      ret_val = defaultValue;
   }
   else if ( ((ERANGE   == errno) &&
              (LONG_MIN == conv_value)) ||
             (conv_value < INT_MIN) )
   {
      // Underflow condition
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_stringValue.c_str());
      ret_val = defaultValue;
   }
   else if ( ('\0' == *pc_setting_value) ||
             ('\0' != *p_end) )
   {
      // Inconvertible
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_stringValue.c_str());
      ret_val = defaultValue;
   }
   else
   {
      // We're good!
      ret_val = conv_value;
   }

   return ret_val;
}


////////////////////////////////////////////////////////////////////////////////
bool
StringConverter::toBoolean(std::string const& rc_stringValue,
                           bool               defaultValue)
{
   bool ret_val = defaultValue;

   if (("true" == rc_stringValue) ||
       ("TRUE" == rc_stringValue) ||
       ("True" == rc_stringValue) ||
       ("t"    == rc_stringValue) ||
       ("T"    == rc_stringValue) ||
       ("1"    == rc_stringValue))
   {
      // Matches a "true" value
      ret_val = true;
   }
   else if (("false" == rc_stringValue) ||
            ("FALSE" == rc_stringValue) ||
            ("False" == rc_stringValue) ||
            ("f"     == rc_stringValue) ||
            ("F"     == rc_stringValue) ||
            ("0"     == rc_stringValue))
   {
      // Matches a "false" value
      ret_val = false;
   }
   else
   {
      // Inconvertible - use default value
      PSB_LOG_WARNING(1,
                      Psb::LOG_TYPE_BOOL, defaultValue);
      ret_val = defaultValue;
   }

   return ret_val;
}


////////////////////////////////////////////////////////////////////////////////
float
StringConverter::toFloat(std::string const& rc_stringValue,
                         float              defaultValue)
{
   float ret_val = defaultValue;

   // We found it, now convert the string value to a float
   char const* pc_setting_value = rc_stringValue.c_str();
   char*       p_end            = NULL;
   float       conv_value       = strtof(pc_setting_value, &p_end);

   // Here is the error handling...
   if ( (ERANGE     == errno) &&
        (+HUGE_VALF == conv_value) )
   {
      // Too big to store in a float
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_stringValue.c_str());
      ret_val = defaultValue;
   }
   else if ( (ERANGE     == errno) &&
             (-HUGE_VALF == conv_value) )
   {
      // Too small to store in a float
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_stringValue.c_str());
      ret_val = defaultValue;
   }
   else if ( ('\0' == *pc_setting_value) ||
             ('\0' != *p_end) )
   {
      // Inconvertible
      PSB_LOG_ERROR(1,
                    Psb::LOG_TYPE_STRING, rc_stringValue.c_str());
      ret_val = defaultValue;
   }
   else
   {
      // We're good!
      ret_val = conv_value;
   }

   return ret_val;
}


} // END Psb Namespace


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

