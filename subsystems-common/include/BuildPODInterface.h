// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
////  DON'T CHANGE THE STUFF BELOW HERE

#define BUILD_POD_CPP_CODE
#include "pod_definition_macros.h"
#undef BUILD_POD_CPP_CODE

#ifdef BUILD_IT

#define BUILD_JSON_DESCRIPTION
#include "pod_definition_macros.h"
#undef BUILD_JSON_DESCRIPTION

#endif // BUILD_IT

//// must undef these so you can include other subsystems
#undef SUBSYSTEM_NAMESPACE_NAME
#undef INCLUDE_FILE
#undef SUBSYSTEM_SID
#undef SUBSYSTEM_DIV

