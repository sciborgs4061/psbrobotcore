// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////

//////////////
#ifndef SUBSYSTEM_NAMESPACE_NAME
   #error "You must define SUBSYSTEM_NAMESPACE_NAME before including this file"
#endif // SUBSYSTEM_NAMESPACE_NAME
#ifndef INCLUDE_FILE
   #error "You must define INCLUDE_FILE before including this file"
#endif // INCLUDE_FILE
#ifndef SUBSYSTEM_SID
   #error "You must define SUBSYSTEM_SID before including this file"
#endif // SUBSYSTEM_SID
#ifndef SUBSYSTEM_DIV
   #error "You must define SUBSYSTEM_DIV before including this file"
#endif // SUBSYSTEM_DIV


///////////  undefine all macros  ///////////////
#undef START_NEW_SS
#undef SS_POD_DATA
#undef SS_CHAR32_POD_DATA
#undef SS_POD_DATA_DELIMITER
#undef END_SS

////////  NOW Define new ones //////////////



////////////////////////////////////////////////////////
#ifdef BUILD_JSON_DESCRIPTION

#define START_NEW_SS(SS_NAME) \
char const Dashstream_POD_Description[] = "\
\n    { \
\n      \"ss_name\": \"" #SS_NAME "\", \
\n      \"ss_data\": [ "

#define SS_CHAR32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n        { \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"char[32]\", \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define SS_POD_DATA(SS_TYPE,SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) "\
\n        { \
\n           \"var_name\": \"" #SS_VAR_NAME "\", \
\n           \"intf_type\": " #INTF_TYPE ", \
\n           \"type\": \"" #SS_TYPE "\", \
\n           \"display_name\": " #SS_DISPLAY_NAME ", \
\n           \"description\": " #SS_DESCRIPTION " \
\n        }"

#define SS_POD_DATA_DELIMITER ","

#define END_SS "\
\n      ] \
\n    } \
\n"; \

namespace Psb {
namespace Subsystem {
namespace SUBSYSTEM_NAMESPACE_NAME {

#include INCLUDE_FILE

char const *get_Dashstream_POD_Description(void)
{
   return Dashstream_POD_Description;
}

} /* END namespace THE_SUBSYSTEM_NAMESPACE_NAME */ \
} /* END namespace Subsystem */ \
} /* END namespace Psb */

#endif // BUILD_JSON_DESCRIPTION
////////////////////////////////////////////////////////




////////////////////////////////////////////////////////
#ifdef BUILD_POD_CPP_CODE

#include <stdlib.h>
#include "PsbMacros.h"
#include "DashstreamBaseInterface_I.h"
#include <string.h>

namespace Psb {
namespace Subsystem {
namespace SUBSYSTEM_NAMESPACE_NAME {

char const *get_Dashstream_POD_Description(void);

class DashstreamInterface : public Psb::Subsystem::Dashstream::BaseInterface_I
{
   public:
      DashstreamInterface(void)
         : BaseInterface_I()
         , m_data()
      {
         /* Start with clean data */
         memset(&m_data,0,sizeof(m_data));
      }

#ifndef X86_PLATFORM
// #pragma pack(push,1)
#else // X86_PLATFORM
#pragma pack(1)
#endif // X86_PLATFORM
      typedef struct \
      {

#define START_NEW_SS(SS_NAME)
#define SS_POD_DATA(SS_TYPE,SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         SS_TYPE SS_VAR_NAME;
#define SS_CHAR32_POD_DATA(SS_VAR_NAME,INTF_TYPE,SS_DISPLAY_NAME,SS_DESCRIPTION) \
         char SS_VAR_NAME[32];
#define SS_POD_DATA_DELIMITER
#define END_SS

#include INCLUDE_FILE

      } DashstreamPOD_t;
#ifndef X86_PLATFORM
// #pragma pack(pop)
#else // X86_PLATFORM
#pragma pack()
#endif // X86_PLATFORM

      virtual ~DashstreamInterface(void) { /* Intentionally left empty */ }
      virtual char const *getDescription(void) const { return get_Dashstream_POD_Description(); }
      // virtual void const *getData(void) const { return &m_data; }
      virtual void *getData(void) { return &m_data; }
      virtual size_t dataSize(void) const { return sizeof(m_data); }
      virtual uint8_t SID(void) const { return SUBSYSTEM_SID; }
      virtual uint8_t DIV(void) const { return SUBSYSTEM_DIV; }

   private:
      DISALLOW_COPY_CTOR(DashstreamInterface);
      DISALLOW_ASSIGNMENT_OPER(DashstreamInterface);

      DashstreamPOD_t m_data;
};

} /* END namespace Dashstream */
} /* END namespace Subsystem */
} /* END namespace Psb */

#endif // BUILD_POD_CPP_CODE
////////////////////////////////////////////////////////

