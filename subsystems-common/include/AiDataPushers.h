////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __AiDataPushers_h__
#define __AiDataPushers_h__

// DEBUG
#include <PsbLogger.h>
#define FILE_NUM 0xACACACAC

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "PsbMacros.h"
#include "SpeedController.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////  
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief 
/// Provides an interface for the AI to push data to the physical objects
/// from the controllers that have the values to push
////////////////////////////////////////////////////////////////////////////////
class AiDataPusher
{
   public:
      AiDataPusher(void) {}
      virtual ~AiDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the function that will actually push the data
      ///   must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void) = 0;

   private:
      DISALLOW_COPY_CTOR(AiDataPusher);
      DISALLOW_ASSIGNMENT_OPER(AiDataPusher);

}; // END class OiDataGrabber

/////////////////////////////////////////////////////////////////////////
/// @breif
///   Class to grab data from a Joystick
///   [in] T is the type of the memory location we are putting the data
class SpeedControllerDataPusher : public AiDataPusher
{
   public:

      /*  ... if there are multiple things to actuate
       *  we would enumerate them here ... but there aren't
      typedef enum {
           RAW_AXIS_1
         , RAW_AXIS_2
         , RAW_BUTTON_1
         , RAW_BUTTON_2
         , RAW_BUTTON_3
         , RAW_BUTTON_4
         , RAW_BUTTON_5
         , RAW_BUTTON_6
         , RAW_BUTTON_7
         , RAW_BUTTON_8
      } wanted_data_t;
      */

      ////////////////////////////////////////////////////////////////////////// 
      SpeedControllerDataPusher(void *mem) 
        : m_memory(mem)
        , m_speedController(NULL)
      {
         // left blank on purpose
         PSB_LOG_DEBUG3(0);
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setSpeedController(SpeedController *speedController)
      {
         PSB_LOG_DEBUG3(0);
         m_speedController = speedController;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~SpeedControllerDataPusher(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the function that will actually grab the data
      ///   must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void pushData(void)
      {
         PSB_LOG_DEBUG3(0);
         m_speedController->Set(*((double *)m_memory));
      }

   private:
      DISALLOW_COPY_CTOR(SpeedControllerDataPusher);
      DISALLOW_ASSIGNMENT_OPER(SpeedControllerDataPusher);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   The pointer to the speed controller
      ////////////////////////////////////////////////////////////////////////// 
      SpeedController *m_speedController;

}; // END class SpeedControllerDataPusher

} // END Subsystem namespace
} // END Psb namespace

#undef FILE_NUM // 0xACACACAC

#endif // __AiDataPushers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// ///////////////////////////////////////////////////////////////////////////// 

