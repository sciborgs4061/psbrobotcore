// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __SubsystemContainer_h__
#define __SubsystemContainer_h__

#include "PsbMacros.h"
#include "Controller_I.h"
#include "DashstreamBaseInterface_I.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This class provides a common interface to get subsystem pieces
////////////////////////////////////////////////////////////////////////////////
class SubsystemContainer
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~SubsystemContainer(void);

      virtual Dashstream::BaseInterface_I  *getDashstreamInterface(void) = 0;
      virtual Controller_I                 *getControllerInterface(void) = 0;

      virtual void init(void) = 0;

   protected:
      //////////////////////////////////////////////////////////////////////////
      /// @brief 
      /// Default Constructor
      //////////////////////////////////////////////////////////////////////////
      SubsystemContainer(void);

   private:
      DISALLOW_COPY_CTOR(SubsystemContainer);
      DISALLOW_ASSIGNMENT_OPER(SubsystemContainer);

}; // END class SubsystemContainer

} // END Subsystem namespace
} // END Psb Namespace


#endif // __SubsystemContainer_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

