// /////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __GlobalSiWpiStore_h__
#define __GlobalSiWpiStore_h__


// ///////////////////////////////////////////////////////////////////////////// 
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <list>
#include "SiDataGrabbers.h"

// ///////////////////////////////////////////////////////////////////////////// 
// Namespace(s)
// ///////////////////////////////////////////////////////////////////////////// 
namespace Psb {
namespace Subsystem {

//////////////////////////////////////////////////////////////////////////////// 
/// @brief 
/// Class that provides a single access point for all sensor interface Wpi 
/// objects in this file
//////////////////////////////////////////////////////////////////////////////// 
class GlobalSiWpiStore
{
   public:

      // if we did this right, this could be a "robot specific"
      // set of enumerations that is included into this file
      // during the build process...and it would define all the
      // sizes of the thing that are available.

      typedef enum {
           ENCODER_ONE  // these should have meaningful names
         , ENCODER_TWO  // these should have meaningful names
         , MAX_ENCODERS
      } si_encoders_t;

      typedef enum {
           LIMITSWITCH_ONE  // these should have meaningful names
         , LIMITSWITCH_TWO  // these should have meaningful names
         , MAX_DIGITAL_INPUTS
      } si_digitalinputs_t;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      /// Singleton create/access function
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalSiWpiStore* instance(void);

      ///////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      ///////////////////////////////////////////////////////////////////////////
      ~GlobalSiWpiStore(void);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   attach a memory location to type of SI
      ///
      /// [in] edg - the encoder data grabber
      /// [in] idx - the zero based index of the item to push data to
      ///              use the si_encoders_t enumeration
      ////////////////////////////////////////////////////////////////////////// 
      void attach(EncoderDataGrabber *edg, si_encoders_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   attach a memory location to type of SI
      ///
      /// [in] lsdg - the limit swith data grabber
      /// [in] idx - the zero based index of the item to push data to
      ///              use the si_digitalinputs_t enumeration
      ////////////////////////////////////////////////////////////////////////// 
      void attach(DigitalInputDataGrabber *didg, si_digitalinputs_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   refresh all the values from the physical objects
      ////////////////////////////////////////////////////////////////////////// 
      void refresh(void);

   private:
     ///////////////////////////////////////////////////////////////////////////
     /// @brief 
     /// Constructor
     ///////////////////////////////////////////////////////////////////////////
     GlobalSiWpiStore(void);

     ////////////////////////////////////////////////////////////////////////// 
     /// @brief 
     /// The class's singleton instance
     ////////////////////////////////////////////////////////////////////////// 
     static GlobalSiWpiStore* msp_instance;

     ///////////////////////////////////////////////////////////////////////////
     /// @brief
     /// Array of Encoders
     ///////////////////////////////////////////////////////////////////////////
     Encoder* m_encoder[MAX_ENCODERS];

     ///////////////////////////////////////////////////////////////////////////
     /// @brief
     /// Array of DigitalInputs
     ///////////////////////////////////////////////////////////////////////////
     DigitalInput* m_digitalInput[MAX_DIGITAL_INPUTS];

     ////////////////////////////////////////////////////////////////////////// 
     /// @brief 
     /// All the data grabbers
     ////////////////////////////////////////////////////////////////////////// 
     std::list<SiDataGrabber *> m_grabbers;

   private:
     DISALLOW_COPY_CTOR(GlobalSiWpiStore);
     DISALLOW_ASSIGNMENT_OPER(GlobalSiWpiStore);
}; // END class GlobalSiWpiStore      

} // END namespace Subsystem
} // END namespace Psb

#endif // __GlobalSiWpiStore_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

