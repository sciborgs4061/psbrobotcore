////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __PsbSubsystemTypes_h__
#define __PsbSubsystemTypes_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {


// /////////////////////////////////////////////////////////////////////////////
// Define(s)
// /////////////////////////////////////////////////////////////////////////////
#define M_PI         3.14159265358979323846  // pi
#define M_PI_OVER_2  1.57079632679489661923  // pi/2
#define M_PI_OVER_4  0.78539816339744830962  // pi/4


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This enum advertises a more readable interface for button states.
////////////////////////////////////////////////////////////////////////////////
typedef enum
{
   BUTTON_RELEASED = 0,
   BUTTON_PRESSED  = 1
} button_state_t;

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This enum advertises a more readable interface for switch states.
////////////////////////////////////////////////////////////////////////////////
typedef enum
{
   SWITCH_CLOSED = 0,
   SWITCH_OPEN = 1
} switch_state_t;
      
////////////////////////////////////////////////////////////////////////////////
/// @brief
/// This enum defines the enabled state of a given element [en/dis]abled.
////////////////////////////////////////////////////////////////////////////////
typedef enum
{
   DISABLED,
   ENABLED
} enabled_state_t;

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Each entry in the enumeration identifies a Match state as defined by the
/// DriverStation control system.  This is a more convenient and succinct way to
/// represent match state than the "IsAutonomous()" and related methods in the
/// DriverStation class.
///
/// @remarks
/// These states are needed because there exists a race condition where FRC
/// match control packets might have updated the global match state whilst we
/// are in the middle of one of our control loops.  When we get around to
/// processing the state change, we note that we have.  Our code will always use
/// these defines for determining match state instead of relying on asynchronous
/// packet data.
////////////////////////////////////////////////////////////////////////////////
typedef enum
{
   MATCH_STATE_DISABLED = 0,
   MATCH_STATE_AUTONOMOUS,
   MATCH_STATE_TELEOPERATED,
   MATCH_STATE_TEST,
   
   // Insert New Match States above this line
   NUM_MATCH_STATES
} frc_match_state_t;

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// System state that determines its behavior.
////////////////////////////////////////////////////////////////////////////////
typedef enum
{
   SUBSYSTEM_DISABLED = 0,  ///< Subsystem is not accepting input
   SUBSYSTEM_MANUAL,        ///< Subsystem is receiving input from the user
   SUBSYSTEM_AUTOMATIC,     ///< Subsystem is receiving input from a program

   // Insert new subsystem states above this line
   NUM_SUBSYSTEM_STATES
} subsystem_state_t;

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Bump Controller configure type
////////////////////////////////////////////////////////////////////////////////
typedef struct
{
   float    m_setPoint;        // Process Set Point
   float    m_minInput;        // Minimum Input Value
   float    m_maxInput;        // Maximum Input Value
   float    m_minBumpOutput;   // Minimum Output Value, bump mode
   float    m_maxBumpOutput;   // Maximum Output Value, bump mode
   float    m_minFastOutput;   // Minimum Output Value, fast mode
   float    m_maxFastOutput;   // Maximum Output Value, fast mode
   uint32_t m_onLoopMaxCount;  // Number of control loops to hold output as "on"
   uint32_t m_offLoopMaxCount; // Number of control loops to hold output as "off"
   float    m_epsilon;         // If |e(t)| < m_epsilon, then enable bump control
   float    m_tau;             // If |e(t)| < m_tau, then onTarget
} bump_controller_config_t;



} // END namespace Subsystem
} // END namespace Psb

#endif // __PsbSubsystemTypes_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

