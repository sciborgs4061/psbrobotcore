// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __GlobalAiWpiStore_h__
#define __GlobalAiWpiStore_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <PsbMacros.h>
#include <list>
#include "AiDataPushers.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief
/// Provides a single access point for all WPI objects that will be used to
/// implement the subsystem-specific Actuator Interface classes
////////////////////////////////////////////////////////////////////////////////
class GlobalAiWpiStore
{
   public: 

      typedef enum {
           MOTOR_ONE  // these should have meaningful names
         , MOTOR_TWO  // these should have meaningful names
         , MAX_SPEED_CONTROLLER
      } ai_speedcontrollers_t;

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief
      /// Singleton create/access function
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalAiWpiStore* instance(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      ~GlobalAiWpiStore(void);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   attach a memory location to type of AI
      ///
      /// [in] scdp - the speed controller data pusher
      /// [in] idx - the zero based index of the item to push data to
      ///              ... use the ai_speedcontrollers_t enumeration
      ////////////////////////////////////////////////////////////////////////// 
      void attach(SpeedControllerDataPusher *scdp, ai_speedcontrollers_t idx);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   flush all the values to the physical objects
      ////////////////////////////////////////////////////////////////////////// 
      void flush(void);

   private:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Default Constructor
      //////////////////////////////////////////////////////////////////////////
      GlobalAiWpiStore(void);

   private:
      ////////////////////////////////////////////////////////////////////////// 
      /// @brief 
      /// The class's singleton instance
      ////////////////////////////////////////////////////////////////////////// 
      static GlobalAiWpiStore* msp_instance;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// All the speed controllers
      ///  ... maybe an enum so they have names?
      //////////////////////////////////////////////////////////////////////////
      SpeedController* m_speedController[MAX_SPEED_CONTROLLER];

      ////////////////////////////////////////////////////////////////////////// 
      /// @brief 
      /// All the data pushers
      ////////////////////////////////////////////////////////////////////////// 
      std::list<AiDataPusher *> m_pushers;

   private:
      DISALLOW_COPY_CTOR(GlobalAiWpiStore);
      DISALLOW_ASSIGNMENT_OPER(GlobalAiWpiStore);

}; // END class GlobalAiWpiStore


////////////////////////////////////////////////////////////////////////////////
} // END namespace Subsystem
} // END namespace Psb


#endif // __GlobalAiWpiStore_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

