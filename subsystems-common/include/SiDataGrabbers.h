////////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef __SiDataGrabbers_h__
#define __SiDataGrabbers_h__

// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include "PsbMacros.h"
#include "DigitalInput.h"
#include "Encoder.h"

// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////  
namespace Psb {
namespace Subsystem {

////////////////////////////////////////////////////////////////////////////////
/// @brief 
/// Provides an interface for the OI to grab data from physical objects
/// and put that data where it needs to go
////////////////////////////////////////////////////////////////////////////////
class SiDataGrabber
{
   public:
      SiDataGrabber(void) {}
      virtual ~SiDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the function that will actually grab the data
      ///   must be overridden by the derived class
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void) = 0;

   private:
      DISALLOW_COPY_CTOR(SiDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(SiDataGrabber);

}; // END class SiDataGrabber

/////////////////////////////////////////////////////////////////////////
/// @breif
///   Class to grab data from a DigitalInput (like a LimitSwitch)
///   [in] T is the type of the memory location we are putting the data
class DigitalInputDataGrabber : public SiDataGrabber
{
   public:

      ////////////////////////////////////////////////////////////////////////// 
      DigitalInputDataGrabber(void *mem) 
        : m_memory(mem)
        , m_digitalInput(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setDigitalInput(DigitalInput *di)
      {
         m_digitalInput = di;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~DigitalInputDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the function that will actually grab the data
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((bool *)m_memory)) = m_digitalInput->Get();
      }

   private:
      DISALLOW_COPY_CTOR(DigitalInputDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(DigitalInputDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   The pointer to the digitalInput
      ////////////////////////////////////////////////////////////////////////// 
      DigitalInput *m_digitalInput;

}; // END class DigitalInputDataGrabber

/////////////////////////////////////////////////////////////////////////
/// @breif
///   Class to grab data from an Encoder
///   [in] T is the type of the memory location we are putting the data
class EncoderDataGrabber : public SiDataGrabber
{
   public:

      // this one might need to take an enumeration of the "type" of
      // data you actually want to get ... but for now we will just "Get()"

      ////////////////////////////////////////////////////////////////////////// 
      EncoderDataGrabber(void *mem) 
        : m_memory(mem)
        , m_encoder(NULL)
      {
         // left blank on purpose
      }

      ////////////////////////////////////////////////////////////////////////// 
      void setEncoder(Encoder *encoder)
      {
         m_encoder = encoder;
      }

      ////////////////////////////////////////////////////////////////////////// 
      virtual ~EncoderDataGrabber(void) {}

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the function that will actually grab the data
      ////////////////////////////////////////////////////////////////////////// 
      virtual void grabData(void)
      {
         (*((int32_t *)m_memory)) = m_encoder->Get();
      }

   private:
      DISALLOW_COPY_CTOR(EncoderDataGrabber);
      DISALLOW_ASSIGNMENT_OPER(EncoderDataGrabber);

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   the memory location that we store the value in
      ////////////////////////////////////////////////////////////////////////// 
      void *m_memory;

      ////////////////////////////////////////////////////////////////////////// 
      /// @ brief
      ///   The pointer to the Encoder
      ////////////////////////////////////////////////////////////////////////// 
      Encoder *m_encoder;

}; // END class EncoderDataGrabber

} // END Subsystem namespace
} // END Psb namespace

#endif //__SiDataGrabbers_h__

// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// ///////////////////////////////////////////////////////////////////////////// 

