// /////////////////////////////////////////////////////////////////////////////
//
// License:
//   Copyright (c) 2016 PART Sciborgs, FRC Team 4061
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// /////////////////////////////////////////////////////////////////////////////
#ifndef __PsbBumpController_h__
#define __PsbBumpController_h__


// /////////////////////////////////////////////////////////////////////////////
// Include(s)
// /////////////////////////////////////////////////////////////////////////////
#include <cstdint>
#include <PsbMacros.h>


// /////////////////////////////////////////////////////////////////////////////
// Namespace(s)
// /////////////////////////////////////////////////////////////////////////////
namespace Psb {


////////////////////////////////////////////////////////////////////////////////
/// @brief
/// The BumpController class implements a simplifying tweak to the "P"
/// component of the classic PID control loop.
///
/// @remarks
/// The basic principal of operation is...
///  - There is a set point, just like a PID controller that we are trying to
///    reach.
///  - The error term is the difference between the set point and the process
///    variable's current value.
///  - If the error is greater than "epsilon" then the controller outputs 100%
///    power in the error-minimizing direction.
///  - If the error is less than the "epsilon" range, but greater than the "tau"
///    range, then the controller will modulate its output based on the "on/off"
///    cycle counts.
///  - If the error is less than the "on target" range, then the outputs are
///    zeroed out.
////////////////////////////////////////////////////////////////////////////////
class BumpController
{
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Constructor
      //////////////////////////////////////////////////////////////////////////
      BumpController(void);
                       
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Destructor
      //////////////////////////////////////////////////////////////////////////
      virtual ~BumpController(void);
      
      
   public:
      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Enables the controller
      //////////////////////////////////////////////////////////////////////////
      void enable(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Disables the controller
      //////////////////////////////////////////////////////////////////////////
      void disable(void);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// Changes the set point on the fly
      ///
      /// @param[in] setPoint   The new desired set point for the controller
      //////////////////////////////////////////////////////////////////////////
      void setSetPoint(float setPoint);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method changes the controller configuration on the fly.
      /// 
      /// @remarks
      /// It is recommended that the controller be disabled and re-enabled
      /// when changing configuration.
      ///
      /// @param[in] setPoint      The target of the Controller
      /// @param[in] minInput      Cap process variable input at this lower limit
      /// @param[in] maxInput      Cap process variable input at this upper limit
      /// @param[in] minBumpOutput Cap Controller output at this lower limit during "bump" mode
      /// @param[in] maxBumpOutput Cap Controller output at this upper limit during "bump" mode
      /// @param[in] minFastOutput Cap Controller output at this lower limit during "fast" mode
      /// @param[in] maxFastOutput Cap Controller output at this upper limit during "fast" mode
      /// @param[in] onLoopCount   The number of calls to "calculate" where output is pulsed "on"
      /// @param[in] offLoopCount  The number of calls to "calculate" where output is pulsed "off"
      /// @param[in] epsilon       The tolerance range within which the output is "bumped"
      /// @param[in] tau           The tolerance range where we consider the process "on target"
      //////////////////////////////////////////////////////////////////////////
      void setConfiguration(float    setPoint,
                            float    minInput,      float    maxInput,
                            float    minBumpOutput, float    maxBumpOutput,
                            float    minFastOutput, float    maxFastOutput,
                            uint32_t onLoopCount,   uint32_t offLoopCount,
                            float    epsilon,       float    tau);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method causes the controller to process one instance of its
      /// control loop.
      ///
      /// @param[in] processVariable  The current value of the variable being controlled.
      ///
      /// @returns
      /// The output data that should be used to control the system in the way
      /// that causes the process variable to "move" in the desired manner.
      //////////////////////////////////////////////////////////////////////////
      float runControlLoop(float processVariable);

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method tells the caller whether or not the controller is enabled.
      ///
      /// @remarks
      /// If the controller was given bad configuration, and then the enable()
      /// function is called, it is still possible for the controller to be
      /// disabled.
      ///
      /// @see enable()
      /// @see setConfiguration()
      //////////////////////////////////////////////////////////////////////////
      bool isEnabled(void) const;

      //////////////////////////////////////////////////////////////////////////
      /// @brief
      /// This method tells the caller whether or not the process variable is
      /// within the innerEpsilon target.
      //////////////////////////////////////////////////////////////////////////
      bool isOnTarget(void) const;


   private:
      float    m_setPoint;        // Process Set Point
      float    m_minInput;        // Minimum Input Value
      float    m_maxInput;        // Maximum Input Value
      float    m_minBumpOutput;   // Minimum Output Value, bump mode
      float    m_maxBumpOutput;   // Maximum Output Value, bump mode
      float    m_minFastOutput;   // Minimum Output Value, fast mode
      float    m_maxFastOutput;   // Maximum Output Value, fast mode
      uint32_t m_onLoopMaxCount;  // Number of control loops to hold output as "on"
      uint32_t m_offLoopMaxCount; // Number of control loops to hold output as "off"
      float    m_epsilon;         // If |e(t)| < m_epsilon, then enable bump control
      float    m_tau;             // If |e(t)| < m_tau, then onTarget

      bool     m_isConfigValid;   // True if the configuration is okay to use
      bool     m_isEnabled;       // Whether or not the PID controller is enabled
      bool     m_isOnTarget;      // True when the error term is within the inner epsilon region
      float    m_processValue;    // p(t) this loop  (input)
      float    m_errorTerm;       // e(t) this loop  (error)
      float    m_outputValue;     // y(t) this loop  (output)
      uint32_t m_onLoopCounter;   // Number of loops the controller has been outputting "on"
      uint32_t m_offLoopCounter;  // Number of loops the controller has been outputting "off"
      
   private:
      void dumpStateToLog(void) const;
      bool validateConfiguration(void);

   private:
      DISALLOW_COPY_CTOR(BumpController);
      DISALLOW_ASSIGNMENT_OPER(BumpController);
}; // END class BumpController


} // END namespace Psb


#endif //__PsbBumpController_h__


// /////////////////////////////////////////////////////////////////////////////
// END OF FILE
// /////////////////////////////////////////////////////////////////////////////

